package tools;

import java.io.InputStream;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import android.util.Log;
/**
 * <p>
 * Classe qui permet de r�cup�rer un inputStream relatif � un certain WebService
 * </p>
 * @author Matthias & Junior
 *
 */
public class HttpMethodeGetRunnable implements Runnable {
	/** Adresse ou est localis� le service web � appel� */
	private String adresse;
	/** Contient l'inputStream � renvoyer lors de l'appel de la m�thode getInputStream() */
	private InputStream inputStream;
	/** Code de retour de la requ�te HTTP */
	private int returnCodeHttp;

	// objet de synchronization
	private  static Object key =  new Object() ;

	/** 
	 * M�thode qui renvoie un objet InputStream � traiter pour r�cup�rer les infos envoy�es par la requ�te http
	 * 
	 * @param adresse String. Contient l'adresse du service web sous forme de chaine de caract�re
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 */
	public InputStream getInputStream(String adresse) {
		synchronized(key) {

			this.adresse = adresse;
			// On lance l� m�thode run() qui s'occupe de tout le traitement
			run();

		}

		return inputStream;
	}

	@Override
	public void run() {
		Log.i("Biocold", adresse);

		//Initialisation du client HTPP qui va �tre utilis� pour ex�cuter la requ�te
		DefaultHttpClient httpClient;
		HttpParams httpParameters = new BasicHttpParams();

		//Permet de visualiser les requ�tes http grace � fiddler install� sur le pc � l'ip indiqu�e
		//HttpHost proxy = new HttpHost("192.168.1.31", 8888);
		//httpParameters.setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

		httpClient = new DefaultHttpClient(httpParameters);
		try {
			URI uri = new URI(adresse);
			//Cr�ation de la requ�te, ici de type GET
			HttpGet myRequest = new HttpGet(uri);

			//L'ex�cution de la requ�te GET me renvoie une r�ponse de type HtppResponse
			//Je r�cup�re le contenu de cette r�ponse en format InputStream
			HttpResponse res = httpClient.execute(myRequest);
			inputStream = res.getEntity().getContent();

			returnCodeHttp = res.getStatusLine().getStatusCode();
			if(res.getStatusLine().getStatusCode() != 200)
				Log.e("Biocold", "Status HTTP : " + res.getStatusLine());
			else
				Log.i("Biocold", "Status HTTP : " + res.getStatusLine());
		} // Quelque chose ne s'est pas bien pass� (erreur de chargement, le login n'�xiste pas...)
		catch (Exception e){}
	}

	/**
	 * @return le code de retour de la requete http
	 */
	public int getReturnCode() {
		return returnCodeHttp;
	}
}
