package tools;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.http.NameValuePair;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.widget.Toast;
import bcp.android.service.SynchroEssaiContractService;
import bpc.android.biocold.R;

/**
 * <p>
 * Toute m�thodes qui puisse �tre g�n�ral et �tre utilis�e un peu n'importe o� et n'importe quand.
 * </p>
 * 
 * @author Junior
 */
public class Utility {
	/**
	 * Permet de savoir si la connection internet est disponible.
	 * @param context Context de l'activit� qui utilisera la connection internet.
	 * @return Indique si la connexion internet est disponible ou non.
	 */
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	} 
	
	/**
	 * M�thode qui permet de convertir une cha�ne de caract�res en format MD5
	 * @param base Cha�ne � convertir
	 * @return {@link base} convertit
	 */
	@SuppressLint("DefaultLocale")
	public static String convertMD5(String base) {
		MessageDigest m;
		String hashtext = "";
		try {
			m = MessageDigest.getInstance("MD5");
			m.reset();
			m.update(base.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			hashtext = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return hashtext.toUpperCase();
	}
	
	public static String convertToXML(List<NameValuePair> liste) {
        StringBuilder sb = new StringBuilder();

        for (NameValuePair paire : liste) {
            sb.append("<");
            sb.append(paire.getName());
            sb.append(">");

            sb.append(paire.getValue());
            sb.append("</");
            sb.append(paire.getName());
            sb.append(">");
        }

        return sb.toString();
    }
	
	/**
	 * Permet de faire afficher une notification depuis n'importe quel activit� ou service de l'application
	 * 
	 * @param context Context du thread en cours
	 * @param ticker Message affich� � l'arriv�e de la notification
	 * @param title Titre de la notification
	 * @param message Message affich� par la notification
	 * @param id Id de la notification (peut �tre utile pour annuler la notif ou autres action sur celle-ci)
	 */
	public static void showNotification(Context context, String ticker, String title, String message, int id){
		Intent notificationIntent = new Intent(context, SynchroEssaiContractService.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		Resources res = context.getResources();
		Builder builder = new NotificationCompat.Builder(context);

		builder.setContentIntent(contentIntent)
		            .setSmallIcon(R.drawable.icone)
		            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.logo_bcp))
		            // Le ticker est le message qui s'affiche � l'arriv�e de la notification
		            .setTicker(ticker)
		            .setWhen(System.currentTimeMillis())
		            .setAutoCancel(true)
		            .setContentTitle(title)
		            .setContentText(message);
		Notification notification = builder.build();

		notificationManager.notify(id, notification);
    }
	
	/**
	 * Classe qui permet d'afficher des toast depuis n'importe quel thread dans l'application (� utiliser uniquement pour les threds secondaires
	 * 
	 * <p><pre>Pour afficher le toast dans un thread secondaire, on �crit la ligne suivante : 
	 * "new Handler(Looper.getMainLooper()).post(new Utility
	 * 				.DisplayToast(this, message_a_afficher));"</pre></p>
	 * @author Matthias
	 * @author Junior
	 *
	 */
	public static class DisplayToast implements Runnable {
		private final Context mContext;
	    String mText;

	    public DisplayToast(Context mContext, String text){
	        this.mContext = mContext;
	        mText = text;
	    }

	    public void run(){
	        Toast.makeText(mContext, mText, Toast.LENGTH_SHORT).show();
	    }
	}
	
	/**
	 * Cette m�thode permet de transformer un inputStream directement en string. Se qui est utile pour r�cup�rer la valeur
	 * de retour d'un webservice REST quand celui-ci en retourne une.
	 * @param is Le flux qui doit �tre transformer en String
	 * @return La valeur en string du flux qui a �t� pass� en param�tre
	 */
	private static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	public static int getReturnValue(java.io.InputStream is)
	{
		int val = 0;
		String string = convertStreamToString(is);
		string = string.substring(string.indexOf(">"));
		try
		{
			string = (String)string.subSequence(1, string.indexOf("<"));
			val = Integer.parseInt(string);
		}
		catch(NumberFormatException e)
		{
			val = 0;
			Log.e("Exception in Utility", "Sequence string -> int : "+string);
		}
		return val;
	}
}
