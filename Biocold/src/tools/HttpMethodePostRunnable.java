package tools;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

/**
 * <p>
 * Classe qui permet de r�cup�rer un inputStream relatif � un certain WebService
 * </p>
 * @author Matthias 
 * @author Junior
 *
 */
public class HttpMethodePostRunnable implements Runnable {
	/** Adresse ou est localis� le service web � appel� */
	private String adresse;
	/** Contient l'inputStream � renvoyer lors de l'appel de la m�thode getInputStream() */
	private InputStream inputStream;
	/** Liste des param�tres � passer dans la requ�te http par la m�thode POST */
	private String xml;
	/** Code de retour de la requ�te HTTP */
	private int returnCodeHttp;

	// objet de synchronization
	private  static Object key =  new Object() ;

	/** 
	 * M�thode qui renvoie un objet InputStream � traiter pour r�cup�rer les infos envoy�es par la requ�te http
	 * 
	 * @param adresse String. Contient l'adresse du service web sous forme de chaine de caract�re
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 * @param listParameters Liste des param�tres � envoyer au serveur en POST
	 */
	public InputStream executeRequest(String adresse, String xml) {

		synchronized(key) {
			this.adresse = adresse;
			this.xml = xml;

			// On lance l� m�thode run() qui s'occupe de tout le traitement
			run();
		}
		
		return inputStream;
	}

	@Override
	public void run() {		
		//Initialisation du client HTPP qui va �tre utilis� pour ex�cuter la requ�te
		DefaultHttpClient httpClient;
		HttpParams httpParameters = new BasicHttpParams();

		//Permet de visualiser les requ�tes http grace � fiddler install� sur le pc � l'ip indiqu�e
		//HttpHost proxy = new HttpHost("192.168.1.32", 8888);
		//httpParameters.setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

		httpClient = new DefaultHttpClient(httpParameters);
		try {
			URI uri = new URI(adresse);
			//Cr�ation de la requ�te, ici de type POST
			HttpPost myRequest = new HttpPost(uri);
			myRequest.setHeader("Content-Type", "application/xml");
			myRequest.setEntity(new StringEntity(xml));

			//L'ex�cution de la requ�te POST me renvoie une r�ponse de type HtppResponse
			//Je r�cup�re le contenu de cette r�ponse en format InputStream
			HttpResponse res = httpClient.execute(myRequest);
			inputStream = res.getEntity().getContent();

			returnCodeHttp = res.getStatusLine().getStatusCode();

			if(res.getStatusLine().getStatusCode() != 200)
				Log.e("Biocold", "Status HTTP : " + res.getStatusLine());
			else
				Log.i("Biocold", "Status HTTP : " + res.getStatusLine());
		} // Quelque chose ne s'est pas bien pass� (erreur de chargement, le login n'�xiste pas...)
		catch (IOException e) {
			Log.e("Biocold", e.toString());
		}
		catch (URISyntaxException use){
			Log.e("Biocold", use.toString());
		}
	}



	public int getReturnCode() {
		return returnCodeHttp;
	}

	public void setReturnCode(int returnCode) {
		this.returnCodeHttp = returnCode;
	}

}

