package tools;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import bpc.android.biocold.R;

public class LocationRunnable implements LocationListener, Runnable {

	/** Variables qui r�cup�reront les informations de localisation */
	private static String latitude, longitude;

	/** Pour les log */
	private final static String TAG = "BIOCOLD_LocationService";

	/** Objet permettant de localiser le smartphone */
	private LocationManager locationManager;

	/** Context de l'application r�cupp�r�e par le constructeur */
	private Context context;

	public LocationRunnable(Context context){
		this.context = context;
	}

	/**
	 * Cette m�thode permet de r�cup�rer la localisation � chaque fois quel change
	 */
	@Override
	public void onLocationChanged(Location location) {
		Log.i(TAG, "Pr�cision : " + location.getAccuracy());
		Log.i(TAG, "Provider : " + location.getProvider());
		// On r�cup�re la latitude et la longitude
		latitude = Double.toString(location.getLatitude());
		longitude = Double.toString(location.getLongitude());
		if(location.getAccuracy() <= 90.0){
			locationManager.removeUpdates(this);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {
		if(provider.equals(LocationManager.GPS_PROVIDER)){
			Log.i(TAG, "GPS activ�");
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, (LocationListener)this);
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	/** Getter de la latitude */
	public static String getLatitude() {
		return latitude;
	}

	/** Getter de la longitude */
	public static String getLongitude() {
		return longitude;
	}

	@Override
	public void run() {
		Looper.prepare();
		Log.i(TAG, "Location");
		
		locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		// On privil�gie toujours le GPS
		// Si on trouve des pr�c�dentes localisations dans le gps on les r�cup�res
		if(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null){
			longitude = Double.toString(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude());
			latitude = Double.toString(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude());
		}
		// Sinon on r�cup�re celles du r�seau
		else if (locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null){
			longitude = Double.toString(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude());
			latitude = Double.toString(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude());
		}
		else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
			// Aucune donn�es de loclisation disponible et aucun provider disponible
			Utility.showNotification(context, context.getResources().getString(R.string.notification_ticker_location_error), context.getResources().getString(R.string.notification_title_location_error), context.getResources().getString(R.string.notification_message_location_error), 1);
			Log.e(TAG, "Erreur de loaclisation, localisation impossible");
		}
		//On active les demandes de localisation
		//Si le gps est activ�
		if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, (LocationListener)this);
		}
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 200, 0, (LocationListener)this);
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locationManager.removeUpdates(this);
		
		Looper.loop();
	}
}
