/**
 * 
 */
package bcp.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import bcp.android.service.SynchroPassageService;

/**
 * @author Junior
 *
 */
public class NetworkStateConnectionRevceiver extends BroadcastReceiver {

	private final static String ACTION_CHANGE_STATUS_NETWORK = "android.net.conn.CONNECTIVITY_CHANGE";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(ACTION_CHANGE_STATUS_NETWORK))
		{
			boolean connecte = !intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
			if(connecte)
			{
				Intent i = new Intent(context ,SynchroPassageService.class);
				context.startService(i);
			}
		}
	}
}

