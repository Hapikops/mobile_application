package bcp.android.metier;

import java.util.GregorianCalendar;

/**
 * 
 * <p>
 * Repr�sente un contrat
 * </p>
 * 
 * @author Junior
 *
 */
public class Contrat {
	/** L'id correspond � l'id du partenaire concat�n� au num�ro de contrat */ 
	private String id;
	private String idPartenaire;
	private String numeroContrat;
	private GregorianCalendar dateDernierPassage;
	private GregorianCalendar dateDernierChangement;
	private String idClient;
	private String idEssai;
	
	/**
    * Etat du contrat
    * (Description des valeurs vient de la db)
    * 1 : contrat en attente de validation par ASI
    * 2 : contrat valid� par ASI
    * 3 : contrat non valid� par ASI
    * 4 : contrat en attente de r�siliation par ASI
    * 5 : contrat r�sili� par ASI
    * */
	private String etat;	
	
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public String getIdClient() {
		return idClient;
	}
	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
	public String getId() {
		return id;
	}
	/** L'id du contrat ne doit jamais �tre modifi� (sauf � la lecture du fichier xml envoy� par le webservice */
	public void setId(String id) {
		this.id = id;
	}
	public String getIdPartenaire() {
		return idPartenaire;
	}
	public void setIdPartenaire(String idPartenaire) {
		this.idPartenaire = idPartenaire;
	}
	public String getNumeroContrat() {
		return numeroContrat;
	}
	public void setNumeroContrat(String numeroContrat) {
		this.numeroContrat = numeroContrat;
	}
	public GregorianCalendar getDateDernierPassage() {
		return dateDernierPassage;
	}
	public void setDateDernierPassage(GregorianCalendar dateDernierPassage) {
		this.dateDernierPassage = dateDernierPassage;
	}
	public GregorianCalendar getDateDernierChangement() {
		return dateDernierChangement;
	}
	public void setDateDernierChangement(GregorianCalendar dateDernierChangement) {
		this.dateDernierChangement = dateDernierChangement;
	}
	public String getIdEssai() {
		return idEssai;
	}
	public void setIdEssai(String idEssaie) {
		this.idEssai = idEssaie;
	}
	
	
}
