package bcp.android.metier;

/**
 * 
 * <p>
 * Représente un utilisateur.
 * </p>
 * 
 * @author Junior
 *
 */

public class Utilisateur {
	private String Id, IdPartenaire, login, niveauAcces, password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getIdPartenaire() {
		return IdPartenaire;
	}

	public void setIdPartenaire(String idPartenaire) {
		IdPartenaire = idPartenaire;
	}

	public String getNiveauAcces() {
		return niveauAcces;
	}

	public void setNiveauAcces(String niveauAcces) {
		this.niveauAcces = niveauAcces;
	}
}