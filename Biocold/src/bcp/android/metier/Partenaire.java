package bcp.android.metier;

import java.util.GregorianCalendar;

/**
 * 
 * <p>
 * Représente un partenaire
 * </p>
 * 
 * @author Junior
 *
 */

public class Partenaire {
	private String adresse, baremCom, cp, email, id, nom,
	prenom, pays, tel1, tel2, telfacture, portable, type, ville;
	private GregorianCalendar dateNaiss;
	
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getBaremCom() {
		return baremCom;
	}

	public void setBaremCom(String baremCom) {
		this.baremCom = baremCom;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public GregorianCalendar getDateNaiss() {
		return dateNaiss;
	}

	public void setDateNaiss(GregorianCalendar dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getTelfacture() {
		return telfacture;
	}

	public void setTelfacture(String telfacture) {
		this.telfacture = telfacture;
	}

	public String getPortable() {
		return portable;
	}

	public void setPortable(String portable) {
		this.portable = portable;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}
	@Override
	public String toString() {
		return "Partenaire Adresse=" + adresse + ", baremCom=" + baremCom
				+ ", cp=" + cp + ", dateNaiss=" + dateNaiss + ", email="
				+ email + ", id=" + id + ", nom=" + nom
				+ ", prenom=" + prenom + ", pays=" + pays + ", tel1=" + tel1
				+ ", tel2=" + tel2 + ", telfacture=" + telfacture
				+ ", portable=" + portable + ", type=" + type + ", ville="
				+ ville ;
	}
}
