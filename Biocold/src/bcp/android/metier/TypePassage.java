package bcp.android.metier;

import java.io.Serializable;

/**
 * 
 * <p>
 * Représente un type de passage
 * 
 * Cette classe est remplacée par l'énuméartion des types de passage
 * </p>
 * 
 * @author Junior
 * @author Matthias
 *
 */
@Deprecated
public class TypePassage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String libelleType;
	private String categorie;
	
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the libelleType
	 */
	public String getLibelleType() {
		return libelleType;
	}
	/**
	 * @param libelleType the libelleType to set
	 */
	public void setLibelleType(String libelleType) {
		this.libelleType = libelleType;
	}
	
	@Override
	public String toString() {
		return getLibelleType();
	}
}
