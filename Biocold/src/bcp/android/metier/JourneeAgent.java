package bcp.android.metier;


/**
 * 
 * <p>
 * Repr�sente une journ�e agent
 * </p>
 * 
 * @author Junior
 *
 */

public class JourneeAgent {
	private String tablejourneeagent;
	private String idjourneeagent;
	private String date;
	private String heuredepartmatin;
	private String lieudepartmatin;
	private String heurestopdejeuner;
	private String lieustopdejeuner;
	private String heuredepartmidi;
	private String lieudepartmidi;
	private String heureretour;
	private String lieuretour;
	private String observationmatin;
	private String observationmidi;
	private String totalheure;
	private String heureservice;
	private String nombreclientvisite;
	private String nombreclientchange;
	private String nombreclientinstalle;
	private String nombreclientvisiteparheure;
	private String nombregrandek7change;
	private String nombrepetitek7change;
	private String nombregrandek7installe;
	private String nombrepetitek7installe;
	private String activitematin;
	private String activitemidi;
	private String kmmatin;
	private String kmmidi;
	private String kmparcouru;
	private String nombrerefus;
	private String idAgent;
	
	public String getIdAgent() {
		return idAgent;
	}
	public void setIdAgent(String idAgent) {
		this.idAgent = idAgent;
	}
	public String getTablejourneeagent() {
		return tablejourneeagent;
	}
	public void setTablejourneeagent(String tablejourneeagent) {
		this.tablejourneeagent = tablejourneeagent;
	}
	public String getIdjourneeagent() {
		return idjourneeagent;
	}
	public void setIdjourneeagent(String idjourneeagent) {
		this.idjourneeagent = idjourneeagent;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHeuredepartmatin() {
		return heuredepartmatin;
	}
	public void setHeuredepartmatin(String heuredepartmatin) {
		this.heuredepartmatin = heuredepartmatin;
	}
	public String getLieudepartmatin() {
		return lieudepartmatin;
	}
	public void setLieudepartmatin(String lieudepartmatin) {
		this.lieudepartmatin = lieudepartmatin;
	}
	public String getHeurestopdejeuner() {
		return heurestopdejeuner;
	}
	public void setHeurestopdejeuner(String heurestopdejeuner) {
		this.heurestopdejeuner = heurestopdejeuner;
	}
	public String getLieustopdejeuner() {
		return lieustopdejeuner;
	}
	public void setLieustopdejeuner(String lieustopdejeuner) {
		this.lieustopdejeuner = lieustopdejeuner;
	}
	public String getHeuredepartmidi() {
		return heuredepartmidi;
	}
	public void setHeuredepartmidi(String heuredepartmidi) {
		this.heuredepartmidi = heuredepartmidi;
	}
	public String getLieudepartmidi() {
		return lieudepartmidi;
	}
	public void setLieudepartmidi(String lieudepartmidi) {
		this.lieudepartmidi = lieudepartmidi;
	}
	public String getHeureretour() {
		return heureretour;
	}
	public void setHeureretour(String heureretour) {
		this.heureretour = heureretour;
	}
	public String getLieuretour() {
		return lieuretour;
	}
	public void setLieuretour(String lieuretour) {
		this.lieuretour = lieuretour;
	}
	public String getObservationmatin() {
		return observationmatin;
	}
	public void setObservationmatin(String observationmatin) {
		this.observationmatin = observationmatin;
	}
	public String getObservationmidi() {
		return observationmidi;
	}
	public void setObservationmidi(String observationmidi) {
		this.observationmidi = observationmidi;
	}
	public String getTotalheure() {
		return totalheure;
	}
	public void setTotalheure(String totalheure) {
		this.totalheure = totalheure;
	}
	public String getHeureservice() {
		return heureservice;
	}
	public void setHeureservice(String heureservice) {
		this.heureservice = heureservice;
	}
	public String getNombreclientvisite() {
		return nombreclientvisite;
	}
	public void setNombreclientvisite(String nombreclientvisite) {
		this.nombreclientvisite = nombreclientvisite;
	}
	public String getNombreclientchange() {
		return nombreclientchange;
	}
	public void setNombreclientchange(String nombreclientchange) {
		this.nombreclientchange = nombreclientchange;
	}
	public String getNombreclientinstalle() {
		return nombreclientinstalle;
	}
	public void setNombreclientinstalle(String nombreclientinstalle) {
		this.nombreclientinstalle = nombreclientinstalle;
	}
	public String getNombreclientvisiteparheure() {
		return nombreclientvisiteparheure;
	}
	public void setNombreclientvisiteparheure(String nombreclientvisiteparheure) {
		this.nombreclientvisiteparheure = nombreclientvisiteparheure;
	}
	public String getNombregrandek7change() {
		return nombregrandek7change;
	}
	public void setNombregrandek7change(String nombregrandek7change) {
		this.nombregrandek7change = nombregrandek7change;
	}
	public String getNombrepetitek7change() {
		return nombrepetitek7change;
	}
	public void setNombrepetitek7change(String nombrepetitek7change) {
		this.nombrepetitek7change = nombrepetitek7change;
	}
	public String getNombregrandek7installe() {
		return nombregrandek7installe;
	}
	public void setNombregrandek7installe(String nombregrandek7installe) {
		this.nombregrandek7installe = nombregrandek7installe;
	}
	public String getNombrepetitek7installe() {
		return nombrepetitek7installe;
	}
	public void setNombrepetitek7installe(String nombrepetitek7installe) {
		this.nombrepetitek7installe = nombrepetitek7installe;
	}
	public String getActivitematin() {
		return activitematin;
	}
	public void setActivitematin(String activitematin) {
		this.activitematin = activitematin;
	}
	public String getActivitemidi() {
		return activitemidi;
	}
	public void setActivitemidi(String activitemidi) {
		this.activitemidi = activitemidi;
	}
	public String getKmmatin() {
		return kmmatin;
	}
	public void setKmmatin(String kmmatin) {
		this.kmmatin = kmmatin;
	}
	public String getKmmidi() {
		return kmmidi;
	}
	public void setKmmidi(String kmmidi) {
		this.kmmidi = kmmidi;
	}
	public String getKmparcouru() {
		return kmparcouru;
	}
	public void setKmparcouru(String kmparcouru) {
		this.kmparcouru = kmparcouru;
	}
	public String getNombrerefus() {
		return nombrerefus;
	}
	public void setNombrerefus(String nombrerefus) {
		this.nombrerefus = nombrerefus;
	}
}