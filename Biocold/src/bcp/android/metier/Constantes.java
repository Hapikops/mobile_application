/**
 * 
 */
package bcp.android.metier;

/**
 * @author Junior
 *
 * Cette classe contient toutes les constantes de l'application
 * 
 */
public class Constantes 
{
	/** Nom du fichier de pr�f�rences */
	public static final String PREFS_NAME = "LoginPrefs";
	
	/** Variable contenue dans le fichier de pr�f�rence PREFS_NAME **/
	public static final String ID_LOGGED = "logged";
	public static final String ID_USER = "idUser";
	public static final String ID_PARTENAIRE = "idPartenaire";
	public static final String ID_AGENT = "idAgent";
	
	/** Cette variable sert de cl� pour les Intents (pour passer un passage d'une activit� � une autre) */
	public static final String PASSAGE = "passageAgent";
	/** Cette variable sert de cl� pour les Intents (pour passer une liste de scanners d'une activit� � une autre) */
	public static final String SCANS = "scanners";
}
