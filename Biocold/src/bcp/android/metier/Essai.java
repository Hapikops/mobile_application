package bcp.android.metier;

import java.util.GregorianCalendar;

/**
 * 
 * Represente un Essai 
 * **/
public class Essai {
	
	private String id;
	private String idEssaiPartenaire;
	private String idPartenaire;
	private GregorianCalendar dateDebut;
	private GregorianCalendar dateDernierPassage;
	private GregorianCalendar dateDernierChangement;
	private String idClient;
	private String nbCassette;
	private String observation;
	private boolean newEssai;
	
	public boolean isNewEssai() {
		return newEssai;
	}

	public void setNewEssai(boolean newEssai) {
		this.newEssai = newEssai;
	}

	public String getNbCassette() {
		return nbCassette;
	}

	public void setNbCassette(String nbCassette) {
		this.nbCassette = nbCassette;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getId() {	
		return id;	
	}
	
	public GregorianCalendar getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(GregorianCalendar dateDebut) {
		this.dateDebut = dateDebut;
	}
	
	public String getIdEssaiPartenaire() {
		return idEssaiPartenaire;
	}

	public void setIdEssaiPartenaire(String idEssaiPartenaire) {
		this.idEssaiPartenaire = idEssaiPartenaire;
	}
	
	public GregorianCalendar getDateDernierPassage() {
		return dateDernierPassage;
	}

	public void setDateDernierPassage(GregorianCalendar dateDernierPassage) {
		this.dateDernierPassage = dateDernierPassage;
	}

	public GregorianCalendar getDateDernierChangement() {
		return dateDernierChangement;
	}

	public void setDateDernierChangement(GregorianCalendar dateDernierChangement) {
		this.dateDernierChangement = dateDernierChangement;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public void setId(String idEssai){
		this.id = idEssai;
	}
	
	public String getIdPartenaire(){
		return idPartenaire;
	}
	
	public void setIdPartenaire(String idPartenaire) {
		this.idPartenaire = idPartenaire;
	}

}
