package bcp.android.metier.enums;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;
import bpc.android.biocold.R;

/**
 * 
 * <p>
 * Se sont les diff�rents type de passage qui existe.
 * Doit �tre synchro avec ce que l'on trouve dans la db 
 * </p>
 * 
 * @author Junior
 *
 */
public enum TypePassage implements Serializable {
	REPLACE("C",  R.string.ENUM_TYPE_PASSAGE_REPLACE, 2),
	INSTALL_WITHOUT_REPLACE("D", R.string.ENUM_TYPE_PASSAGE_INSTALL_WITHOUT_REPLACE, 3),
	INSTALL_WITH_REPLACE ("E", R.string.ENUM_TYPE_PASSAGE_INSTALL_WITH_REPLACE, 3),
	REFUSAL_WITH_MADE("F", R.string.ENUM_TYPE_PASSAGE_REFUSAL_WITH_MADE, 3),
	REFUSAL_WITHOUT_MADE("G", R.string.ENUM_TYPE_PASSAGE_REFUSAL_WITHOUT_MADE, 3),
	START_ESSAI("A1", R.string.ENUM_TYPE_PASSAGE_START_ESSAI, 1),
	REPLACE_DURING_ESSAI("A2", R.string.ENUM_TYPE_PASSAGE_REPLACE_DURING_ESSAI, 1),
	RECOVRY_END_ESSAI("A3", R.string.ENUM_TYPE_PASSAGE_RECOVRY_END_ESSAI, 1),
	RECOVRY_END_CONTRACT("A4", R.string.ENUM_TYPE_PASSAGE_RECOVRY_END_CONTRACT, 2);
	
	/** Le code repr�sentant le type */
	private String code;
	/** Le nom complet du type de passage */
	private int libelle;
	/** 
	 * 1 : Essai
	 * 2 : Contrat
	 * 3 : Essai et Contrat
	 * */
	private int categorie;
	
	private static Context context;
	
	private TypePassage(String code, int libelle, int categorie) {
		this.code = code;
		this.libelle = libelle;
		this.categorie = categorie;
	}
	
	public String getCode() {
		return code;
	}
	/**
	 * Permet de r�cup�r� le libell�  d'un type de passage. 
	 * Ce libell� �tant dans le fichier de ressource string, c'est pour cela que l'on a besoin du context pour y avoir acc�s.
	 * @param context Context de l'application
	 * @return Retourne 
	 */
	public String getLibelle(Context context) {
		return context.getString(libelle);
	}
	public int getCategorie() {
		return categorie;
	}
	
	/**
	 * Permet de r�cup�rer une ArrayList de l'�num�ration en fonction d'un param�tre
	 * @param categorie La liste contiendra tout les types de la cat�gorie. 
	 * Si se param�tre vaut 0 alors toutes les cat�gories sont renvoy�es
	 * @return Retourne une ArrayList
	 */
	public static ArrayList<TypePassage> getList(int categorie) {
		ArrayList<TypePassage> listEnumTypePassages = new ArrayList<TypePassage>(Arrays.asList(TypePassage.values()));
		if(categorie == 0)
			return listEnumTypePassages;
		ArrayList<TypePassage> listTypePassages = new ArrayList<TypePassage>();
		for(TypePassage enumTypePassage : listEnumTypePassages) {
			if(enumTypePassage.getCategorie() == categorie)
				listTypePassages.add(enumTypePassage);
		}
		return listTypePassages;
	}
	
	/**
	 * Doit �tre appell�e avant l'utilisation du toString
	 */
	public static void setContext(Context context)
	{
		TypePassage.context = context;
	}
	
	/**
	 * Doit imp�rativement appell� la m�thod setContext(Context context) avant d'�tre utilis�e sinon plantage.
	 * Le context est utilis� pour avoir acc�s aux ressources
	 */
	@Override
	public String toString() {
		return getLibelle(context);
	}
}
