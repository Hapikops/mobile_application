package bcp.android.metier.enums;

import bpc.android.biocold.R;
import android.content.Context;

/**
 * 
 * <p>
 * Se sont les diff�rents �tat que peut prendre une cassette
 * Doit �tre synchro avec ce que l'on trouve dans la db 
 * </p>
 * 
 * @author Junior
 *
 */
public enum EtatK7 {
	PROPRE (1, R.string.ENUM_ETAT_K7_PROPRE),
	SALE (2, R.string.ENUM_ETAT_K7_SALE),
	CONTRAT (3, R.string.ENUM_ETAT_K7_CONTRAT),
	SALE_PARTENAIRE (4, R.string.ENUM_ETAT_K7_SALE_PARTENAIRE),
	SALE_RETOUR_ESAT (5, R.string.ENUM_ETAT_K7_SALE_RETOUR_ESAT),
	INUTILISABLE (6, R.string.ENUM_ETAT_K7_INUTILISABLE),
	PERDUE_CLIENT (7, R.string.ENUM_ETAT_K7_PERDUE_CLIENT),
	SALE_ESAT (8, R.string.ENUM_ETAT_K7_SALE_ESAT),
	PERDUE_PARTENAIRE (9, R.string.ENUM_ETAT_K7_PERDUE_PARTENAIRE),
	SALE_ABIMEE (10, R.string.ENUM_ETAT_K7_SALE_ABIMEE);
	
	private int code;
	private int libelle;
	
	private EtatK7(int code, int lib) {
		this.code = code;
		libelle = lib;
	}
	
	public int getCode(){
		return code;
	}
	
	public String getLibelle(Context context) {
		return context.getString(libelle);
	}
}
