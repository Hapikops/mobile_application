package bcp.android.metier;

import java.io.Serializable;
import java.util.GregorianCalendar;

import bcp.android.metier.enums.EtatK7;

/**
 * 
 * <p>
 * Représente un scan de cassette
 * </p>
 * 
 * @author Junior
 *
 */
public class ScanK7 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idscank7;
	private String longitude;
	private String latitude;
	private GregorianCalendar datesaisie;
	private EtatK7 etatk7;
	private GregorianCalendar etatdate;
	private String codebarre;
	private String idUser;
	private String idPassageAgent;
	
	public String getIdscank7() {
		return idscank7;
	}
	public void setIdscank7(String idscank7) {
		this.idscank7 = idscank7;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public GregorianCalendar getDatesaisie() {
		return datesaisie;
	}
	public void setDatesaisie(GregorianCalendar datesaisie) {
		this.datesaisie = datesaisie;
	}
	public EtatK7 getEtatk7() {
		return etatk7;
	}
	public void setEtatk7(EtatK7 etatk7) {
		this.etatk7 = etatk7;
	}
	public GregorianCalendar getEtatdate() {
		return etatdate;
	}
	public void setEtatdate(GregorianCalendar etatdate) {
		this.etatdate = etatdate;
	}
	public String getCodebarre() {
		return codebarre;
	}
	public void setCodebarre(String codebarre) {
		this.codebarre = codebarre;
	}
	public String getIdPassageAgent() {
		return idPassageAgent;
	}
	public void setIdPassageAgent(String idPassageAgent) {
		this.idPassageAgent = idPassageAgent;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

}
