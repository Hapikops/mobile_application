package bcp.android.metier;

/**
 * 
 * <p>
 * Représente un Client
 * </p>
 * 
 * @author Junior
 *
 */
public class Client {

	private String id;
	private String nomCommerce;
	private String typeCommerce;
	private String civilite;
	private String nom;
	private String prenom;
	private String adresseCommerce;
	private String villeCommerce;
	private String codePostalCommerce;
	private String telephone;
	private String idContrat;
	private String idPartenaire;
	private String idEssai;
	private boolean newClient;
	
	
	public boolean isNewClient() {
		return newClient;
	}
	public void setNewClient(boolean newClient) {
		this.newClient = newClient;
	}
	public String getTypeCommerce() {
		return typeCommerce;
	}
	public void setTypeCommerce(String typeCommerce) {
		this.typeCommerce = typeCommerce;
	}
	public String getCivilite() {
		return civilite;
	}
	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNomCommerce() {
		return nomCommerce;
	}
	public void setNomCommerce(String nomCommerce) {
		this.nomCommerce = nomCommerce;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresseCommerce() {
		return adresseCommerce;
	}
	public void setAdresseCommerce(String adresseCommerce) {
		this.adresseCommerce = adresseCommerce;
	}
	public String getVilleCommerce() {
		return villeCommerce;
	}
	public void setVilleCommerce(String villeCommerce) {
		this.villeCommerce = villeCommerce;
	}
	public String getCodePostalCommerce() {
		return codePostalCommerce;
	}
	public void setCodePostalCommerce(String codePostalCommerce) {
		this.codePostalCommerce = codePostalCommerce;
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
	public String getIdPartenaire() {
		return idPartenaire;
	}
	public void setIdPartenaire(String idPartenaire) {
		this.idPartenaire = idPartenaire;
	}
	public String getIdEssai() {
		return idEssai;
	}
	public void setIdEssai(String idEssai) {
		this.idEssai = idEssai;
	}
}
