package bcp.android.metier;

import java.util.GregorianCalendar;

/**
 * 
 * <p>
 * Représente un agent
 * </p>
 * 
 * @author Junior
 *
 */
public class Agent {
	private String idagent;
	private String nomagent;
	private String prenomagent;
	private String telephone;
	private String telephoneportable;
	private String initialespartenaire;
	private GregorianCalendar dateentree;
	private GregorianCalendar datesortie;
	private String actif;
	private String idUtilisateur;
	
	public String getIdagent() {
		return idagent;
	}
	public void setIdagent(String idagent) {
		this.idagent = idagent;
	}
	public String getNomagent() {
		return nomagent;
	}
	public void setNomagent(String nomagent) {
		this.nomagent = nomagent;
	}
	public String getPrenomagent() {
		return prenomagent;
	}
	public void setPrenomagent(String prenomagent) {
		this.prenomagent = prenomagent;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getTelephoneportable() {
		return telephoneportable;
	}
	public void setTelephoneportable(String telephoneportable) {
		this.telephoneportable = telephoneportable;
	}
	public String getInitialespartenaire() {
		return initialespartenaire;
	}
	public void setInitialespartenaire(String initialespartenaire) {
		this.initialespartenaire = initialespartenaire;
	}
	public GregorianCalendar getDateentree() {
		return dateentree;
	}
	public void setDateentree(GregorianCalendar dateentree) {
		this.dateentree = dateentree;
	}
	public GregorianCalendar getDatesortie() {
		return datesortie;
	}
	public void setDatesortie(GregorianCalendar datesortie) {
		this.datesortie = datesortie;
	}
	public String getActif() {
		return actif;
	}
	public void setActif(String actif) {
		this.actif = actif;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

}
