package bcp.android.metier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import bcp.android.metier.enums.TypePassage;

/**
 * 
 * <p>
 * Représente un passage d'agent
 * </p>
 * 
 * @author Junior
 * @author Matthias
 *
 */

public class Passage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private TypePassage typepassage;
	private GregorianCalendar datepassage;
	private String dureepassage;
	private String observationpassageinterne;
	private String observationpassageclient;
	private String idAgent;
	private String idEssai;
	private String idUtilisateur;
	private List<ScanK7> listScank7;
	private String idContrat;
	private String idEssaiPartenaire;

	public Passage() {
		listScank7 = new ArrayList<ScanK7>();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdEssaiPartenaire() {
		return idEssaiPartenaire;
	}

	public void setIdEssaiPartenaire(String idEssaiPartenaire) {
		this.idEssaiPartenaire = idEssaiPartenaire;
	}
	public TypePassage getTypepassage() {
		return typepassage;
	}
	public void setTypepassage(TypePassage typepassage) {
		this.typepassage = typepassage;
	}
	public GregorianCalendar getDatePassage() {
		return datepassage;
	}
	public void setDatepassage(GregorianCalendar datepassage) {
		this.datepassage = datepassage;
	}
	public String getDureepassage() {
		return dureepassage;
	}
	public void setDureepassage(String dureepassage) {
		this.dureepassage = dureepassage;
	}
	public String getObservationpassageinterne() {
		return observationpassageinterne;
	}
	public void setObservationpassageinterne(String observationpassageinterne) {
		this.observationpassageinterne = observationpassageinterne;
	}
	public String getObservationpassageclient() {
		return observationpassageclient;
	}
	public void setObservationpassageclient(String observationpassageclient) {
		this.observationpassageclient = observationpassageclient;
	}
	public String getIdAgent() {
		return idAgent;
	}
	public void setIdAgent(String idAgent) {
		this.idAgent = idAgent;
	}
	public String getIdEssai() {
		return idEssai;
	}
	public void setIdEssai(String idEssai) {
		this.idEssai = idEssai;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public List<ScanK7> getListScank7() {
		return listScank7;
	}
	public void setListScank7(List<ScanK7> listScank7) {
		this.listScank7 = listScank7;
	}
	public void addScank7(ScanK7 scanK7)
	{
		listScank7.add(scanK7);
	}
	public String getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(String idContrat) {
		this.idContrat = idContrat;
	}
}
