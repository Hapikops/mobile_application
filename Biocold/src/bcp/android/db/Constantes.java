package bcp.android.db;


/**
 * <pre>
 * Cette classe permet de definir toutes les constantes utilisees pour la base de donnee.
 * Ces constantes sont utilisee aussi bien lors de la creation de la base de donnee que 
 * lors de l'utilisation de celle-ci via les adapteurs.
 * <pre>
 * @author Junior
 *
 */
public class Constantes {

	/*
	 * Constantes pour stocker les elements necessaires e la creation 
	 * des differentes tables de la base de donnees
	 */
	/**
	 * Table partenaire : 
	 */
	public static final String TABLE_PARTENAIRE = "partenaire";
	public static final String ID_PARTENAIRE = "_id_partenaire";
	public static final String PARTENAIRE_ADRESSE = "adresse";
	public static final String PARTENAIRE_BAREM = "barem_commission";
	public static final String PARTENAIRE_CP = "code_postal";
	public static final String PARTENAIRE_NAISS = "date_naissance";
	public static final String PARTENAIRE_MAIL = "email";
	public static final String PARTENAIRE_NOM = "nom";
	public static final String PARTENAIRE_PRENOM = "prenom";
	public static final String PARTENAIRE_PAYS = "pays";
	public static final String PARTENAIRE_TEL1 = "tel1";
	public static final String PARTENAIRE_TEL2 = "tel2";
	public static final String PARTENAIRE_TELFACT = "tel_facture";
	public static final String PARTENAIRE_TELPORT = "tel_portable";
	public static final String PARTENAIRE_TYPE = "code_type";
	public static final String PARTENAIRE_VILLE = "ville";
	/**
	 * Table utilisateur
	 */
	public static final String TABLE_UTILISATEUR = "utilisateur";
	public static final String ID_UTILISATEUR = "_id_utilisateur";
	public static final String UTILISATEUR_LOGIN = "login";
	public static final String UTILISATEUR_PASSWORD = "password";
	public static final String UTILISATEUR_NIVEAUACCES = "niveau_acces";
	/**
	 * Table agent
	 */
	public static final String TABLE_AGENT = "agent";
	public static final String ID_AGENT = "_id_agent";
	public static final String AGENT_NOM = "nom";
	public static final String AGENT_PRENOM = "prenom";
	public static final String AGENT_TELEPHONE = "telephone";
	public static final String AGENT_TELEPHONEPORTABLE = "telephone_agent";
	public static final String AGENT_INITIALESPARTENAIRE = "initiales_partenaire";
	public static final String AGENT_DATEENTREE = "date_entree";
	public static final String AGENT_DATESORTIE = "date_sortie";
	public static final String AGENT_ACTIF = "actif";
	/**
	 * Table journee_agent
	 * Est inutilis�e et peut-�tre ne le sera jamais. Mais le travaille de cr�ation de cette table ayant �t� fait
	 * on ne va pas le supprimer surtout que �a ne g�ne pas.
	 * Attention vu l'inutilit� de cette table actuellement elle est mise en commentaire. Et la nomenclature n'a pas �t� mise � jour. 
	 */
	/*
	public static final String TABLE_JOURNEE_AGENT = "journee_agent";
	public static final String IDJOURNEEAGENT = "_id_journee_agent";
	public static final String DATE = "date";
	public static final String HEUREDEPARTMATIN = "heure_depart_matin";
	public static final String LIEUDEPARTMATIN = "lieu_depart_matin";
	public static final String HEURESTOPDEJEUNER = "heure_stop_dejeuner";
	public static final String LIEUSTOPDEJEUNER = "lieu_stop_dejeuner";
	public static final String HEUREDEPARTMIDI = "heure_depart_midi";
	public static final String LIEUDEPARTMIDI = "lieu_depart_midi";
	public static final String HEURERETOUR = "heure_retour";
	public static final String LIEURETOUR = "lieu_retour";
	public static final String OBSERVATIONMATIN = "observation_matin";
	public static final String OBSERVATIONMIDI = "observation_midi";
	public static final String TOTALHEURE = "tot_heure";
	public static final String HEURESERVICE = "heure_service";
	public static final String NOMBRECLIENTVISITE = "nb_client_visite";
	public static final String NOMBRECLIENTCHANGE = "nb_client_change";
	public static final String NOMBRECLIENTINSTALLE = "nb_client_installe";
	public static final String NOMBRECLIENTVISITEPARHEURE = "nb_client_visite_par_heure";
	public static final String NOMBREGRANDEK7CHANGE = "nb_grande_k7_change";
	public static final String NOMBREPETITEK7CHANGE = "nb_petite_k7_change";
	public static final String NOMBREGRANDEK7INSTALLE = "nb_grande_k7_installe";
	public static final String NOMBREPETITEK7INSTALLE = "nb_petite_k7_installe";
	public static final String ACTIVITEMATIN = "activite_matin";
	public static final String ACTIVITEMIDI = "activite_midi";
	public static final String KMMATIN = "km_matin";
	public static final String KMMIDI = "km_midi";
	public static final String KMPARCOURU = "km_parcouru";
	public static final String NOMBREREFUS = "nb_refus";
	*/
	/**
	 * Table passage_agent
	 */
	public static final String TABLE_PASSAGE = "passage";
	public static final String ID_PASSAGE_AGENT = "_id_passage";
	public static final String PASSAGE_TYPE = "type_passage";
	public static final String PASSAGE_DATE = "date_passage";
	public static final String PASSAGE_DUREE = "duree_passage";
	public static final String PASSAGE_OBSERVATION_INTERNE = "observation_passage_interne";
	public static final String PASSAGE_OBSERVATION_CLIENT = "observation_passage_client";
	/**
	 * Table scan_k7
	 */
	public static final String TABLE_SCAN_K7 = "scan_k7";
	public static final String ID_SCAN_K7 = "_id_scan_k7";
	public static final String SCAN_K7_CODE = "code";
	public static final String SCAN_K7_LONGITUDE = "longitude";
	public static final String SCAN_K7_LATITUDE = "latitude";
	public static final String SCAN_K7_DATE_SAISIE = "date_saisie";
	public static final String SCAN_K7_ETATK7 = "etat_k7";
	public static final String SCAN_K7_ETAT_DATE = "etat_date";
	/**
	 * Table contrat
	 */
	public static final String TABLE_CONTRAT = "contrat";
	public static final String ID_CONTRAT = "_id_contrat";
	public static final String IDESSAI = "id_essai";
	/**
	 * Table type de passage
	 * Est inutilis�e et peut-�tre ne le sera jamais. Mais le travaille de cr�ation de cette table ayant �t� fait
	 * on ne va pas le supprimer surtout que �a ne g�ne pas.
	 * Attention vu l'inutilit� de cette table actuellement elle est mise en commentaire. Et la nomenclature n'a pas �t� mise � jour. 
	 */
	/*
	public static final String TABLE_TYPE_PASSAGE = "type_passage";
	public static final String ID_TYPEPASSAGE = "_id_typepassage";
	public static final String LIBELLE_TYPEPASSAGE = "value";
	public static final String CATEGORIE_TYPEPASSAGE = "categorie";
	*/
	/**
	 * Table Essai
	 */
	public static final String TABLE_ESSAI = "essai";
	public static final String ID_ESSAI = "_id_essai";
	public static final String ESSAI_OBSERVATION = "observation";
	public static final String ESSAI_NB_CASSETTE = "nombre_cassette";
	public static final String ESSAI_DATE_DEBUT = "date_debut";
	public static final String ESSAI_ID_PARTENAIRE = "id_essai_partenaire";
	public static final String ESSAI_NEW_FLAG = "new_flag";
	
	/**
	 * Table type de commerce
	 */
	public static final String TABLE_TYPECOMMERCE = "type_commerce";
	public static final String ID_TYPECOMMERCE = "_id_typecommerce";
	public static final String TYPECOMMERCE_LIBELLE = "value";
	
	/**
	 * Table client
	 */
	public static final String TABLE_CLIENT = "client";
	public static final String ID_CLIENT = "_id_client";
	public static final String CLIENT_NOM_COMMERCE = "nom_commerce";
	public static final String CLIENT_TYPE_COMMERCE = "type_commerce";
	public static final String CLIENT_CIVILITE = "civilite";
	public static final String CLIENT_NOM = "nom";
	public static final String CLIENT_PRENOM = "prenom";
	public static final String CLIENT_ADRESSE = "adresse";
	public static final String CLIENT_VILLE = "ville";
	public static final String CLIENT_CODEPOSTAL = "codepostal";
	public static final String CLIENT_TELEPHONE = "telephone";
	public static final String CLIENT_NEW_FLAG = "new_flag";
	
	/*
	 * Cles etrangeres
	 * Toutes les cles etrangeres sont placees ici car elles peuvent etre utilisees dans plusieurs tables e la fois
	 */
	/** Cette cle etrangere est utilisee dans les tables : Agent, Utilisateur, ScanK7 */
	public static final String FK_IDPARTENAIRE = "id_partenaire";
	/** Cette cle etrangere est utilisee dans les tables : PassageAgent, ScanK7, Agent */
	public static final String FK_IDUSER = "id_utilisateur";
	/** Cette cle etrangere est utilisee dans la table : ScanK7 */
	public static final String FK_IDPASSAGEAGENT = "id_passage_agent";
	/** Cette cle etrangere est utilisee dans les tables : PassageAgent et JourneeAgent */
	public static final String FK_IDAGENT = "id_agent";
	/** Cette cle etrangere est utilisee dans les tables : essai */
	public static final String FK_IDCLIENT = "id_client";
	
}
