package bcp.android.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class ConnectionDB{
	
	/** Pour g�rer le fait que cette classe soit un singleton */
	private static SQLiteDatabase dataBase = null;
	
	private static DatabaseHelper databaseHelper = null;
	
	/** Nom de la base de donn�es */
	private static final String DB_NAME = "biocold";
	/** Num�ro de version de la base de donn�es */
	private static final int DB_VERSION = 1;
	
	
	private ConnectionDB() {
	}
	
	/** 
	 * Pour r�cup�rer l'instance de cette classe
	 **/
	public static SQLiteDatabase getInstance(Context context)
	{
		if(dataBase == null)
		{
			databaseHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
			dataBase =  databaseHelper.getWritableDatabase();
		}
		
	    while(dataBase.isDbLockedByCurrentThread()) {
	        //db is locked, keep looping
	    }
		return dataBase;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		dataBase.close();
	}
}
