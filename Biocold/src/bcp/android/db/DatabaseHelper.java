package bcp.android.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * <p>
 * Cette classe permet de creer la base de donne local
 * </p>
 * @author Junior
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	/** Permet de garder en memoire le contexte de l'application sur laquel est base la database*/
	@SuppressWarnings("unused")
	private Context context;

	/** Requete de Creation de la table partenaire */
	private static final String CREATE_TABLE_PARTENAIRE = 
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_PARTENAIRE + " (" +
					Constantes.ID_PARTENAIRE + " TEXT PRIMARY KEY, " + 
					Constantes.PARTENAIRE_ADRESSE + " TEXT, " +
					Constantes.PARTENAIRE_BAREM + " TEXT, " +
					Constantes.PARTENAIRE_CP + " TEXT, " +
					Constantes.PARTENAIRE_NAISS + " TEXT, " + 
					Constantes.PARTENAIRE_MAIL + " TEXT, " +
					Constantes.PARTENAIRE_NOM +	" TEXT, " + 
					Constantes.PARTENAIRE_PRENOM + " TEXT, " + 
					Constantes.PARTENAIRE_PAYS + " TEXT, " + 
					Constantes.PARTENAIRE_TEL1 + " TEXT, " + 
					Constantes.PARTENAIRE_TEL2 + " TEXT, " +
					Constantes.PARTENAIRE_TELFACT + " TEXT, " + 
					Constantes.PARTENAIRE_TELPORT + " TEXT, " + 
					Constantes.PARTENAIRE_TYPE + " TEXT,  " +
					Constantes.PARTENAIRE_VILLE + " TEXT);";
	/** Requete de Creation de la table utilisateur */
	private static final String CREATE_TABLE_UTILISATEUR =
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_UTILISATEUR + " (" +
					Constantes.ID_UTILISATEUR + " TEXT PRIMARY KEY, " +
					Constantes.UTILISATEUR_LOGIN + " TEXT, " +
					Constantes.UTILISATEUR_PASSWORD + " TEXT, " +
					Constantes.UTILISATEUR_NIVEAUACCES + " TEXT, " +
					Constantes.FK_IDPARTENAIRE + " TEXT NOT NULL, " +
					"FOREIGN KEY("+Constantes.FK_IDPARTENAIRE+") REFERENCES " + Constantes.TABLE_PARTENAIRE +"(" + Constantes.ID_PARTENAIRE + ") );";
	/** Requete de Creation de la table agent */
	private static final String CREATE_TABLE_AGENT =
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_AGENT + " (" + 
					Constantes.ID_AGENT + " TEXT PRIMARY KEY, " +
					Constantes.AGENT_NOM + " TEXT, " +
					Constantes.AGENT_PRENOM + " TEXT, " +
					Constantes.AGENT_TELEPHONE + " TEXT, " +
					Constantes.AGENT_TELEPHONEPORTABLE + " TEXT, " +
					Constantes.AGENT_INITIALESPARTENAIRE + " TEXT, " + 
					Constantes.AGENT_DATEENTREE + " TEXT, " +
					Constantes.AGENT_DATESORTIE + " TEXT, " + 
					Constantes.AGENT_ACTIF + " TEXT, " +
					Constantes.FK_IDPARTENAIRE + " TEXT NOT NULL, " +					
					Constantes.FK_IDUSER + " TEXT NOT NULL, " +
					"FOREIGN KEY("+Constantes.FK_IDPARTENAIRE+") REFERENCES " + Constantes.TABLE_PARTENAIRE +"(" + Constantes.ID_PARTENAIRE + "), " +
					"FOREIGN KEY("+Constantes.FK_IDUSER+") REFERENCES " + Constantes.TABLE_UTILISATEUR +"(" + Constantes.ID_UTILISATEUR + ") );";
	/** Requete de Creation de la table journee_agent */
	/*
	private static final String CREATE_TABLE_JOURNEE_AGENT =
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_JOURNEE_AGENT + " (" + 
					Constantes.IDJOURNEEAGENT + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					Constantes.DATE + " TEXT, " +
					Constantes.HEUREDEPARTMATIN + " TEXT, " + 
					Constantes.LIEUDEPARTMATIN + " TEXT, " +
					Constantes.HEURESTOPDEJEUNER + " TEXT, " +
					Constantes.LIEUSTOPDEJEUNER + " TEXT, " +
					Constantes.HEUREDEPARTMIDI + " TEXT, " +
					Constantes.LIEUDEPARTMIDI + " TEXT, " +
					Constantes.HEURERETOUR + " TEXT, " +
					Constantes.LIEURETOUR + " TEXT, " + 
					Constantes.OBSERVATIONMATIN + " TEXT, " + 
					Constantes.OBSERVATIONMIDI + " TEXT, " +
					Constantes.TOTALHEURE + " TEXT, " +
					Constantes.HEURESERVICE + " TEXT, " +
					Constantes.NOMBRECLIENTVISITE + " TEXT, " +
					Constantes.NOMBRECLIENTCHANGE + " TEXT, " +
					Constantes.NOMBRECLIENTINSTALLE + " TEXT, " +
					Constantes.NOMBRECLIENTVISITEPARHEURE + " TEXT, " +
					Constantes.NOMBREGRANDEK7CHANGE + " TEXT, " + 
					Constantes.NOMBREPETITEK7CHANGE + " TEXT, " +
					Constantes.NOMBREGRANDEK7INSTALLE + " TEXT, " +	
					Constantes.NOMBREPETITEK7INSTALLE + " TEXT, " + 
					Constantes.ACTIVITEMATIN + " TEXT, " +
					Constantes.ACTIVITEMIDI + " TEXT, " + 
					Constantes.KMMATIN + " TEXT, " + 
					Constantes.KMMIDI + " TEXT, " + 
					Constantes.KMPARCOURU + " TEXT, " +
					Constantes.NOMBREREFUS + " TEXT, " +
					Constantes.FK_IDAGENT + " TEXT NOT NULL, " + 
					"FOREIGN KEY("+Constantes.FK_IDAGENT+") REFERENCES " + Constantes.TABLE_AGENT +"(" + Constantes.IDAGENT + ") );";
	*/
	/** Requete de Creation de la table passage_agent */
	private static final String CREATE_TABLE_PASSAGE =
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_PASSAGE + " (" + 
					Constantes.ID_PASSAGE_AGENT + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					Constantes.IDESSAI + " TEXT NOT NULL, " +
					Constantes.PASSAGE_TYPE + " TEXT, " +
					Constantes.PASSAGE_DATE + " TEXT, " +
					Constantes.PASSAGE_DUREE + " TEXT, " +
					Constantes.PASSAGE_OBSERVATION_INTERNE + " TEXT, " +
					Constantes.PASSAGE_OBSERVATION_CLIENT + " TEXT, " +
					Constantes.FK_IDUSER + " TEXT NOT NULL, " +
					Constantes.FK_IDAGENT + " TEXT NOT NULL, "+
					"FOREIGN KEY("+Constantes.FK_IDUSER+") REFERENCES " + Constantes.TABLE_UTILISATEUR +"(" + Constantes.ID_UTILISATEUR + "), " +
					"FOREIGN KEY("+Constantes.FK_IDAGENT+") REFERENCES " + Constantes.TABLE_AGENT +"(" + Constantes.ID_AGENT + ") );";
	/** Requete de Creation de la table scan_k7 */
	private static final String CREATE_TABLE_SCAN_K7 =
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_SCAN_K7 + " (" +
					Constantes.ID_SCAN_K7 + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					Constantes.SCAN_K7_CODE + " TEXT, " +
					Constantes.SCAN_K7_LONGITUDE + " TEXT, " +
					Constantes.SCAN_K7_LATITUDE + " TEXT, " +
					Constantes.SCAN_K7_DATE_SAISIE + " TEXT, " +
					Constantes.SCAN_K7_ETATK7 + " TEXT, " +
					Constantes.SCAN_K7_ETAT_DATE + " TEXT, " +
					Constantes.FK_IDUSER + " TEXT NOT NULL, " +
					Constantes.FK_IDPASSAGEAGENT + " TEXT NOT NULL, " +
					"FOREIGN KEY("+Constantes.FK_IDUSER+") REFERENCES " + Constantes.TABLE_UTILISATEUR +"(" + Constantes.ID_UTILISATEUR + "), " +
					"FOREIGN KEY("+Constantes.FK_IDPASSAGEAGENT+") REFERENCES " + Constantes.TABLE_PASSAGE +"(" + Constantes.ID_PASSAGE_AGENT + ") );";
	/** Requete de Creation de la table contrat */
	private static final String CREATE_TABLE_CONTRAT = 
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_CONTRAT + " (" +
					Constantes.ID_CONTRAT + " TEXT PRIMARY KEY, " +
					Constantes.IDESSAI + " TEXT, " +
					Constantes.FK_IDPARTENAIRE + " TEXT NOT NULL, " +
					"FOREIGN KEY("+Constantes.FK_IDPARTENAIRE+") REFERENCES " + Constantes.TABLE_PARTENAIRE +"(" + Constantes.ID_PARTENAIRE + ") );";
	/** Requete de Creation de la table type_passage */
	/*
	private static final String CREATE_TABLE_TYPE_PASSAGE_AGENT = 
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_TYPE_PASSAGE + " (" + 
					Constantes.ID_TYPEPASSAGE + " TEXT PRIMARY KEY, " +
					Constantes.CATEGORIE_TYPEPASSAGE + " TEXT, " +
					Constantes.LIBELLE_TYPEPASSAGE + " TEXT ); ";
	*/
	/** Requete de Creation de la table essai */
	private static final String CREATE_TABLE_ESSAI =
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_ESSAI + "(" +
					Constantes.ID_ESSAI + " TEXT PRIMARY KEY, " +
					Constantes.ESSAI_OBSERVATION + " TEXT, " +
					Constantes.ESSAI_NB_CASSETTE + " TEXT, " +
					Constantes.ESSAI_DATE_DEBUT + " TEXT, " +
					Constantes.ESSAI_ID_PARTENAIRE + " TEXT, " +
					Constantes.ESSAI_NEW_FLAG + " TEXT, " +
					Constantes.FK_IDCLIENT + " TEXT, " +
					Constantes.FK_IDPARTENAIRE + " TEXT NOT NULL, " +
					"FOREIGN KEY("+Constantes.FK_IDCLIENT+") REFERENCES " + Constantes.TABLE_CLIENT +"(" + Constantes.ID_CLIENT + "), " +
					"FOREIGN KEY("+Constantes.FK_IDPARTENAIRE+") REFERENCES " + Constantes.TABLE_PARTENAIRE + "(" + Constantes.ID_PARTENAIRE + ") );";
	/**Requette de Creation de la table type de commerce */
	private static final String CREATE_TABLE_TYPECOMMERCE = 
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_TYPECOMMERCE + "(" +
					Constantes.ID_TYPECOMMERCE + "TEXT PRIMARY KEY, " +
					Constantes.TYPECOMMERCE_LIBELLE + " TEXT ); ";
	/**Requette de Creation de la table client */
	private static final String CREATE_TABLE_CLIENT = 
			"CREATE TABLE IF NOT EXISTS " + Constantes.TABLE_CLIENT + "(" +
					Constantes.ID_CLIENT + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					Constantes.CLIENT_NOM_COMMERCE + " TEXT, " +
					Constantes.CLIENT_TYPE_COMMERCE + " TEXT, " +
					Constantes.CLIENT_CIVILITE + " TEXT, " +
					Constantes.CLIENT_NOM + " TEXT, " +
					Constantes.CLIENT_PRENOM + " TEXT, " +
					Constantes.CLIENT_ADRESSE + " TEXT, " +
					Constantes.CLIENT_NEW_FLAG + " TEXT, " +
					Constantes.CLIENT_VILLE + " TEXT, " +
					Constantes.CLIENT_CODEPOSTAL + " TEXT, " +
					Constantes.CLIENT_TELEPHONE + " TEXT ); ";

	/** 
	 * Constructeur de la classe 
	 * @param context
	 * @param name : Nom de la base de donnees
	 * @param factory
	 * @param version : Numero de version de la base de donnees
	 */
	public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
	
	/** Methode appelee a chaque fois que la base de donnees n'existe pas */
	@Override
	public void onCreate(SQLiteDatabase db) {
		//on cree la table a partir de la requete ecrite dans la variable CREATE_BDD
		db.execSQL(CREATE_TABLE_PARTENAIRE);
		db.execSQL(CREATE_TABLE_UTILISATEUR);
		db.execSQL(CREATE_TABLE_AGENT);
		//db.execSQL(CREATE_TABLE_JOURNEE_AGENT);
		db.execSQL(CREATE_TABLE_PASSAGE);
		db.execSQL(CREATE_TABLE_SCAN_K7);
		db.execSQL(CREATE_TABLE_CONTRAT);
		//db.execSQL(CREATE_TABLE_TYPE_PASSAGE);
		db.execSQL(CREATE_TABLE_ESSAI);
		db.execSQL(CREATE_TABLE_CLIENT);
		db.execSQL(CREATE_TABLE_TYPECOMMERCE);
	}
	/** 
	 * Methode appelee au changement de numero de version de la base de donnees
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// On supprime les anciennes tables
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_PARTENAIRE);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_UTILISATEUR);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_AGENT);
		//db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_JOURNEE_AGENT);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_PASSAGE);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_SCAN_K7);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_CONTRAT);
		//db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_TYPE_PASSAGE);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_ESSAI);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_CLIENT);
		db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE_TYPECOMMERCE);

		// On cree les nouvelles tables
		onCreate(db);
	}
}