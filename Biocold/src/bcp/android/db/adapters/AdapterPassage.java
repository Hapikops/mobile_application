package bcp.android.db.adapters;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.Passage;
import bcp.android.metier.enums.TypePassage;

/**
 * <p>
 * Adapteur de la classe PassageAgent pour travailler avec la base de donn�es
 * Sqlite en local sur le t�l�phone
 * </p>
 * @author Matthias Paulmier
 *
 */
public class AdapterPassage {
	/** Pour les log */
	final static String TAG = "BIOCOLD_AdapterPassage";
	
	/** Cr�ation de notre objet qui permet le lien avec la base de donn�e */
	private  SQLiteDatabase dataBase = null;
	
	private Context context;
	
	public AdapterPassage(Context context) {
		this.context = context;
		dataBase = ConnectionDB.getInstance(context);
	}
	
	/**
	 * M�thode qui permet d'ins�rer dans la base de donn�es un passageAgent pass� en param�tres 
	 * @param context
	 * @param passageAgent
	 */
	public long insert(Passage passageAgent){
		Log.i(TAG, "Enregistrement d'un passage");
		long rowId = -1;
		
		AdapterScanK7 adapterScanK7 = new AdapterScanK7(context);
		try{

			// On met dans cette HashMap les informations � ins�rer
			ContentValues valeurs = new ContentValues();
			// On ajoute l'id
			valeurs.put(Constantes.ID_PASSAGE_AGENT, passageAgent.getId());
			valeurs.put(Constantes.PASSAGE_TYPE, passageAgent.getTypepassage().name());
			valeurs.put(Constantes.PASSAGE_DATE, passageAgent.getDatePassage().getTimeInMillis());
			valeurs.put(Constantes.PASSAGE_DUREE, passageAgent.getDureepassage());
			valeurs.put(Constantes.PASSAGE_OBSERVATION_INTERNE, passageAgent.getObservationpassageinterne());
			valeurs.put(Constantes.FK_IDUSER, passageAgent.getIdUtilisateur());
			valeurs.put(Constantes.FK_IDAGENT, passageAgent.getIdAgent());
			valeurs.put(Constantes.IDESSAI, passageAgent.getIdEssai());
			
			rowId = dataBase.insert(Constantes.TABLE_PASSAGE, null, valeurs);
			adapterScanK7.insert(passageAgent.getListScank7(), Long.toString(rowId));
		}
		catch(Exception e){
			Log.e(TAG, e.toString());
		}
		return rowId;
	}
	
	/**
	 * Permet de r�cup�rer tout les passage en base de donn�e. Attention �a r�cup�re �galement tout les scans li� � chaque passage
	 * @return Liste de passage d'agent et leur scans respectif.
	 */
	public List<Passage> list()
	{
		Log.i(TAG, "R�cup�ration de la liste des passages");
		
		List<Passage> listPassageAgent = null;
		Passage passageAgent = null;
		AdapterScanK7 adapterScanK7 = null;
		
		try{
			// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
			String[] colonnes = {Constantes.ID_PASSAGE_AGENT, Constantes.PASSAGE_TYPE, Constantes.PASSAGE_DATE, Constantes.PASSAGE_DUREE, Constantes.PASSAGE_OBSERVATION_INTERNE, Constantes.PASSAGE_OBSERVATION_CLIENT, Constantes.IDESSAI, Constantes.FK_IDAGENT, Constantes.FK_IDUSER};
			Cursor cursor = dataBase.query(Constantes.TABLE_PASSAGE, colonnes, null, null, null, null, null, null);
			
			if(cursor.getCount() != 0)
			{
				listPassageAgent = new ArrayList<Passage>();
				adapterScanK7 = new AdapterScanK7(context);
			}
			while(cursor.moveToNext())
			{
				passageAgent = new Passage();
				GregorianCalendar gregorianCalendar = new GregorianCalendar();
				
				//Log.i(TAG, "D�but de mise dans l'objet");
				gregorianCalendar.setTimeInMillis(Long.parseLong(cursor.getString(cursor.getColumnIndex(Constantes.PASSAGE_DATE))));
				passageAgent.setDatepassage(gregorianCalendar);
				passageAgent.setDureepassage(cursor.getString(cursor.getColumnIndex(Constantes.PASSAGE_DUREE)));;
				passageAgent.setIdAgent(cursor.getString(cursor.getColumnIndex(Constantes.FK_IDAGENT)));
				passageAgent.setIdEssai(cursor.getString(cursor.getColumnIndex(Constantes.IDESSAI)));
				passageAgent.setId(cursor.getString(cursor.getColumnIndex(Constantes.ID_PASSAGE_AGENT)));
				passageAgent.setIdUtilisateur(cursor.getString(cursor.getColumnIndex(Constantes.FK_IDUSER)));
				passageAgent.setListScank7(adapterScanK7.list(Integer.parseInt(passageAgent.getId())));
				passageAgent.setObservationpassageclient(cursor.getString(cursor.getColumnIndex(Constantes.PASSAGE_OBSERVATION_CLIENT)));
				passageAgent.setObservationpassageinterne(cursor.getString(cursor.getColumnIndex(Constantes.PASSAGE_OBSERVATION_INTERNE)));
				passageAgent.setTypepassage(TypePassage.valueOf((cursor.getString(cursor.getColumnIndex(Constantes.PASSAGE_TYPE)))));
				//Log.i(TAG, "Fin de mise dans l'objet");
				
				listPassageAgent.add(passageAgent);
			}
		}
		catch(Exception e){
			Log.e(TAG, e.toString());
		}
		
		return listPassageAgent;
	}
	
	
	public void delete(int id)
	{
		// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
		String[] paramSelcetion = {String.valueOf(id)};
		
		try
		{
			new AdapterScanK7(context).delete(id);
			dataBase.delete(Constantes.TABLE_PASSAGE, Constantes.ID_PASSAGE_AGENT+ "= ?", paramSelcetion);
			Log.i(TAG, "Suppression d'un passage agent");
		}
		catch(Exception e)
		{
			Log.e(TAG, e.toString());
		}
	}
	
	/**
	 * Permet de connaitre le nombre de record dans la table PassageAgent .
	 * @return Retourne un entier qui inidique le nombre de record dans la table en db local.
	 */
	public int count()
	{		
		int nbRecord = 0;
		
		try{
			// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
			String[] colonnes = {Constantes.ID_PASSAGE_AGENT};
			Cursor cursor = dataBase.query(Constantes.TABLE_PASSAGE, colonnes, null, null, null, null, null, null);
			
			nbRecord = cursor.getCount();
		}
		catch(Exception e){
			Log.e(TAG, e.toString());
		}
		
		return nbRecord;
	}
	
	
}
