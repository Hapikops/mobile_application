package bcp.android.db.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.Partenaire;

/**
 * <p>
 * Adapteur de la classe Partenaire pour travailler avec la base de donn�es
 * Sqlite en local sur le t�l�phone
 * </p>
 * @author Matthias Paulmier
 *
 */
public class AdapterPartenaire {

	/** Cr�ation de notre objet qui permet le lien avec la base de donn�e */
	private  SQLiteDatabase dataBase = null;

	public AdapterPartenaire(Context context) {
		dataBase = ConnectionDB.getInstance(context);
	}
	
	/**
	 * M�thode qui permet d'ins�rer dans la base de donn�es un passageAgent pass� en param�tres
	 * @param scan
	 */
	public long insert(Partenaire partenaire){
		// On met dans cette Collection les informations � ins�rer
		ContentValues valeurs = new ContentValues();
		// On ajoute l'id
		valeurs.put(Constantes.ID_PARTENAIRE, partenaire.getId());
		// ...
		valeurs.put(Constantes.PARTENAIRE_ADRESSE, partenaire.getAdresse());
		valeurs.put(Constantes.PARTENAIRE_BAREM, partenaire.getBaremCom());
		valeurs.put(Constantes.PARTENAIRE_CP, partenaire.getCp());
		valeurs.put(Constantes.PARTENAIRE_NAISS, partenaire.getDateNaiss().getTimeInMillis());
		valeurs.put(Constantes.PARTENAIRE_MAIL, partenaire.getEmail());
		valeurs.put(Constantes.PARTENAIRE_NOM, partenaire.getNom());
		valeurs.put(Constantes.PARTENAIRE_PRENOM, partenaire.getPrenom());
		valeurs.put(Constantes.PARTENAIRE_PAYS, partenaire.getPays());
		valeurs.put(Constantes.PARTENAIRE_TEL1, partenaire.getTel1());
		valeurs.put(Constantes.PARTENAIRE_TEL2, partenaire.getTel2());
		valeurs.put(Constantes.PARTENAIRE_TELFACT, partenaire.getTelfacture());
		valeurs.put(Constantes.PARTENAIRE_TELPORT, partenaire.getPortable());
		valeurs.put(Constantes.PARTENAIRE_TYPE, partenaire.getType());
		valeurs.put(Constantes.PARTENAIRE_VILLE, partenaire.getVille());

		return dataBase.insert(Constantes.TABLE_PARTENAIRE, null, valeurs);
	}
}
