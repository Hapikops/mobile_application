package bcp.android.db.adapters;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.Contrat;

/**
 * <p>
 * Adapteur de la classe Agent pour travailler avec la base de donn�es
 * Sqlite en local sur le t�l�phone
 * </p>
 * @author Matthias Paulmier
 *
 */
public class AdapterContrat{

	/** Cr�ation de notre objet qui permet le lien avec la base de donn�e */
	private  SQLiteDatabase dataBase = null;

	
	public AdapterContrat(Context context) {
		dataBase = ConnectionDB.getInstance(context);
	}

	/**
	 * M�thode qui permet d'ins�rer dans la base de donn�es un passageAgent pass� en param�tres 
	 * @param context
	 * @param agent
	 */
	public void insert(List<Contrat> lesContrats){
		try{
			for(Contrat contrat : lesContrats){
				// On met dans cette Collection les informations � ins�rer
				ContentValues valeurs = new ContentValues();
				
				valeurs.put(Constantes.ID_CONTRAT, contrat.getId());
				valeurs.put(Constantes.FK_IDPARTENAIRE, contrat.getIdPartenaire());
				valeurs.put(Constantes.IDESSAI, contrat.getIdEssai());
				
				// On appel la m�thode d'insetion sur l'objet maBaseSQL
				dataBase.insert(Constantes.TABLE_CONTRAT, null, valeurs);
			}
		}catch(Exception e){
			Log.e("Biocold", e.toString());
		}
		
	}

	/**
	 * M�thode de selection qui renvoie tous un contrat en fonction de son id

	 * @param idContract ID du contrat a r�cup�rer
	 * @return Null ou un Contrat
	 */
	public Contrat find(String idContract){
		// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
		String[] colonnes = {Constantes.ID_CONTRAT, Constantes.FK_IDPARTENAIRE, Constantes.IDESSAI};
		String[] paramSelcetion = {idContract};
		Cursor cursor = dataBase.query(Constantes.TABLE_CONTRAT, colonnes, Constantes.ID_CONTRAT + " = ?", paramSelcetion, null, null, null, null);
		
		Contrat contrat = null;
		
		if(cursor.moveToFirst())
		{			
			contrat = new Contrat();
			contrat.setId(cursor.getString(cursor.getColumnIndex(Constantes.ID_CONTRAT)));
			contrat.setIdPartenaire(cursor.getString(cursor.getColumnIndex(Constantes.FK_IDPARTENAIRE)));
			contrat.setIdEssai(cursor.getString(cursor.getColumnIndex(Constantes.IDESSAI)));
		}		
		return contrat;
	}
	
	/**
	 * M�thode qui permet de supprimer tous les contrats  de la base de donn�es 
	 * (utilis�e par le service de synchro afin de mettre � jout la liste de contrats pour le moment)
	 * 
	 */
	public void deleteAll(){
		dataBase.delete(Constantes.TABLE_CONTRAT, null, null);
	}
}
