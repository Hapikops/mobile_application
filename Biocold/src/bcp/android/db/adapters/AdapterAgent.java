package bcp.android.db.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.Agent;

/**
 * <p>
 * Adapteur de la classe Agent pour travailler avec la base de donn�es
 * Sqlite en local sur le t�l�phone
 * </p>
 * 
 * @author Matthias Paulmier
 *
 */
public class AdapterAgent {
	/** Cr�ation de notre objet qui permet le lien avec la base de donn�e */
	private  SQLiteDatabase dataBase = null;

	public AdapterAgent(Context context) {
		dataBase = ConnectionDB.getInstance(context);
	}
	
	/**
	 * M�thode qui permet d'ins�rer dans la base de donn�es un Agent pass� en param�tres 
	 * @param context
	 * @param agent
	 */
	public long insert(Agent agent){
		
		// On met dans cette Collection les informations � ins�rer
		ContentValues valeurs = new ContentValues();
		// On ajoute l'id
		valeurs.put(Constantes.ID_AGENT, agent.getIdagent());
		// ...
		valeurs.put(Constantes.AGENT_NOM, agent.getNomagent());
		valeurs.put(Constantes.AGENT_PRENOM, agent.getPrenomagent());
		valeurs.put(Constantes.AGENT_TELEPHONE, agent.getTelephone());
		valeurs.put(Constantes.AGENT_TELEPHONEPORTABLE, agent.getTelephoneportable());
		valeurs.put(Constantes.AGENT_INITIALESPARTENAIRE, agent.getInitialespartenaire());
		valeurs.put(Constantes.AGENT_DATEENTREE, agent.getDateentree().getTimeInMillis());
		valeurs.put(Constantes.AGENT_DATESORTIE, agent.getDatesortie().getTimeInMillis());
		valeurs.put(Constantes.AGENT_ACTIF, agent.getActif());
		valeurs.put(Constantes.FK_IDUSER, agent.getIdUtilisateur());
		valeurs.put(Constantes.FK_IDPARTENAIRE, agent.getInitialespartenaire());
		// On appel la m�thode d'insetion sur l'objet maBaseSQL

		return dataBase.insert(Constantes.TABLE_AGENT, null, valeurs);
	}
	/**
	 * Fonction qui permet, � partir d'un id d'utilisateur de trouver l'agent associ�.
	 * 
	 * @param idUtilisateur Id de l'utilisateur dont on veut conna�tre l'agent associ�.
	 * @return Id de l'agent associ� � l'utilisateur ou null si celui ci n'�xiste pas
	 */
	public String getId(String idUtilisateur){
		// Tableau qui contient toutes les colonnes � r�cup�rer
		String[] colonnes = {Constantes.ID_AGENT};
		String[] paramSelcetion = {idUtilisateur};
		Cursor cursor = dataBase.query(Constantes.TABLE_AGENT, colonnes, Constantes.FK_IDUSER + "= ?", paramSelcetion, null, null, null, null);

		String idAgent = null;

		if(cursor.moveToFirst())
		{
			idAgent = (cursor.getString(cursor.getColumnIndex(Constantes.ID_AGENT)));
		}
		return idAgent;
	}
}
