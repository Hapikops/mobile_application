package bcp.android.db.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.Client;

/**
 * <p>
 * Adapteur de la classe Client pour travailler avec la base de donn�es
 * Sqlite en local sur le t�l�phone
 * </p>
 * 
 * @author Burl�on Junior
 *
 */
public class AdapterClient {
	
	/** Pour les log */
	final static String TAG = "BIOCOLD_AdapterClient";
	
	/** Cr�ation de notre objet qui permet le lien avec la base de donn�e */
	private  SQLiteDatabase dataBase = null;

	public AdapterClient(Context context) {
		dataBase = ConnectionDB.getInstance(context);
	}
	
	/**
	 * M�thode qui permet d'ins�rer dans la base de donn�es un client pass� en param�tres 
	 * @param context
	 * @param client
	 */
	public long insert(Client client){
		
		// On met dans cette Collection les informations � ins�rer
		ContentValues valeurs = new ContentValues();
		
		valeurs.put(Constantes.CLIENT_NOM_COMMERCE, client.getNomCommerce());
		valeurs.put(Constantes.CLIENT_TYPE_COMMERCE, client.getTypeCommerce());
		valeurs.put(Constantes.CLIENT_CIVILITE, client.getCivilite());
		valeurs.put(Constantes.CLIENT_NOM, client.getNom());
		valeurs.put(Constantes.CLIENT_PRENOM, client.getPrenom());
		valeurs.put(Constantes.CLIENT_ADRESSE, client.getAdresseCommerce());
		valeurs.put(Constantes.CLIENT_VILLE, client.getVilleCommerce());
		valeurs.put(Constantes.CLIENT_CODEPOSTAL, client.getCodePostalCommerce());
		valeurs.put(Constantes.CLIENT_TELEPHONE, client.getTelephone());
		if(client.isNewClient())
			valeurs.put(Constantes.CLIENT_NEW_FLAG, "1");
		
		// On appel la m�thode d'insetion sur l'objet dataBase
		return dataBase.insert(Constantes.TABLE_CLIENT, null, valeurs);
	}
	
	/**
	 * Permet de connaitre le nombre de nouveau client qui existe en db local
	 * Un nouveau client est un client qui a �t� cr�� gr�ce � l'appli mobile et qui n'a pas encore �t� synchro en db externe.
	 * @return Retourne un entier qui inidique le nombre de nouveau client.
	 */
	public int countNewClient()
	{		
		int nbRecord = 0;
		
		try{
			// Tableau qui contient toutes les colonnes � r�cup�rer 
			String[] colonnes = {Constantes.ID_CLIENT};
			String[] paramSelcetion = {"1"};
			Cursor cursor = dataBase.query(Constantes.TABLE_CLIENT, colonnes, Constantes.CLIENT_NEW_FLAG + " = ?", paramSelcetion, null, null, null, null);
			
			nbRecord = cursor.getCount();
		}
		catch(Exception e){
			//Log.e(TAG, e.toString());
		}
		
		return nbRecord;
	}
	
	/**
	 * Permet de r�cup�rer tout les nouveaux clients en base de donn�e.
	 * Un nouveau client est un client qui a �t� cr�� gr�ce � l'appli mobile et qui n'a pas encore �t� synchro en db externe.
	 * @return Retourne la liste des nouveaux clients.
	 */
	public List<Client> listNewClient()
	{
		Log.i(TAG, "R�cup�ration de la liste des nouveaux clients");
		
		List<Client> listNewClient = null;
		Client client = null;
		
		try{
			// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
			String[] colonnes = {Constantes.ID_CLIENT,
								Constantes.CLIENT_ADRESSE, 
								Constantes.CLIENT_CIVILITE, 
								Constantes.CLIENT_CODEPOSTAL, 
								Constantes.CLIENT_NOM, 
								Constantes.CLIENT_NOM_COMMERCE, 
								Constantes.CLIENT_PRENOM, 
								Constantes.AGENT_TELEPHONE, 
								Constantes.CLIENT_TYPE_COMMERCE, 
								Constantes.CLIENT_VILLE,
								Constantes.FK_IDPARTENAIRE};
			String[] paramSelcetion = {"1"};
			Cursor cursor = dataBase.query(Constantes.TABLE_CLIENT, colonnes, Constantes.CLIENT_NEW_FLAG + " = ?", paramSelcetion, null, null, null, null);
			
			if(cursor.getCount() != 0)
			{
				listNewClient = new ArrayList<Client>();
			}
			while(cursor.moveToNext())
			{
				client = new Client();
				
				//Log.i(TAG, "D�but de mise dans l'objet");
				client.setAdresseCommerce(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_ADRESSE)));
				client.setCivilite(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_CIVILITE)));
				client.setCodePostalCommerce(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_CODEPOSTAL)));
				client.setId(cursor.getString(cursor.getColumnIndex(Constantes.ID_CLIENT)));
				client.setIdPartenaire(cursor.getString(cursor.getColumnIndex(Constantes.FK_IDPARTENAIRE)));
				client.setNom(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_NOM)));
				client.setNomCommerce(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_NOM_COMMERCE)));
				client.setPrenom(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_PRENOM)));
				client.setTelephone(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_TELEPHONE)));
				client.setTypeCommerce(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_TYPE_COMMERCE)));
				client.setVilleCommerce(cursor.getString(cursor.getColumnIndex(Constantes.CLIENT_VILLE)));
				//Log.i(TAG, "Fin de mise dans l'objet");
				
				listNewClient.add(client);
			}
		}
		catch(Exception e){
			Log.e(TAG, e.toString());
		}
		
		return listNewClient;
	}
}
