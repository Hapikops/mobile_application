package bcp.android.db.adapters;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.Utilisateur;

/**
 * </p>
 * Adapteur de la classe Agent pour travailler avec la base de donn�es
 * Sqlite en local sur le t�l�phone
 * <p>
 * @author Matthias Paulmier
 *
 */
public class AdapterUtilisateur {

	/** Cr�ation de notre objet qui permet le lien avec la base de donn�e */
	private  SQLiteDatabase dataBase = null;

	public AdapterUtilisateur(Context context) {
		dataBase = ConnectionDB.getInstance(context);
	}

	/**
	 * M�thode qui permet d'ins�rer dans la base de donn�es un utilisateur pass� en param�tres 
	 * @param context
	 * @param user
	 */
	@SuppressLint("DefaultLocale")
	public long insert(Utilisateur user){
		// On met dans cette Collection les informations � ins�rer
		ContentValues valeurs = new ContentValues();
		// On ajoute l'id
		valeurs.put(Constantes.ID_UTILISATEUR, user.getId());
		// Le login
		valeurs.put(Constantes.UTILISATEUR_LOGIN, user.getLogin().toLowerCase());
		// ...
		valeurs.put(Constantes.UTILISATEUR_PASSWORD, user.getPassword());
		valeurs.put(Constantes.UTILISATEUR_NIVEAUACCES, user.getNiveauAcces());
		valeurs.put(Constantes.FK_IDPARTENAIRE, user.getIdPartenaire());
		// On appel la m�thode d'insetion sur l'objet maBaseSQL

		return dataBase.insert(Constantes.TABLE_UTILISATEUR, null, valeurs);
	}

	/**
	 * Fonction qui permet de r�cup�rer un utilisateur en fonction de son login
	 * 
	 * @param login Login de l'utilisateur � r�cup�rer
	 * @return un Utilisateur ou null si il n'existe pas
	 */
	public Utilisateur find(String login){
		// Tableau qui contient toutes les colonnes � r�cup�rer
		String[] colonnes = {Constantes.ID_UTILISATEUR, Constantes.UTILISATEUR_LOGIN, Constantes.UTILISATEUR_PASSWORD, Constantes.UTILISATEUR_NIVEAUACCES, Constantes.FK_IDPARTENAIRE};
		String[] paramSelcetion = {login};
		Cursor cursor = dataBase.query(Constantes.TABLE_UTILISATEUR, colonnes, Constantes.UTILISATEUR_LOGIN + "= ?", paramSelcetion, null, null, null, null);

		Utilisateur user = null;

		if(cursor.moveToFirst())
		{			
			user = new Utilisateur();
			user.setId(cursor.getString(cursor.getColumnIndex(Constantes.ID_UTILISATEUR)));
			user.setIdPartenaire(cursor.getString(cursor.getColumnIndex(Constantes.FK_IDPARTENAIRE)));
			user.setLogin(cursor.getString(cursor.getColumnIndex(Constantes.UTILISATEUR_LOGIN)));
			user.setNiveauAcces(cursor.getString(cursor.getColumnIndex(Constantes.UTILISATEUR_NIVEAUACCES)));
			user.setPassword(cursor.getString(cursor.getColumnIndex(Constantes.UTILISATEUR_PASSWORD)));
		}		
		return user;
	}

	/**
	 * M�thodes qui permet de mettre � jour un utilisteur pr�sent dans la base de donn�es 
	 * 
	 * @param user
	 */
	public void update(Utilisateur user){
		String[] whereArgs = {user.getId()};
		
		ContentValues values = new ContentValues();

		// On ajoute l'id
		values.put(Constantes.ID_UTILISATEUR, user.getId());
		// Le login
		values.put(Constantes.UTILISATEUR_LOGIN, user.getLogin());
		// ...
		values.put(Constantes.UTILISATEUR_PASSWORD, user.getPassword());
		values.put(Constantes.UTILISATEUR_NIVEAUACCES, user.getNiveauAcces());
		values.put(Constantes.FK_IDPARTENAIRE, user.getIdPartenaire());
		// On appel la m�thode d'insetion sur l'objet maBaseSQL

		dataBase.update(Constantes.TABLE_UTILISATEUR, values, Constantes.ID_UTILISATEUR + "= ?", whereArgs);
	}
}
