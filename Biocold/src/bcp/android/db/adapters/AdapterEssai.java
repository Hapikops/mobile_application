package bcp.android.db.adapters;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.Essai;

/**
 *Cette Adapteur de la classe pour travailler avec la base de donn�e
 *Sqlite en local sur le t�l�phone
 *
 *@autor : frederic radigoy
 *
 */ 
public class AdapterEssai {

	/** Pour les log */
	final static String TAG = "BIOCOLD_AdapterEssai";
	
	/**Creation de l'Objet permet la lien avec la base de donn�es **/
	private SQLiteDatabase database = null;

	public AdapterEssai(Context context){
		database = ConnectionDB.getInstance(context);

	}

	/**
	 *Cette m�thode permet d'inserer dans la base de donn�es local un essai
	 *@param essai Essai � ins�rer
	 *
	 */
	public long insert(Essai essai){
		// creation du collections d'un information � inserer en db
		ContentValues lesValeurs = new ContentValues();

		lesValeurs.put(Constantes.ID_ESSAI, essai.getId());
		lesValeurs.put(Constantes.ESSAI_ID_PARTENAIRE, essai.getIdEssaiPartenaire());
		lesValeurs.put(Constantes.ESSAI_OBSERVATION, essai.getObservation());
		lesValeurs.put(Constantes.ESSAI_NB_CASSETTE, essai.getNbCassette());
		lesValeurs.put(Constantes.ESSAI_DATE_DEBUT, essai.getDateDebut().getTimeInMillis());
		lesValeurs.put(Constantes.FK_IDCLIENT, essai.getIdClient());
		lesValeurs.put(Constantes.FK_IDPARTENAIRE, essai.getIdPartenaire());
		if(essai.isNewEssai())
			lesValeurs.put(Constantes.ESSAI_NEW_FLAG, "1");
		
		// appel � la function d'insertion Sqlite
		return database.insert(Constantes.TABLE_ESSAI, null, lesValeurs);

	}

	/**
	 *Cette m�thode permet d'inserer dans la base de donn�es local une liste d'essai
	 *@param Liste d'essai 
	 *
	 */
	public void insert(List<Essai> listEssais){
		try{
			for(Essai essai : listEssais)
			{
				// creation du collections d'un information � inserer en db
				ContentValues lesValeurs = new ContentValues();

				lesValeurs.put(Constantes.ID_ESSAI, essai.getId());
				lesValeurs.put(Constantes.ESSAI_ID_PARTENAIRE, essai.getIdEssaiPartenaire());
				lesValeurs.put(Constantes.ESSAI_OBSERVATION, essai.getObservation());
				lesValeurs.put(Constantes.ESSAI_NB_CASSETTE, essai.getNbCassette());
				lesValeurs.put(Constantes.ESSAI_DATE_DEBUT, essai.getDateDebut().getTimeInMillis());
				lesValeurs.put(Constantes.FK_IDCLIENT, essai.getIdClient());
				lesValeurs.put(Constantes.FK_IDPARTENAIRE, essai.getIdPartenaire());

				// appel � la function d'insertion Sqlite
				database.insert(Constantes.TABLE_ESSAI, null, lesValeurs);

			} 
		}
		catch(Exception e)
		{
			Log.e("Biocold", e.toString());
		}
	}

	/**
	 * Method permet de recherche un ESSAI � partir d'un idEssaiPartenaire
	 * 
	 * @param idEssaiPartenaire de l'Essai a retrouver 
	 * @return Null ou un Essai
	 * 
	 */ 
	public Essai find(String idEssaiPartenaire){
		String[] colonnes = {Constantes.ID_ESSAI, Constantes.ESSAI_ID_PARTENAIRE, Constantes.FK_IDPARTENAIRE };
		String[] paramSelect = {idEssaiPartenaire};
		/** Interrogez la table donn�e, retour d'un curseur sur l'ensemble des r�sultats.**/
		Cursor curseur = database.query(Constantes.TABLE_ESSAI, colonnes, Constantes.ESSAI_ID_PARTENAIRE + "= ?", paramSelect, null, null, null);

		Essai essai = null;

		if(curseur.moveToFirst()){
			essai = new Essai();
			essai.setId(curseur.getString(curseur.getColumnIndex(Constantes.ID_ESSAI)));
			essai.setIdEssaiPartenaire(curseur.getString(curseur.getColumnIndex(Constantes.ESSAI_ID_PARTENAIRE)));
			essai.setIdPartenaire(curseur.getString(curseur.getColumnIndex(Constantes.FK_IDPARTENAIRE)));
		} 
		return essai;	 
	}

	/** cette methode  permet de supprimer de tous les essais  de la base de donn�es **/ 
	public void deleteAll(){
		database.delete(Constantes.TABLE_ESSAI, null, null);
	}
	
	/**
	 * Permet de connaitre le nombre de nouveau essai qui existe en db local
	 * Un nouvel essai est un essai qui a �t� cr�� gr�ce � l'appli mobile et qui n'a pas encore �t� synchro en db externe.
	 * @return Retourne un entier qui inidique le nombre de nouveaux essais.
	 */
	public int countNewEssai()
	{		
		int nbRecord = 0;
		
		try{
			// Tableau qui contient toutes les colonnes � r�cup�rer
			String[] colonnes = {Constantes.ID_ESSAI};
			String[] paramSelcetion = {"1"};
			Cursor cursor = database.query(Constantes.TABLE_ESSAI, colonnes, Constantes.ESSAI_NEW_FLAG + " = ?", paramSelcetion, null, null, null, null);
			
			nbRecord = cursor.getCount();
		}
		catch(Exception e){
			//Log.e(TAG, e.toString());
		}
		
		return nbRecord;
	}
	
	/**
	 * Permet de r�cup�rer tout les nouveaux essais en base de donn�e.
	 * Un nouvel essai est un essai qui a �t� cr�� gr�ce � l'appli mobile et qui n'a pas encore �t� synchro en db externe.
	 * @return Retourne la liste des nouveaux essais.
	 */
	public List<Essai> listNewEssai()
	{
		Log.i(TAG, "R�cup�ration de la liste des nouveaux essais");
		
		List<Essai> listNewEssai = null;
		Essai essai = null;
		
		try{
			// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
			String[] colonnes = {Constantes.ID_ESSAI,
								Constantes.ESSAI_DATE_DEBUT, 
								Constantes.ESSAI_ID_PARTENAIRE, 
								Constantes.ESSAI_NB_CASSETTE, 
								Constantes.ESSAI_OBSERVATION};
			String[] paramSelcetion = {"1"};
			Cursor cursor = database.query(Constantes.TABLE_ESSAI, colonnes, Constantes.ESSAI_NEW_FLAG + " = ?", paramSelcetion, null, null, null, null);
			
			if(cursor.getCount() != 0)
			{
				listNewEssai = new ArrayList<Essai>();
			}
			while(cursor.moveToNext())
			{
				essai = new Essai();
				GregorianCalendar gregorianCalendar = new GregorianCalendar();
				
				//Log.i(TAG, "D�but de mise dans l'objet");
				gregorianCalendar.setTimeInMillis(Long.parseLong(cursor.getString(cursor.getColumnIndex(Constantes.ESSAI_DATE_DEBUT))));
				essai.setDateDebut(gregorianCalendar);
				essai.setIdPartenaire(cursor.getString(cursor.getColumnIndex(Constantes.ESSAI_ID_PARTENAIRE)));
				essai.setNbCassette(cursor.getString(cursor.getColumnIndex(Constantes.ESSAI_NB_CASSETTE)));
				essai.setId(cursor.getString(cursor.getColumnIndex(Constantes.ID_ESSAI)));
				essai.setObservation(cursor.getString(cursor.getColumnIndex(Constantes.ESSAI_OBSERVATION)));
				
				//Log.i(TAG, "Fin de mise dans l'objet");
				
				listNewEssai.add(essai);
			}
		}
		catch(Exception e){
			Log.e(TAG, e.toString());
		}
		
		return listNewEssai;
	}

}
