package bcp.android.db.adapters;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import bcp.android.db.ConnectionDB;
import bcp.android.db.Constantes;
import bcp.android.metier.ScanK7;
import bcp.android.metier.enums.EtatK7;

/**
 * </p>
 * Adapteur de la classe Agent pour travailler avec la base de donn�es
 * Sqlite en local sur le t�l�phone
 * <p>
 * @author Matthias Paulmier
 *
 */
public class AdapterScanK7 {
	/** Pour les log */
	final static String TAG = "BIOCOLD_AdaptaterScanK7";

	/** Cr�ation de notre objet qui permet le lien avec la base de donn�e */
	private  SQLiteDatabase dataBase = null;

	public AdapterScanK7(Context context) {
		dataBase = ConnectionDB.getInstance(context);
	}

	/**
	 * M�thode qui permet d'ins�rer dans la base de donn�es un scan pass� en param�tres 
	 * @param conext
	 * @param scan
	 */
	public void insert(List<ScanK7> list, String idPassage){
		Log.i(TAG, "Enregistrement d'une liste de scan");
		try{
			for(ScanK7 scan: list){
				ContentValues valeurs = new ContentValues();
				valeurs.put(Constantes.ID_SCAN_K7, scan.getIdscank7());
				valeurs.put(Constantes.SCAN_K7_CODE, scan.getCodebarre());
				valeurs.put(Constantes.SCAN_K7_LONGITUDE, scan.getLongitude());
				valeurs.put(Constantes.SCAN_K7_LATITUDE, scan.getLatitude());
				valeurs.put(Constantes.SCAN_K7_DATE_SAISIE, scan.getDatesaisie().getTimeInMillis());
				valeurs.put(Constantes.SCAN_K7_ETATK7, scan.getEtatk7().getCode());
				valeurs.put(Constantes.SCAN_K7_ETAT_DATE, scan.getEtatdate().getTimeInMillis());
				valeurs.put(Constantes.FK_IDUSER, scan.getIdUser());
				valeurs.put(Constantes.FK_IDPASSAGEAGENT, idPassage);

				dataBase.insert(Constantes.TABLE_SCAN_K7, null, valeurs);
			}
		}catch(Exception e){
			Log.e(TAG, e.toString());
		}
	}

	/**
	 * M�thode de selection qui renvoie tous les Scan �ffectu�s et inscrits en base de donn�es
	 * @param idPassageAgent ID du passage auquel est li� les scans
	 * @return Retourne la liste des scans
	 */
	public List<ScanK7> list(int idPassageAgent)
	{
		Log.i(TAG, "R�cup�ration de la liste de scan");

		List<ScanK7> listSkanK7 = null;
		ScanK7 scanK7 = null;

		try{
			// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
			String[] colonnes = {Constantes.SCAN_K7_CODE, Constantes.SCAN_K7_DATE_SAISIE, Constantes.SCAN_K7_ETAT_DATE,Constantes.SCAN_K7_ETATK7, Constantes.FK_IDPASSAGEAGENT, Constantes.ID_SCAN_K7, Constantes.FK_IDUSER, Constantes.SCAN_K7_LATITUDE, Constantes.SCAN_K7_LONGITUDE};
			String[] paramSelcetion = {Integer.toString(idPassageAgent)};
			Cursor cursor = dataBase.query(Constantes.TABLE_SCAN_K7, colonnes, Constantes.FK_IDPASSAGEAGENT + "= ?", paramSelcetion, null, null, null, null);

			//Log.i(TAG, "Il y a de la lecture");
			listSkanK7 = new ArrayList<ScanK7>();
			while(cursor.moveToNext())
			{
				scanK7 = new ScanK7();
				//Log.i(TAG, "D�but de mise dans l'objet");
				scanK7.setCodebarre(cursor.getString(cursor.getColumnIndex(Constantes.SCAN_K7_CODE)));
				GregorianCalendar gregorianCalendar = new GregorianCalendar();
				gregorianCalendar.setTimeInMillis(Long.parseLong(cursor.getString(cursor.getColumnIndex(Constantes.SCAN_K7_DATE_SAISIE))));
				scanK7.setDatesaisie(gregorianCalendar);
				gregorianCalendar.setTimeInMillis(Long.parseLong(cursor.getString(cursor.getColumnIndex(Constantes.SCAN_K7_ETAT_DATE))));
				scanK7.setEtatdate(gregorianCalendar);
				int codeEtat = cursor.getInt(cursor.getColumnIndex(Constantes.SCAN_K7_ETATK7));
				if(codeEtat == EtatK7.PROPRE.getCode())
					scanK7.setEtatk7(EtatK7.PROPRE);
				else if(codeEtat == EtatK7.SALE.getCode())
					scanK7.setEtatk7(EtatK7.SALE);
				scanK7.setIdPassageAgent(cursor.getString(cursor.getColumnIndex(Constantes.FK_IDPASSAGEAGENT)));
				scanK7.setIdscank7(cursor.getString(cursor.getColumnIndex(Constantes.ID_SCAN_K7)));
				scanK7.setIdUser(cursor.getString(cursor.getColumnIndex(Constantes.FK_IDUSER)));
				scanK7.setLatitude(cursor.getString(cursor.getColumnIndex(Constantes.SCAN_K7_LATITUDE)));
				scanK7.setLongitude(cursor.getString(cursor.getColumnIndex(Constantes.SCAN_K7_LONGITUDE)));
				//Log.i(TAG, "Fin de mise dans l'objet");

				listSkanK7.add(scanK7);
			}
		}
		catch(Exception e){
			Log.e(TAG, e.toString());
		}

		return listSkanK7;
	}

	/** Permet de supprimer  tout les scan li� � un passage **/
	public void delete(int idPassage)
	{
		// Tableau qui contient toutes les colonnes � r�cup�rer (ici nous n'avons que la colonne num�ro de contrat)
		String[] paramSelcetion = {String.valueOf(idPassage)};

		try
		{
			dataBase.delete(Constantes.TABLE_SCAN_K7, Constantes.FK_IDPASSAGEAGENT+ "= ?", paramSelcetion);
			Log.i(TAG, "Suppression d'un scan");
		}
		catch(Exception e)
		{
			Log.e(TAG, e.toString());
		}
	}

}
