package bcp.android.db.adapters;


import java.util.List;

import bcp.android.db.ConnectionDB;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import bcp.android.metier.TypeCommerce;




public class AdapterTypeCommerce {

	
	/** Nom de la table */
	public static final String TABLE_TYPE_COMMERCE = "type_commerce";
	/** identifiant unique de la classe  */
	public static final String ID_TYPECOMMERCE = "_id_typecommerce";
	public static final String LIBELLE_TYPECOMMERCE = "value";
	
	
	
	
	/** Creation de l'objet qui fait le lien avec la base de données */
	private  SQLiteDatabase dataBase = null;
	
	
	public AdapterTypeCommerce(Context context) {
		dataBase = ConnectionDB.getInstance(context);
	}
	/* Méthode qui permet d'insérer dans la base de données une liste de type de passage passé en paramètres 
	 * @param listTypePassage Liste des types de passage
	 */
	
	public void insert(List<TypeCommerce> listTypeCommerce)
	{
		
			for(TypeCommerce typeCommerce : listTypeCommerce)
			{
				// On met dans cette Collection les informations à insérer dans la base de données 
				ContentValues valeurs = new ContentValues();
				
				valeurs.put(ID_TYPECOMMERCE, typeCommerce.getId());
				valeurs.put(LIBELLE_TYPECOMMERCE, typeCommerce.getLibelle());
				
				// On appele la méthode d'insetion sur l'objet maBaseSQL
				dataBase.insert(TABLE_TYPE_COMMERCE, null, valeurs);
				
			}
		
	}
	
	

}
