package bcp.android.graphic.adapters;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import bcp.android.biocold.ListLastPassageActivity;
import bcp.android.metier.Passage;
import bpc.android.biocold.R;

public class ListLastPassageAdapter extends BaseExpandableListAdapter {

	//Les passages
	private List<List<Passage>> childItems;
	private LayoutInflater inflater;
	private Context context;

	//Date
	private List<GregorianCalendar> parentItems;

	public ListLastPassageAdapter(List<GregorianCalendar> groupes, List<List<Passage>> childItems) {
		this.parentItems = groupes;
		this.childItems = childItems;
	}

	public void setInflater(LayoutInflater inflater, Context context) {
		this.inflater = inflater;
		this.context = context;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		TextView textView = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child_view_list_last_passage, null);
		}

		LinearLayout linearLayout = (LinearLayout)convertView.findViewById(R.id.list_lastPassage_ligne);
		textView = (TextView) convertView.findViewById(R.id.list_lastPassage_heure);
		textView.setTextColor(Color.WHITE);
		String heure = childItems.get(groupPosition).get(childPosition).getDatePassage().get(Calendar.HOUR_OF_DAY) + "h" + (childItems.get(groupPosition).get(childPosition).getDatePassage().get(Calendar.MINUTE));
		textView.setText(heure);
		textView = (TextView) convertView.findViewById(R.id.list_lastPassage_idEssaiContrat);
		textView.setTextColor(Color.WHITE);
		textView.setText(childItems.get(groupPosition).get(childPosition).getIdEssaiPartenaire());
		linearLayout.setBackgroundColor(context.getResources().getColor(R.color.orangeBicold));
		if(childItems.get(groupPosition).get(childPosition).getIdContrat() != null)
		{
			textView.setText(childItems.get(groupPosition).get(childPosition).getIdContrat());
			linearLayout.setBackgroundColor(context.getResources().getColor(R.color.blueBiocold));
		}
		textView = (TextView) convertView.findViewById(R.id.list_lastPassage_idPassage);
		textView.setTextColor(Color.WHITE);
		textView.setText(childItems.get(groupPosition).get(childPosition).getId());


		/* Permet d'afficher une boite d'information sur le passage 
		 * Dans un premier temps se n'est qu'un Toast mais si d'autre information doivent pouvoir
		 * �tre montr�e alors il vaudrait mieux passer par une Dialog */
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				String title = context.getResources().getString(R.string.list_last_passage_head_idPassage) + " : " + childItems.get(groupPosition).get(childPosition).getId();				
				String message = context.getResources().getString(R.string.list_last_passage_info_type)+" : "+childItems.get(groupPosition).get(childPosition).getTypepassage().getLibelle(context);
				new AlertDialog.Builder(context).setTitle(title).setMessage(message).setNeutralButton(context.getResources().getString(R.string.default_ok), null).show();
			}
		});

		return convertView;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.parent_view, null);
		}

		String dateStr = parentItems.get(groupPosition).get(Calendar.DAY_OF_MONTH) + "/" + (parentItems.get(groupPosition).get(Calendar.MONTH) + 1 ) + "/" + parentItems.get(groupPosition).get(Calendar.YEAR);

		((CheckedTextView) convertView).setText(dateStr);
		((CheckedTextView) convertView).setChecked(true);

		return convertView;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return ((ArrayList<Passage>) childItems.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public int getGroupCount() {
		return parentItems.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
		for(GregorianCalendar date: parentItems){
			int position;
			if((position = parentItems.indexOf(date)) != groupPosition){
				ListLastPassageActivity.collapse(position);
			}
		}
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

}