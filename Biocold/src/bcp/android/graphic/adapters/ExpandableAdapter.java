package bcp.android.graphic.adapters;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import bcp.android.biocold.MainActivity;
import bcp.android.biocold.RecapScanActivity;
import bcp.android.metier.ScanK7;
import bcp.android.metier.enums.EtatK7;
import bpc.android.biocold.R;

public class ExpandableAdapter extends BaseExpandableListAdapter {

	private Activity activity;
	private List<List<ScanK7>> childItems;
	private LayoutInflater inflater;
	private List<EtatK7> parentItems;

	public ExpandableAdapter(List<EtatK7> groupes, List<List<ScanK7>> childItems) {
		this.parentItems = groupes;
		this.childItems = childItems;
	}

	public void setInflater(LayoutInflater inflater, Activity activity) {
		this.inflater = inflater;
		this.activity = activity;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		TextView textView = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child_view, null);
		}

		textView = (TextView) convertView.findViewById(R.id.child_text_view);
		textView.setText(childItems.get(groupPosition).get(childPosition).getCodebarre());

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// On cr�� un builer qui permet de cr�er la bo�te de dialogue
				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				// On lui ajoute un titre
				builder.setTitle(activity.getResources().getString(R.string.title_delete));
				builder.setMessage(activity.getResources().getString(R.string.message_delete));
				//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
				builder.setPositiveButton(activity.getResources().getString(R.string.default_delete), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						RecapScanActivity.deleteScan(groupPosition, childItems.get(groupPosition).get(childPosition));
						RecapScanActivity.collapse(groupPosition);
						
						boolean encoreDesScan = false;
						for(int i = 0; i < getGroupCount(); i++)
							if(childItems.get(i).size() <= 0);
							else
								encoreDesScan = true;
						if(!encoreDesScan)
						{
							activity.startActivity(new Intent(activity, MainActivity.class));
							activity.finish();
						}
					}
				}); 
				builder.setNegativeButton(activity.getResources().getString(R.string.default_cancel), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// On ferme la bo�te de dialogue
						dialog.dismiss();
					}
				});
				// On cr�� l'AlertDialog (Bo�te de dialogue) � partir du builder cr�� au dessus
				AlertDialog alertDialog = builder.create();

				// On l'affiche
				alertDialog.show();
			}
		});

		return convertView;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.parent_view, null);
		}
		
		((CheckedTextView) convertView).setText(parentItems.get(groupPosition).getLibelle(activity) + " (" + childItems.get(groupPosition).size() + ")");
		((CheckedTextView) convertView).setChecked(true);
					
		return convertView;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return ((ArrayList<ScanK7>) childItems.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public int getGroupCount() {
		return parentItems.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
		for(EtatK7 etat: parentItems){
			int position;
			if((position = parentItems.indexOf(etat)) != groupPosition){
				RecapScanActivity.collapse(position);
			}
		}
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

}