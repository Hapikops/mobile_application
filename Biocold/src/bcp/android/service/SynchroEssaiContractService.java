package bcp.android.service;

import java.util.List;

import tools.Utility;
import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import bcp.android.db.adapters.AdapterContrat;
import bcp.android.db.adapters.AdapterEssai;
import bcp.android.metier.Constantes;
import bcp.android.metier.Contrat;
import bcp.android.metier.Essai;
import bcp.android.parsers.ParserXMLContrat;
import bcp.android.parsers.ParserXMLEssai;
import bpc.android.biocold.R;

public class SynchroEssaiContractService extends IntentService{

	/** Pour les log */
	private final static String TAG = "SynchroEssaiContractService";
	/** Adapter permettant d'acc�der � la table des contrats */
	private AdapterContrat adapterContrat;
	private List<Contrat> listContrat;
	private AdapterEssai adapterEssai;
	private List<Essai> listEssai;

	public SynchroEssaiContractService() {
		super("SynchroEssaiContractService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i(TAG, "Service permetant la synchronisation d'essais/contrats de la base de donn�es interne avec la base de donn�es externe");
		if(Utility.isNetworkAvailable(getBaseContext())){
			adapterContrat = new AdapterContrat(getBaseContext());
			adapterEssai = new AdapterEssai(getBaseContext());
			// Liste des contrats
			try {
				/* R�cup�ration de la liste des contrats li� au partenaire qui est lui m�me li� � l'agent qui vient de se connecter */
				SharedPreferences settings = getSharedPreferences(Constantes.PREFS_NAME, 0);
				String idPartenaire = settings.getString(Constantes.ID_PARTENAIRE, "");
				listContrat = new ParserXMLContrat().getListContrat(idPartenaire);
				listEssai = new ParserXMLEssai().getListEssai(idPartenaire);
				if (listContrat != null && listEssai != null){
					// On supprime les contrats apr�s la r�cup�ration de la liste de contrats
					adapterContrat.deleteAll();
					adapterContrat.insert(listContrat);
					
					adapterEssai.deleteAll();
					adapterEssai.insert(listEssai);
					
					Utility.showNotification(this, getApplicationContext().getResources().getString(R.string.notification_ticker_SynchroEssaiContract_success), getApplicationContext().getResources().getString(R.string.notification_title_SynchroEssaiContract_success), getApplicationContext().getResources().getString(R.string.notification_message_SynchroEssaiContract_success), 501);
				}
				else {
					Utility.showNotification(this, getApplicationContext().getResources().getString(R.string.notification_ticker_SynchroEssaiContract_echec), getApplicationContext().getResources().getString(R.string.notification_title_SynchroEssaiContract_echec), getApplicationContext().getResources().getString(R.string.notification_message_SynchroEssaiContract_echec), 501);
				}
			} 
			catch (Exception e) {
				Log.e(TAG, "Erreur de chargement XML des contrats ou essai", e);
				Utility.showNotification(this, "Echec de la synchronisation...", "Essais & Contrats", "La synchronisation des contrats et essais a �chou�e, vos contrats et essais ne sont pas � jour", 501);
			}
		}
		else{
			Log.i(TAG, "La connexion n'est pas disponible");
			//new Handler(Looper.getMainLooper()).post(new Utility.DisplayToast(this, "La connexion n'est pas disponible."));
		}
	}
}
