/**
 * 
 */
package bcp.android.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import tools.HttpMethodePostRunnable;
import tools.Utility;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import bcp.android.db.adapters.AdapterPassage;
import bcp.android.metier.Constantes;
import bcp.android.metier.Passage;
import bcp.android.metier.ScanK7;
import bpc.android.biocold.R;

/**
 * @author Junior
 *
 * Ce service permet de synchroniser la base de donn�e local � la db extrene.
 * C'est � dire envoyer tout les passage et scan qui sont stocker en local vers la db externe.
 * 
 * Ce service  permet �galement d'enregistrer en local ou en externe (selon que la connexion est disponible ou non)
 * les passages et scans.
 * 
 */
public class SynchroPassageService extends IntentService {

	/** Pour les log */
	private final static String TAG = "BIOCOLD_ServiceSynchroPassage";
	/** Adapeter permettant d'acc�der a la table passage */
	private AdapterPassage adapterPassage;
	/** Passage utilis� comme variable de travail */
	private Passage passage;

	public SynchroPassageService() {
		super("SynchroPassageAgentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i(TAG, "Service permetant la synchronisation  et l'enregistrement en local ou externe des passages");

		adapterPassage = new AdapterPassage(getApplicationContext());
		// R�cup�ration du bundle pass� par l'activit� pr�c�dente
		Bundle bundle = intent.getExtras();

		if(adapterPassage.count() != 0 && Utility.isNetworkAvailable(getApplicationContext()))
		{
			Log.i(TAG, "Il faut synchroniser");
			List<Passage> listPassageAgent = adapterPassage.list();
			for (Passage passageAgent : listPassageAgent) {
				this.passage =  passageAgent;
				loadScansPassage(Integer.parseInt(passageAgent.getId()));
				//Log.i(TAG, "Passage synchro vers db externe");
			}

			//Supprimer tout les scans et tout les passages qui ont �t� synchroniser
			//Mais attention cela est fait par le thread qui envoie en db externe
			//Car chaque passage est supprimer un � un en fonction du status HTTP retourner

		}
		else
		{
			Log.i(TAG, "Pas besoin de synchroniser ou R�seau indisponible");
		}

		if(bundle != null)
		{
			Log.i(TAG, "Enregistrement d'un passage");

			// R�cup�ration du passage grace au bundle
			this.passage = (Passage) bundle.getSerializable(Constantes.PASSAGE);
			loadScansPassage(0);
		}
		else
			Log.i(TAG, "Aucun passage � enregistrer");
	}


	/**
	 * M�thode qui permet de g�n�rer le XML qui doit �tre envoy� au webservice
	 * 
	 * @return Retourne du XML
	 */
	private String getXmlPassage() {
		/* Liste des param�tres � passer dans la requ�te http POST pour envoyer le passage au service web */
		List<NameValuePair> listParametersPassage = new ArrayList<NameValuePair>();
		listParametersPassage.add(new BasicNameValuePair("IdUser", passage.getIdUtilisateur()));  
		listParametersPassage.add(new BasicNameValuePair("Date", Long.toString(passage.getDatePassage().getTimeInMillis()))); 
		listParametersPassage.add(new BasicNameValuePair("IdAgent", passage.getIdAgent())); 
		listParametersPassage.add(new BasicNameValuePair("IdEssai", passage.getIdEssai()));
		listParametersPassage.add(new BasicNameValuePair("ObservationPassageInterne", passage.getObservationpassageinterne()));
		listParametersPassage.add(new BasicNameValuePair("TypePassage", passage.getTypepassage().getCode()));
		
		String xml = "<ContractDataPassageAgent xmlns=\"http://extranetbcp.fr/WS/Service/contractDataPassageAgent\">";
		xml += Utility.convertToXML(listParametersPassage);
		xml += getXmlScans();
		xml += "</ContractDataPassageAgent>";
		//Log.i(TAG, xml);
		return xml;
	}

	/**
	 * M�thode qui permet de g�n�rer le XML qui doit �tre envoy� au webservice
	 * 
	 * @return Retourne du XML
	 */
	public String getXmlScans(){
		/* Liste des parametres � passer dans la requ�te http POST pour envoyer les scanners au service web */
		List<List<NameValuePair>> listListParametersScans = new ArrayList<List<NameValuePair>>();
		// Pour chaque scanner on ajoute la liste de param�tres
		for(ScanK7 scan: passage.getListScank7()){
			List<NameValuePair> listParametersScan = new ArrayList<NameValuePair>(8);
			listParametersScan.add(new BasicNameValuePair("a:IdUser", scan.getIdUser()));
			listParametersScan.add(new BasicNameValuePair("a:CodeBarre", scan.getCodebarre()));
			listParametersScan.add(new BasicNameValuePair("a:Date", Long.toString(scan.getDatesaisie().getTimeInMillis())));
			listParametersScan.add(new BasicNameValuePair("a:Longitude", scan.getLongitude()));
			listParametersScan.add(new BasicNameValuePair("a:Latitude", scan.getLatitude()));
			listParametersScan.add(new BasicNameValuePair("a:IdEssai", passage.getIdEssai()));
			listParametersScan.add(new BasicNameValuePair("a:EtatK7", Integer.toString(scan.getEtatk7().getCode())));
			// On ajoute cette liste de param�tres � la liste de liste de param�tres (�a parra�t un peu lourd non ?)
			listListParametersScans.add(listParametersScan);
		}

		String xml = "<ListContractDataScanK7 xmlns:a=\"http://extranetbcp.fr/WS/Service/ContractDataScanK7\">";
		for(List<NameValuePair> listParametersScan: listListParametersScans){
			xml += "<a:ContractDataScanK7>";
			xml += Utility.convertToXML(listParametersScan);
			xml += "</a:ContractDataScanK7>";
		}
		xml += "</ListContractDataScanK7>";
		//Log.i(TAG, xml);
		return xml;
	}

	/**
	 * Cette m�thode permet d'envoyer les scanners dans la base de donn�es
	 * En local si pas le net
	 * En externe si le net
	 * 
	 * Et cette m�thode permet aussi de supprimer en db local les passages une fois que ceux ont bien �t� 
	 * inscrit en db externe.

	 * @param idPassage Id du passage qui est envoy�. Ce param�tre doit valoir z�ro si aucun passage ne 
	 * doit �tre supprimer � la fin de cette m�thode
	 * 
	 */
	private void loadScansPassage(int idPassage)
	{		
		if(Utility.isNetworkAvailable(getApplicationContext())){
			// On envoie directement au webService le passage lorsque le r�seau est disponible
			try{
				Log.i(TAG, "Envoie au webservice d'un passage agent et de ces scans pour insertion en db externe");
				HttpMethodePostRunnable httpMethodePost = new HttpMethodePostRunnable();
				InputStream is = httpMethodePost.executeRequest("http://extranetbcp.fr/WebService/Implementation/PassageAgentWS.svc/", getXmlPassage());
				
				int returnValue = Utility.getReturnValue(is);
				
				//Si le status HTTP est 200 et que la record vient bien de la db local alors on peut supprimer le passage de la DB Local
				if(httpMethodePost.getReturnCode() == 200 && idPassage != 0)
				{
					new AdapterPassage(getApplicationContext()).delete(idPassage);
					Log.i(TAG, "Suppression du passage en DB Local");
				}
				if(httpMethodePost.getReturnCode() != 200 && idPassage == 0)
				{
					Log.i(TAG, "Erreur lors de l'insertion en db externe donc => Insertion en DB Local");
					adapterPassage.insert(passage);
				}
				if(httpMethodePost.getReturnCode() == 200 && idPassage == 0)
				{
					if(passage.getIdContrat() != null)
						Utility.showNotification(this, getApplicationContext().getResources().getString(R.string.notification_ticker_SynchroPassageAgent), String.valueOf(returnValue), getApplicationContext().getResources().getString(R.string.notification_message_contract) + " : "+passage.getIdContrat(), 1);
					else
						Utility.showNotification(this, getApplicationContext().getResources().getString(R.string.notification_ticker_SynchroPassageAgent), String.valueOf(returnValue), getApplicationContext().getResources().getString(R.string.notification_message_essai) + " : "+passage.getIdEssai(), 1);
				}
			}
			catch(Exception e){
				Log.e(TAG, e.toString());
			}

		}
		else{
			Log.i(TAG, "Insertion en DB Local d'un passage agent et de ces scans");
			adapterPassage.insert(passage);
		}
	}
}
