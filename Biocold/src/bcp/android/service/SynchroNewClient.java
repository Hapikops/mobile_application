package bcp.android.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import tools.HttpMethodePostRunnable;
import tools.Utility;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import bcp.android.db.adapters.AdapterClient;
import bcp.android.db.adapters.AdapterPassage;
import bcp.android.metier.Client;

/**
 * @author Junior
 *
 * Ce service permet de synchroniser la base de donn�e local � la db extrene.
 * C'est � dire envoyer tout les nouveaux essais en db externe.
 * 
 */
public class SynchroNewClient extends IntentService {

	/** Pour les log */
	private final static String TAG = "BIOCOLD_ServiceSynchroNewClient";
	/** Adapeter permettant d'acc�der a la table passage agent */
	private AdapterClient adapterClient;
	/** Passage agent utilis� comme variable de travail */
	private Client client;

	public SynchroNewClient() {
		super("SynchroNewClient");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i(TAG, "Service permetant la synchronisation  des new clients");

		adapterClient = new AdapterClient(getApplicationContext());

		if(adapterClient.countNewClient() != 0 && Utility.isNetworkAvailable(getApplicationContext()))
		{
			Log.i(TAG, "Il faut synchroniser");
			List<Client> listClient = adapterClient.listNewClient();
			for (Client client : listClient) {
				this.client =  client;
				loadClient(Integer.parseInt(client.getId()));
			}

			//Supprimer tout les new clients qui ont �t� synchroniser
			//Mais attention cela est fait par le thread qui envoie en db externe
			//Car chaque clients est supprim� un � un en fonction du status HTTP retourner

		}
		else
		{
			Log.i(TAG, "Pas besoin de synchroniser ou R�seau indisponible");
		}
	}
	
	
	/**
	 * M�thode qui permet de g�n�rer le XML qui doit �tre envoy� au webservice
	 * 
	 * @return Retourne du XML
	 */
	private String getXmlClient() {
		/* Liste des param�tres � passer dans la requ�te http POST pour envoyer le client au service web 
		 * Et mise en forme de ceux-ci pour �tre pris par le web service */
		List<NameValuePair> listParametersPassage = new ArrayList<NameValuePair>();
		listParametersPassage.add(new BasicNameValuePair("AdresseCommerce", client.getAdresseCommerce()));
		//La civilit� doit �tre un int et correspondre � la liste suivante : 
        // 1 : M
        // 2 : Mme  
        // 3 : Mlle
		listParametersPassage.add(new BasicNameValuePair("Civilite", client.getCivilite())); 
		listParametersPassage.add(new BasicNameValuePair("CodePostalCommerce", client.getCodePostalCommerce())); 
		listParametersPassage.add(new BasicNameValuePair("Id", client.getId()));
		listParametersPassage.add(new BasicNameValuePair("IdContrat", client.getIdContrat()));
		listParametersPassage.add(new BasicNameValuePair("IdEssai", client.getIdEssai()));
		listParametersPassage.add(new BasicNameValuePair("IdPartenaire", client.getIdPartenaire()));
		listParametersPassage.add(new BasicNameValuePair("Nom", client.getNom()));
		listParametersPassage.add(new BasicNameValuePair("NomCommerce", client.getNomCommerce()));
		listParametersPassage.add(new BasicNameValuePair("Prenom", client.getPrenom()));
		listParametersPassage.add(new BasicNameValuePair("Tel", client.getTelephone()));
		//Cas particulier pour le type de commerce.
		listParametersPassage.add(new BasicNameValuePair("TypeCommerce", client.getTypeCommerce()));
		listParametersPassage.add(new BasicNameValuePair("VilleCommerce", client.getVilleCommerce()));
		
		String xml = "<Client xmlns=\"http://extranetbcp.fr/WS/Domaine/Client\">";
		xml += Utility.convertToXML(listParametersPassage);
		xml += "</Client>";
		//Log.i(TAG, xml);
		return xml;
	}

	
	/**
	 * Cette m�thode permet d'envoyer les scanners dans la base de donn�es
	 * En local si pas le net
	 * En externe si le net
	 * 
	 * Et cette m�thode permet aussi de supprimer en db local les passages une fois que ceux ont bien �t� 
	 * inscrit en db externe.

	 * @param idPassage Id du passage qui est envoy�. Ce param�tre doit valoir z�ro si aucun passage ne 
	 * doit �tre supprimer � la fin de cette m�thode
	 * 
	 */
	private void loadClient(int idClient)
	{		
		if(Utility.isNetworkAvailable(getApplicationContext()))
		{
			// On envoie directement au webService le passage lorsque le r�seau est disponible
			try{
				Log.i(TAG, "Envoie au webservice d'un client pour insertion en db externe");
				HttpMethodePostRunnable httpMethodePost = new HttpMethodePostRunnable();
				InputStream is = httpMethodePost.executeRequest("http://extranetbcp.fr/WebService/Implementation/ClientWS.svc/", getXmlClient());
				
				int returnValue = Utility.getReturnValue(is);
				
				//Si le status HTTP est 200 et que la record vient bien de la db local alors on peut supprimer le passage de la DB Local
				if(httpMethodePost.getReturnCode() == 200 && idClient != 0)
				{
					new AdapterPassage(getApplicationContext()).delete(idClient);
					Log.i(TAG, "Suppression du passage en DB Local");
				}
				if(httpMethodePost.getReturnCode() != 200)
				{
					Log.i(TAG, "Erreur lors de l'insertion en db externe donc => Insertion en DB Local");
				}
			}
			catch(Exception e){
				Log.e(TAG, e.toString());
			}

		}
	}
}
