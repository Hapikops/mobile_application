package bcp.android.biocold;

import java.util.GregorianCalendar;
import java.util.List;

import tools.LocationRunnable;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import bcp.android.metier.Constantes;
import bcp.android.metier.Passage;
import bcp.android.metier.enums.TypePassage;
import bpc.android.biocold.R;

/**
 * 
 * <p>
 * Activit� qui permet � l'utilisateur de choisir le type de passage qu'il s'appr�te � r�aliser
 * parmis une liste r�cup�r�e depuis la base de donn�es
 * </p>
 * <pre>
 * Cette activit� peut �tre lanc� depuis :
 * - MainActivity
 * 
 * Cette activit� m�ne � : 
 * - ChoiceContract
 * - ChoiceEssai
 * - NewEssai
 * 
 * Cette activit� doit permettre de s�lectionner un type de passage et de rediriger vers l'activit� qui convient.
 * 
 * </pre>
 * 
 * @author Junior
 * 
 */
public class ChoiceTypePassageActivity extends Activity {
	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_ChoiceTypePassage";
	/** Liste qui contient les valeurs possibles de types de passage concernant les Essais */
	private List<TypePassage> listTypePassageEssai;
	/** Liste qui contient les valeurs possibles de types de passage concernant les Contrats */
	private List<TypePassage> listTypePassageContrat;
	/** Les deux bouton */
	private Button dialogContrat, dialogEssai;
	/** Permet d'avoir acc�s � l'activit� courante partout dans cette classe **/
	private Activity thisActivity;
	/** Permet de savoir si l'on a clicker pour faire un essai ou un contrat */
	private int choice;

	/** Deux petite contstante utilis�e pour rendre le code plus claire */
	private static int CONTRAT = 1;
	private static int ESSAI = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choice_type_passage_activity);

		thisActivity = this;

		// On r�cup�re toutes les valeurs dans la db local
		listTypePassageEssai = TypePassage.getList(1);
		listTypePassageContrat = TypePassage.getList(2);
		listTypePassageEssai.addAll(TypePassage.getList(3));
		listTypePassageContrat.addAll(TypePassage.getList(3));

		dialogEssai = (Button)findViewById(R.id.button_choice_type_essai);
		dialogEssai.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				choice = ESSAI;
				callAlertDialogList(listTypePassageEssai);
			}
		});

		dialogContrat = (Button)findViewById(R.id.button_choice_type_contrat);
		dialogContrat.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				choice = CONTRAT;
				callAlertDialogList(listTypePassageContrat);
			}
		});
	}

	private void callAlertDialogList(final List<TypePassage> listTypePassage)
	{  
		final Dialog dialog = new Dialog(thisActivity);
		dialog.setContentView(R.layout.list_type_passage_alert_dialog);
		dialog.setTitle(getResources().getString(R.string.title_choice_type_passage));

		ListView listView = (ListView) dialog.findViewById(R.id.list_types_passage);


		/*
        L'adapter qui suis va utiliser la m�thode toString de EnumTypePassage et comme le dit la javadoc
        il faut donc d'abord initialiser le context de cette classe
		 **/
		TypePassage.setContext(thisActivity);
		ArrayAdapter<TypePassage> adapter = new ArrayAdapter<TypePassage>(this, R.layout.child_view_list_type , R.id.child_text_view_type, listTypePassage);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {    
				//Toast.makeText(thisActivity, listTypePassage.get(position).getLibelleType(), Toast.LENGTH_LONG).show(); // affichage de l'objet selectionn�             
				if(choice == CONTRAT)
				{
					//On lance le service de localisation
					new Thread(new LocationRunnable(getApplicationContext())).start();
					// Cr�ation d'un Intent qui permet de passer � l'activity de selection de contrat
					Intent intent = new Intent(thisActivity, ChoiceContractActivity.class);
					Bundle bundle = new Bundle();
					bundle.putSerializable(Constantes.PASSAGE, initialisationPassage(listTypePassage.get(position)));
					intent.putExtras(bundle);
					startActivity(intent);	
					//Pour fermer la boite de dialog sinon erreur
					dialog.dismiss();
					thisActivity.finish();
				}
				else if(choice == ESSAI)
				{
					Intent intent;
					if(listTypePassage.get(position) == TypePassage.START_ESSAI)
					{
						// Cr�ation d'un Intent qui permet de passer � l'activity de cr�ation de client et essai
						intent = new Intent(thisActivity, NewEssaiActivity.class);
					}
					else
					{
						// Cr�ation d'un Intent qui permet de passer � l'activity de selection de contrat
						intent = new Intent(thisActivity, ChoiceEssaiActivity.class);
					}
					//On lance le service de localisation
					new Thread(new LocationRunnable(getApplicationContext())).start();
					Bundle bundle = new Bundle();
					bundle.putSerializable(Constantes.PASSAGE, initialisationPassage(listTypePassage.get(position)));
					intent.putExtras(bundle);
					startActivity(intent);	
					//Pour fermer la boite de dialog sinon erreur
					dialog.dismiss();
					thisActivity.finish();
				}
			}
		});
		dialog.show();
	}

	/**
	 * Cette m�thode permet d'initialiser correctement un passage qui va ensuite �tre transmit � chaque activit� et celle-ci le compl�teront.
	 * @return Retour un PassageAgent correctement initialis�
	 */
	private Passage initialisationPassage(TypePassage typePassage)
	{
		Passage passageAgent = new Passage();
		passageAgent.setIdUtilisateur(getSharedPreferences(Constantes.PREFS_NAME, 0).getString(Constantes.ID_USER, ""));
		passageAgent.setIdAgent(getSharedPreferences(Constantes.PREFS_NAME, 0).getString(Constantes.ID_AGENT, ""));
		passageAgent.setDatepassage(new GregorianCalendar());
		passageAgent.setTypepassage(typePassage);
		return passageAgent;
	}

	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(thisActivity, MainActivity.class);
		startActivity(mIntent);
		thisActivity.finish();
		super.onBackPressed();
	}
}
