package bcp.android.biocold;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import bcp.android.graphic.adapters.ExpandableAdapter;
import bcp.android.metier.Constantes;
import bcp.android.metier.Passage;
import bcp.android.metier.ScanK7;
import bcp.android.metier.enums.EtatK7;
import bpc.android.biocold.R;

/**
 * 
 * 
 * Affiche un r�capitulatif des K7 Scann�es dans l'activit� pr�c�dente. 
 * <p>Le partenaire est invit� � saisir une observation concernant le passage</p>
 * 
 * <pre>
 * Cette activit� peut �tre lanc� depuis :
 * - MenuScanActivity
 * 
 * Cette activit� m�ne � : 
 * - MainActivity
 * - MenuScanActivity (Si l'utilisateur souhaite scanner � nouveau
 * </pre>
 * @author Junior 
 * @author Matthias
 */
public class RecapScanActivity extends Activity {

	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_RecapScan";

	/** Variable qui correspond au passage de l'agent (lorsqu'un agent part de cette page en ayant r�alis� des scans le passage est cr��) */
	private static Passage passage;
	/** Bouton pour valider le passage */
	private Button boutonValider;
	/** Bouton qui permet de scanner � nouveau des cassettes */
	private Button boutonScanAgain;
	/** Activit�e */
	private Activity thisActivity;
	/** List qui contient les diff�rents groupes de k7 (ici on trouve des k7 sales et des k7 propres seulement pour le moment) */
	private List<EtatK7> groupes;
	/** List qui contient des listes de ScanK7 pour chqaue groupe */
	private static List<List<ScanK7>> childItems;
	/** ExpandableList qui permet d'afficher la liste des scanners selon leur type */
	private static ExpandableListView expandableList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.recap_scan_activity);

		expandableList = (ExpandableListView) findViewById(R.id.expandable_list_scans);

		// R�cup�ration du bundle pass� par l'activit� pr�c�dente
		Bundle bundle = getIntent().getExtras();
		// R�cup�ration du passage grace au bundle
		passage = (Passage) bundle.getSerializable(Constantes.PASSAGE);		

		thisActivity = this;

		expandableList.setDividerHeight(2);
		expandableList.setGroupIndicator(null);
		expandableList.setClickable(true);

		setGroupParents();
		setChildData();

		ExpandableAdapter adapter = new ExpandableAdapter(groupes, childItems);

		adapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), this);
		expandableList.setAdapter(adapter);


		// R�cup�ration des boutons
		boutonValider = (Button) findViewById(R.id.button_recap_passage);
		boutonScanAgain = (Button) findViewById(R.id.button_scan_again);

		boutonValider.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// On passe � l'activit� suivante
				Intent intent = new Intent(thisActivity, EndPassageActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable(Constantes.PASSAGE, passage);
				intent.putExtras(bundle);
				startActivity(intent);
				thisActivity.finish();
			}
		});

		boutonScanAgain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// On renvoie � l'activit� MenuScanActivity pour r�aliser � nouveau des scans
				Intent mIntent = new Intent(thisActivity, MenuScanActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable(Constantes.PASSAGE, passage);
				mIntent.putExtras(bundle);
				startActivity(mIntent);
				thisActivity.finish();
			}
		});
	}

	/** method to add parent Items */
	public void setGroupParents(){
		groupes = new ArrayList<EtatK7>();
		groupes.add(EtatK7.PROPRE);
		groupes.add(EtatK7.SALE);
		// groupes.add(...); A faire si on a besoins des autres EtatK7
	}

	/** method to set child data of each parent */
	public void setChildData(){
		childItems = new ArrayList<List<ScanK7>>();
		for(EtatK7 etat: groupes){
			// Contient les k7 correspondant � l'etat en cours
			List<ScanK7> listeScan = new ArrayList<ScanK7>();
			for(ScanK7 scan: passage.getListScank7()){
				if(scan.getEtatk7().equals(etat)){
					listeScan.add(scan);
				}
			}
			childItems.add(listeScan);
		}
	}

	/**
	 *  M�thode qui se lance quand on appuie sur le bouton "Physique" de retour 
	 */
	@Override
	public void onBackPressed(){
		afficherAlerteRetourMainActivity();
	}

	private void afficherAlerteRetourMainActivity(){
		// On cr�� une boite de dialogue
		AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
		// On lui ajoute un titre
		builder.setTitle(thisActivity.getResources().getString(R.string.warning_title));
		// On affiche le message
		builder.setMessage(thisActivity.getResources().getString(R.string.warning_quit));
		//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
		builder.setPositiveButton(thisActivity.getResources().getString(R.string.default_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On retourne � l'activit� principale
				startActivity(new Intent(thisActivity, MainActivity.class));
				thisActivity.finish();
			}
		}); 
		builder.setNegativeButton(thisActivity.getResources().getString(R.string.default_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On ferme la bo�te de dialogue
				dialog.dismiss();
			}
		});
		builder.show();
	}

	/**
	 * M�thode qui permet de supprimer un �l�ment de la liste de scanners
	 * 
	 * @param groupPosition Index de la liste dans laquelle supprimer
	 * @param scan Scan � supprimer
	 */
	public static void deleteScan(int groupPosition, ScanK7 scan){
		childItems.get(groupPosition).remove(childItems.get(groupPosition).indexOf(scan));
		passage.getListScank7().remove(passage.getListScank7().indexOf(scan));
	}

	public static void collapse(int groupPosition){
		expandableList.collapseGroup(groupPosition);
	}
}
