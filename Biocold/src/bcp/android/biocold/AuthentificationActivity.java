package bcp.android.biocold;

import java.util.List;

import tools.Utility;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import bcp.android.db.adapters.AdapterAgent;
import bcp.android.db.adapters.AdapterContrat;
import bcp.android.db.adapters.AdapterEssai;
import bcp.android.db.adapters.AdapterPartenaire;
import bcp.android.db.adapters.AdapterUtilisateur;
import bcp.android.metier.Agent;
import bcp.android.metier.Constantes;
import bcp.android.metier.Contrat;
import bcp.android.metier.Essai;
import bcp.android.metier.Partenaire;
import bcp.android.metier.Utilisateur;
import bcp.android.parsers.ParserXMLAgent;
import bcp.android.parsers.ParserXMLContrat;
import bcp.android.parsers.ParserXMLEssai;
import bcp.android.parsers.ParserXMLPartenaire;
import bcp.android.parsers.ParserXMLUtilisateur;
import bpc.android.biocold.R;

/**
 * 
 * <p>
 * Permet de s'authentifier sur l'application et ainsi savoir quel utilisateur/Agent/Partenaire utilise l'application.
 * </p>
 * 
 * @author Junior & Matthias
 * 
 */
public class AuthentificationActivity extends Activity implements OnClickListener {

	/** Pour les log */
	private final static String TAG = "BIOCOLD_Authentification";
	/** Bouton de connexion */
	private Button btnConnecter;
	/** Champs de saisie du login */
	private EditText username;
	/** Champs de saisie du mot de passe */
	private EditText password;
	/** Fichier de pr�f�rences de l'application */
	private SharedPreferences settings;
	/** Utilisateur r�cup�r� grace � la requ�te http */
	private Utilisateur user;
	/** Partenaire associ� � l'utilisateur */
	private Partenaire partenaire;
	/** Agent associ� � l'utilisateur */
	private Agent agent;
	/** Liste des contrats du partenaire qui utilise l'application */
	private List<Contrat> listContrat;
	/** Liste des essais du partenaire qui utilise l'application */
	private List<Essai> listEssai;
	/** Permet d'avoir acc�s � l'activit� courante partout dans cette classe **/
	private Activity thisActivity;
	/** Progress bar lors du chargement **/
	private ProgressBar progressBar;
	/** Booleen qui permet de d�terminer si l'utilisateur existe ou non en base de donn�es locale */
	private boolean userExists = false;

	private String userLogin;
	private String userPwd;

	/** M�thode appel�e au lancement de l'activit� */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.authentification_activity);
		thisActivity = this;

		progressBar = (ProgressBar)findViewById(R.id.progressBar);
		btnConnecter = (Button) findViewById(R.id.loginbutton);

		//Pour que la progressBar ne se voit pas avant que le chargement ne soit vraiment lanc�.
		progressBar.setVisibility(View.INVISIBLE);

		// Verifie si nous nous somme d�j� logger avant
		settings = getSharedPreferences(Constantes.PREFS_NAME, 0);

		/*
		 *  On essaie de se connecter apr�s la r�cup�ration des pr�f�rences de l'application
		 *  La m�thode nous connecte si l'utilisateur s'est d�j� connect� et n'a pas effacer les param�tres de l'application
		 */
		connecter();


		btnConnecter.setOnClickListener(this);
	}

	/** M�thode h�rit�e de l'interface OnClickListener */
	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.loginbutton){

			progressBar.setVisibility(View.VISIBLE);
			btnConnecter.setClickable(false);

			username = (EditText) findViewById(R.id.edit_login);
			password = (EditText) findViewById(R.id.edit_password);

			userLogin = username.getText().toString();
			userPwd = password.getText().toString();

			//Les deux lignes suivante permette de forcer � cacher le clavier
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(username.getWindowToken(), 0);
			// .trim() de la classe String sert � supprimer les espaces au d�but et � la fin de la chaine
			if(userLogin != null && userLogin.trim().length() > 0 && userPwd != null && userPwd.trim().length() > 0)
			{
				if(new AdapterUtilisateur(getApplicationContext()).find(userLogin) != null){
					user = new AdapterUtilisateur(getApplicationContext()).find(userLogin);
					userExists = true;
					if(user.getPassword().equals(Utility.convertMD5(userPwd.trim()))){
						Editor editor = settings.edit();
						editor.putString(Constantes.ID_LOGGED, "logged");
						editor.putString(Constantes.ID_PARTENAIRE, user.getIdPartenaire());
						editor.putString(Constantes.ID_USER, user.getId());
						editor.putString(Constantes.ID_AGENT, new AdapterAgent(getApplicationContext()).getId(user.getId()));
						editor.commit();
						connecter();
					}
					// Donc si l'utilisateur s'est tromp� dans son mot de passe ou � changer de mot de passe 
					// (C'est pour �a qu'on va v�rifier dans la base externe)
					else {
						if(Utility.isNetworkAvailable(thisActivity))
						{
							new LoadXMLData().execute();	
						}
						else
						{
							Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.error_login), Toast.LENGTH_LONG).show();
							progressBar.setVisibility(View.INVISIBLE);
							btnConnecter.setClickable(true);
						}
					}
				}
				else {
					if(Utility.isNetworkAvailable(thisActivity))
					{
						new LoadXMLData().execute();	
					}
					else
					{
						noConnectionAlert();
						progressBar.setVisibility(View.INVISIBLE);
						btnConnecter.setClickable(true);
					}	
				}
			}
			// Si un des champs (voire les deux) n'est pas renseigner : 
			else{
				Toast.makeText(this, thisActivity.getResources().getString(R.string.warning_login), Toast.LENGTH_LONG).show();
				progressBar.setVisibility(View.INVISIBLE);
				btnConnecter.setClickable(true);
			}
		}
	}

	/** 
	 *  Cette classe permet de r�cup�rer le XML :
	 *  - d'un partenaire
	 *  - d'un utilisateur
	 *  - d'un agent 
	 *  - des contrats
	 *  - des types de passage
	 * **/
	private class LoadXMLData extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... arg0) {			
			if(userExists){
				user = new ParserXMLUtilisateur().getUser(userLogin);
				if(user.getPassword().equals(Utility.convertMD5(userPwd.trim()))){
					Editor editor = settings.edit();
					editor.putString(Constantes.ID_LOGGED, "logged");
					editor.putString(Constantes.ID_PARTENAIRE, user.getIdPartenaire());
					editor.putString(Constantes.ID_USER, user.getId());
					editor.putString(Constantes.ID_AGENT, new AdapterAgent(getApplicationContext()).getId(user.getId()));
					editor.commit();
				}
				// Donc si l'utilisateur s'est tromp� dans son mot de passe
				else{
					return thisActivity.getResources().getString(R.string.error_login);
				}
			}
			else{
				// Bloc pour r�cup�rer l'utilisateur
				try{
					user = new ParserXMLUtilisateur().getUser(userLogin);
					if(user != null && user.getPassword().equals(Utility.convertMD5(userPwd.trim()))){
						Editor editor = settings.edit();
						editor.putString(Constantes.ID_LOGGED, "logged");
						editor.putString(Constantes.ID_PARTENAIRE, user.getIdPartenaire());
						editor.putString(Constantes.ID_USER, user.getId());
						editor.commit();
					}
					// Donc si l'utilisateur s'est tromp� dans son mot de passe
					else{
						return thisActivity.getResources().getString(R.string.error_login);
					}
				}
				// L'objet InputStream ne s'est pas bien charg�
				catch (Exception e) {
					return thisActivity.getResources().getString(R.string.error_login);
				}
				// Informations sur l'agent qui vient de se connecter
				try {
					ParserXMLAgent pagent = new ParserXMLAgent();
					agent = pagent.getAgent(user.getId());
					Editor editor = settings.edit();
					editor.putString(Constantes.ID_AGENT, agent.getIdagent());
					editor.commit();
				}
				catch(Exception e){
					Log.e(TAG, "Chargement XML de l'agent", e);
				}
				// Information sur le partenaire li� � l'agent qui vient se connecter
				try {
					/* R�cup�ration des information du partenaire qui est li� � l'agent qui vient de se connecter */
					partenaire = new ParserXMLPartenaire().getPartenaire(user.getIdPartenaire());
				} 
				catch (Exception e) {
					Log.e(TAG, "Chargement XML du partenaire", e);
				}

				// Liste des contrats
				try {
					/* R�cup�ration de la liste des contrats li� au partenaire qui est lui m�me li� � l'agent qui vient de se connecter */
					listContrat = new ParserXMLContrat().getListContrat(partenaire.getId());
				} 
				catch (Exception e) {
					Log.e(TAG, "Chargement XML des contrats", e);
				}
				// Liste des essais
				try {
					/* R�cup�ration de la liste des essais li� au partenaire qui est lui m�me li� � l'agent qui vient de se connecter */
					listEssai = new ParserXMLEssai().getListEssai(partenaire.getId());
				} 
				catch (Exception e) {
					Log.e(TAG, "Chargement XML des essais", e);
				}
			}			
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(result != null){
				Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
				// Comme cette m�thode est utilis�e uniquement en cas d'erreur de saisie ici on rend le bouton clickable
				// et la progressbar invisible � nouveau ici
				progressBar.setVisibility(View.INVISIBLE);
				btnConnecter.setClickable(true);
			}
			/*
			 *  Maintenant qu'on � r�cup�rer l'utilisateur et le partenaire on cr�� les 
			 *  Adapters afin de les ins�rer en base de donn�es (dans le bon ordre pour 
			 *  ne pas avoir de conflits avec les cl�s �trang�res (Voir la classe priv�e :
			 *  InsertPartenaireAndUtilisateur )
			 */
			// Chargement de l'utilisateur, de l'agent, du partenaire venant de se connecter et de la liste des contrats.
			else
				new LoadData().execute();
		}
	}

	/** 
	 *  Cette classe permet d'ins�rer 
	 *  - l'utilisateur
	 *  - l'agent
	 *  - le partenaire 
	 *  - la liste des contrats li�s � l'utilisateur venant de se connecter en db local 
	 *  - Les types de passage
	 *  Si ils sont tous dans la m�me tache asynchrone c'est pour emp�cher un acc�s concurrent � la base de donn�es et parce que le partenaire
	 *  doivent �tre ins�r�s avant l'utilisateur
	 **/
	private class LoadData extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... arg0) {
			if(userExists)
				new AdapterUtilisateur(thisActivity).update(user);
			// On execute cette op�ration que si l'utilisateur n'�tait pas dans la base de donn�es avant
			else{
				// Cr�ation d'un adapter pour envoyer le partenaire dans la base de donn�es locale
				AdapterPartenaire adapterPartenaire = new AdapterPartenaire(thisActivity);
				adapterPartenaire.insert(partenaire);


				// Cr�ation d'un adapter pour envoyer l'utilisateur dans la base de donn�es locale
				AdapterUtilisateur adapterUtilisateur = new AdapterUtilisateur(thisActivity);
				adapterUtilisateur.insert(user);

				// Cr�ation d'un adapter pour envoyer l'agent dans la base de donn�es locale
				AdapterAgent adapterAgent = new AdapterAgent(thisActivity);
				adapterAgent.insert(agent);

				// Cr�ation d'un adapter pour envoyer les contrats du partenaire dans la base de donn�es local
				AdapterContrat adapterContrat = new AdapterContrat(thisActivity);
				adapterContrat.insert(listContrat);
				
				// Cr�ation d'un adapter pour envoyer les essais du partenaire dans la base de donn�es local
				AdapterEssai adapterEssai = new AdapterEssai(thisActivity);
				adapterEssai.insert(listEssai);
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			//Connecte l'utilisateur apr�s que le XML soit charger
			connecter();
		}
	}

	/** M�thode de connexion */
	private void connecter(){
		// Si le fichier de settings nous dit que l'utilisateur est connect� : 
		if (settings.getString("logged", "").toString().equals("logged")) {
			Intent intent = new Intent(this, MainActivity.class);
			// On lance notre activit� suivante
			startActivity(intent);
			// On fini cette acitivt� � la connexion pour ne pas retomber dessus en faisant un retour arri�re dans l'application
			this.finish();
		}
		else{
			btnConnecter.setClickable(true);
			progressBar.setVisibility(View.INVISIBLE);
		}
	}

	public void noConnectionAlert(){
		// On cr�� une boite de dialogue
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// On lui ajoute un titre
		builder.setTitle(thisActivity.getResources().getString(R.string.error_title_connexion));
		// On affiche le message
		builder.setMessage(thisActivity.getResources().getString(R.string.error_connexion_server));
		//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
		builder.setPositiveButton(thisActivity.getResources().getString(R.string.default_ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		builder.show();
	}
}