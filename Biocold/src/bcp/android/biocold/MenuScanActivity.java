package bcp.android.biocold;

import java.util.GregorianCalendar;

import tools.LocationRunnable;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import bcp.android.metier.Constantes;
import bcp.android.metier.Passage;
import bcp.android.metier.ScanK7;
import bcp.android.metier.enums.EtatK7;
import bpc.android.biocold.R;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 *
 * 
 * Permet de choisir si l'on refais un scan ou si l'on retourne au menu principal (acceuil)
 * 
 * <pre>Cette activit� peut �tre lanc� depuis :
 * - NewPassage (mais qui passe d'abord par l'activit� de l'application Barre Code)
 * - RecapScanActivity (quand l'utilisateur a choisis de scanner � nouveau)
 * 
 * Cette activit� m�ne � : 
 * - MainActivity
 * - RecapScanActivity
 * </pre>
 * @author Junior 
 * @author Matthias
 */

public class MenuScanActivity extends Activity {
	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_MenuScan";
	/** bouton pour r�aliser un nouveau scan de cassette(s) propre(s) */
	private Button btn_new_scan_clean;

	/** Bouton pour r�aliser un nouveau scan de cassette(s) propre(s) */
	private Button btn_new_scan_dirty;

	/** Variables qui r�cup�reront les informations de localisation */
	private String latitude, longitude;

	/** Objet pour stocker l'activit� (utilis� dans la d�claration des OnClicksListeners) */
	private Activity thisActivity;

	/** Objet de scan */
	private ScanK7 scan;

	/** On stock l'�tat dans cette variable */
	private EtatK7 etat;

	/** On enregistre le passage dans cette variable et on l'envoie � l'activit� suivante */
	private Passage passage;

	/** Bouton pour terminet les scanners */ 
	private Button btn_finish;
	/** Nombre de k7 sales scann�es */
	private static int nbK7Sales;
	/** Nombre de k7 propres scann�es */
	private static int nbK7Propres;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_scan_activity);
		thisActivity = this;
		
		// Initialisation du nombre de scans
		nbK7Propres = 0;
		nbK7Sales = 0;
		latitude = LocationRunnable.getLatitude();
		longitude = LocationRunnable.getLongitude();
		// R�cup�ration du bundle pass� par l'activit� pr�c�dente
		Bundle bundle = getIntent().getExtras();
		
		//Apr�s chaque scan
		if((Passage) bundle.getSerializable(Constantes.PASSAGE) != null){
			passage = (Passage) bundle.getSerializable(Constantes.PASSAGE);
			//On compte le nombre de k7 d�j� saisies
			for(ScanK7 scan: passage.getListScank7()){
				if(scan.getEtatk7().equals(EtatK7.PROPRE))
					nbK7Propres += 1;
				else
					nbK7Sales += 1;
			}
		}
		
		btn_new_scan_clean = (Button)findViewById(R.id.button_new_scan_clean);
		btn_new_scan_dirty = (Button)findViewById(R.id.button_new_scan_dirty);
		
		//On ajoute le nombre de k7 scann�es dans le texte du bouton
		btn_new_scan_clean.setText(getResources().getString(R.string.new_scan_clean) + "\n(" + nbK7Propres + ")");
		//On fait de m�me avec les k7 sales
		btn_new_scan_dirty.setText(getResources().getString(R.string.new_scan_dirty) + "\n(" + nbK7Sales + ")");
		
		//Les conditions qui suivent permettent d'activer ou d�sactiver l'un des boutons en fonction du type de passage
		switch (passage.getTypepassage()) {
		case REPLACE:
			//Les deux bouttons doivent �tre pr�sent
			break;
		case INSTALL_WITH_REPLACE:
			//Les deux bouton doivent �tre pr�sent
			break;
		case INSTALL_WITHOUT_REPLACE:
			//L'utilisateur ne doit pas arriver sur cette activit�
			break;
		case RECOVRY_END_CONTRACT:
			//Seul la r�cup�ration de cassettes sales doit �tre disponible
			btn_new_scan_clean.setVisibility(View.GONE);
			break;
		case RECOVRY_END_ESSAI:
			//Seul la r�cup�ration de cassettes sales doit �tre disponible
			btn_new_scan_clean.setVisibility(View.GONE);
			break;
		case REFUSAL_WITH_MADE:
			//Seul la r�cup�ration de cassettes sales doit �tre disponible
			btn_new_scan_clean.setVisibility(View.GONE);
			break;
		case REFUSAL_WITHOUT_MADE:
			//L'utilisateur ne doit pas arriver sur cette activit�
			break;
		case REPLACE_DURING_ESSAI:
			//Les deux bouttons doivent �tre pr�sent
			break;
		case START_ESSAI:
			//Seul le button de cassettes propre doit �tre disponible
			btn_new_scan_dirty.setVisibility(View.GONE);
			break;
		default:
			break;
		}
		
		btn_new_scan_clean.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				etat = EtatK7.PROPRE;
				startScanner();
			}
		});

		btn_new_scan_dirty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				etat = EtatK7.SALE;
				startScanner();
			}
		});		

		btn_finish = (Button) findViewById(R.id.button_finish);

		if(passage.getListScank7() == null || passage.getListScank7().size() <= 0){
			btn_finish.setVisibility(View.INVISIBLE);
		}
		btn_finish.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// On passe � l'activit� suivante
				Intent mIntent = new Intent(thisActivity, RecapScanActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable(Constantes.PASSAGE, passage);
				mIntent.putExtras(bundle);
				startActivity(mIntent);
				thisActivity.finish();
			}
		});
	}

	/** 
	 * - Appelle le scanner
	 * - R�cup�re la position au moment o� elle est appell�e 
	 */
	public void startScanner(){
		//Scan
		IntentIntegrator scanIntegrator = new IntentIntegrator(this);
		scanIntegrator.addExtra("SCAN_WIDTH", 800);
		scanIntegrator.addExtra("SCAN_HEIGHT", 500);
		scanIntegrator.addExtra("RESULT_DISPLAY_DURATION_MS", 0L);
		scanIntegrator.addExtra("PROMPT_MESSAGE", this.getResources().getString(R.string.message_bar_code));

		//Cette ligne doit �tre la derni�re de la m�thode. Car elle lance l'ex�cution d'une application Scanner
		//Si l'on met encore du code � la suite de cette m�thode celui-ci sera ex�cut� mais risque fortement de faire planter l'application si 
		//l'on touche � l'affichage. Car l'affichage de l'application Biocold laisse place � celui de l'application scanner. 
		//Et donc l'application biocold n'est plus au premier plan.
		//La suite de cette instruction est en fait l'appelle � la m�thode onActivityResult(int requestCode, int resultCode, Intent intent) 
		//C'est lors du retour de l'application scanner que l'on arrive automatiquement dans cette m�thode.
		scanIntegrator.initiateScan();
	}

	/**
	 * - Envoie les informations du scanner vers la db local ou externe en fonction de la disponibilit� de celle-ci
	 * - Envoie vers l'activit� suivante
	 * Les informations sont : 
	 * 	- Le code
	 *  - La date o� est pris le scan
	 *  - ...
	 */
	public void afterScanner()
	{
		if(scan.getCodebarre() != null)
		{
			// Ajout du scanner dans la liste
			ajouterScan(scan);
		}
	}

	/**
	 * Apr�s le scan, l'application reprend dans cette m�thode.
	 * Permet de r�cup�rer le code barre et son format
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent){
		// Renvoie le r�sultat du scan
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (intent != null) {
			// Permet de v�rifier si le scan a d�j� �t� r�alis�
			boolean verifScan = true;
			int i = 0;
			if(passage.getListScank7().size() > 0){
				while(i < passage.getListScank7().size() && verifScan == true){
					if(passage.getListScank7().get(i).getCodebarre().equals(scanningResult.getContents()))
						verifScan = false;
					i++;
				}				
			}
			try{
				if(verifScan){
					//Si la k7 ajout�e est propre on ajoute une k7 propre au compte
					if(etat.equals(EtatK7.PROPRE)){
						nbK7Propres += 1;
						btn_new_scan_clean.setText(getResources().getString(R.string.new_scan_clean) + "\n(" + nbK7Propres + ")");
					}
					else{
						nbK7Sales += 1;
						btn_new_scan_dirty.setText(getResources().getString(R.string.new_scan_dirty) + "\n(" + nbK7Sales + ")");
					}
					// On instancie le scan
					scan = new ScanK7();
					scan.setEtatk7(etat);
					// On r�cup�re les informations n�cessaires dans notre objet de scan
					scan.setCodebarre(scanningResult.getContents());
					scan.setLatitude(latitude);
					scan.setLongitude(longitude);
					GregorianCalendar date = new GregorianCalendar();
					scan.setDatesaisie(date);
					// On donne la date de l'�tat de la k7 (pour l'historique dans la base de donn�es)
					scan.setEtatdate(date);
					// R�cup�ration de l'id de l'utilisateur avec les SharedPreferences
					scan.setIdUser(getSharedPreferences(Constantes.PREFS_NAME, 0).getString(Constantes.ID_USER, ""));
					afterScanner();
				}
				else{
					Toast.makeText(this, thisActivity.getResources().getString(R.string.error_same_scan), Toast.LENGTH_LONG).show();
				}
			}
			catch(NullPointerException npe){
				//Toast.makeText(this, this.getResources().getString(R.string.error_init_scan), Toast.LENGTH_LONG).show();
			}
		}
		else {
			// Si il n'y a pas de r�sultat renvoy�
			Toast.makeText(getBaseContext(), this.getResources().getString(R.string.error_scan), Toast.LENGTH_LONG).show();
		}
		if(passage.getListScank7() != null && passage.getListScank7().size() > 0){
			btn_finish.setVisibility(View.VISIBLE);
		}
	}
	/**
	 * M�thode pour ajouter un nouveau scan dans la liste
	 * 
	 * <p>Cette m�thode v�rifie que le scanner existe avant de l'ajouter dans la liste avec la m�thode {@link #checkBarCode()}</p>
	 * 
	 * @param scan � ajouter dans la liste
	 */
	private void ajouterScan(ScanK7 scan){
		if(checkBarCode(scan.getCodebarre())){
			passage.addScank7(scan);
		}
		else{
			Toast.makeText(this,this.getResources().getString(R.string.error_bar_code_not_exist) , Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Cette m�thode doit permettre de v�rifier que le code barre scann� correspond bien 
	 * � une k7 de BiocoldProcess
	 * 
	 * @param barCode est le code barre que l'on doit v�rifier
	 */
	private boolean checkBarCode(String barCode){
		/* 
		 * TODO La m�thode sera impl�ment�e quand les bases de donn�es et les services
		 * web permettront de v�rifier que les codes barre existent
		 */
		return true;
	}
	
	/**
	 *  M�thode qui se lance quand on appuie sur le bouton "Physique" de retour 
	 */
	@Override
	public void onBackPressed(){
		afficherAlerteRetourMainActivity();
	}

	private void afficherAlerteRetourMainActivity(){
		// On cr�� une boite de dialogue
		AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
		// On lui ajoute un titre
		builder.setTitle(thisActivity.getResources().getString(R.string.warning_title));
		// On affiche le message
		builder.setMessage(thisActivity.getResources().getString(R.string.warning_quit));
		//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
		builder.setPositiveButton(thisActivity.getResources().getString(R.string.default_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On retourne � l'activit� principale
				startActivity(new Intent(thisActivity, MainActivity.class));
				thisActivity.finish();
			}
		}); 
		builder.setNegativeButton(thisActivity.getResources().getString(R.string.default_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On ferme la bo�te de dialogue
				dialog.dismiss();
			}
		});
		builder.show();
	}
}