package bcp.android.biocold;

import tools.Utility;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import bcp.android.metier.Constantes;
import bcp.android.service.SynchroEssaiContractService;
import bcp.android.service.SynchroPassageService;
import bpc.android.biocold.R;

/**
 * <p>
 * C'est l'activit� de base, l'activt� d'accueil et qui contient donc le menu principal 
 * </p>
 * <p>
 * Cette activit� peut �tre lanc� depuis :
 * - Authentification
 * </p>
 * <p>
 * Cette activit� m�ne � : 
 * - ChoiceTypePassage
 * - ListLastPassage
 * </p>
 * <p>
 * Cette activit� permet de lancer manuellement le service de synchronisation des contrats.
 * </p>
 * @author Junior & Matthias
 */
public class MainActivity extends Activity implements OnClickListener{
	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_Main";
	
	/** Boutons pour les diff�rentes parties de l'application */
	private Button btnService, btnLastPassage, btnTypesPassage;
	
	/** Cette activit� */
	Activity thisActivity;


	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		thisActivity = this;

		// On d�finie les diff�rents composants de l'activit�
		btnService = (Button)findViewById(R.id.button_synchro_contracts);
		btnLastPassage = (Button)findViewById(R.id.bouton_list_last_passage);
		btnTypesPassage = (Button)findViewById(R.id.button_choice_type_passage);
		
		btnService.setOnClickListener(this);
		btnLastPassage.setOnClickListener(this);
		btnTypesPassage.setOnClickListener(this);

		//Appelle du service de synchronisation des passages et des scans qui leur sont li�s.
		startService(new Intent(thisActivity, SynchroPassageService.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater Inflater = getMenuInflater();
		Inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.logout) {
			SharedPreferences settings = getSharedPreferences(Constantes.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.remove(Constantes.ID_LOGGED);
			editor.remove(Constantes.ID_AGENT);
			editor.remove(Constantes.ID_USER);
			editor.remove(Constantes.ID_PARTENAIRE);
			editor.commit();
			Intent intent = new Intent(this, AuthentificationActivity.class);
			startActivity(intent);
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.button_synchro_contracts){
			if(Utility.isNetworkAvailable(thisActivity))
			{
				Intent intent = new Intent(thisActivity, SynchroEssaiContractService.class);
				startService(intent);
			}
			else
				displayAlertToNoConnectionNetwork();
		}
		else if(v.getId()==R.id.bouton_list_last_passage){
			if(Utility.isNetworkAvailable(thisActivity))
			{
				Intent intent = new Intent(thisActivity, ListLastPassageActivity.class);
				startActivity(intent);	
				this.finish();
			}
			else
				displayAlertToNoConnectionNetwork();
		}
		else if(v.getId() == R.id.button_choice_type_passage){
			Intent intent = new Intent(thisActivity, ChoiceTypePassageActivity.class);
			startActivity(intent);
			finish();
		}
	}

	@Override
	public void onBackPressed(){
		/*
		// On cr�� une boite de dialogue
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// On lui ajoute un titre
		builder.setTitle(this.getResources().getString(R.string.title_quitter));
		// On affiche le message
		builder.setMessage(this.getResources().getString(R.string.message_quitter));
		//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
		builder.setPositiveButton(this.getResources().getString(R.string.default_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On ferme l'activit�
				thisActivity.finish();
			}
		});
		builder.setNegativeButton(this.getResources().getString(R.string.default_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On ferme la bo�te de dialogue
				dialog.dismiss();
			}
		});
		builder.show();
		*/
		// On ferme l'activit�
		thisActivity.finish();
	}
	
	private void displayAlertToNoConnectionNetwork(){
		// On cr�� une boite de dialogue
		AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
		// On lui ajoute un titre
		builder.setTitle(thisActivity.getResources().getString(R.string.warning_title_see_passage_list));
		// On affiche le message
		builder.setMessage(thisActivity.getResources().getString(R.string.warning_message_see_passage_list));
		//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
		builder.setPositiveButton(thisActivity.getResources().getString(R.string.bouton_see_passage_list), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//Permet de renvoyer vers les settings permettant � l'utilisateur de se connecter au r�seau
				startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
				
				//Si bien connect� renvoie vers la liste des derniers passages
				if(Utility.isNetworkAvailable(thisActivity))
				{
					Intent intent = new Intent(thisActivity, ListLastPassageActivity.class);
					startActivity(intent);	
					thisActivity.finish();
				}
			}
		}); 
		builder.setNegativeButton(thisActivity.getResources().getString(R.string.default_cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On ferme la bo�te de dialogue
				dialog.dismiss();
			}
		});
		builder.show();
	}
}
