package bcp.android.biocold;

import java.util.GregorianCalendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import bcp.android.db.adapters.AdapterClient;
import bcp.android.db.adapters.AdapterEssai;
import bcp.android.metier.Client;
import bcp.android.metier.Constantes;
import bcp.android.metier.Essai;
import bcp.android.metier.Passage;
import bpc.android.biocold.R;

public class NewEssaiActivity extends Activity{
	/** Permet d'avoir acces � l'activite courante partout dans cette classe **/
	private Activity thisActivity;
	/** Bouton Continuer*/
	private Button buttonContinuer;

	/** Edittext qui permet r�cup�rer et g�rer les diff�rents champs texte */
	private EditText editTextNomCommerce, editTextTypeCommerce, editTextNomGerant, editTextCivilite, editTextPrenomGerant, editTextAdressCommmerce, editTextCodePostal, editTextVille, editTextNumeroTelephone, editTextObservation, editTextNbCassette;

	/** Variable contenant le passage avec lequel on travail*/
	private Passage passage;

	/** Variable qui contiendra tout les informations d'un essai dans l'activit� pr�sente **/
	private Essai essai = null;

	/** Variable qui contiendra tout les informations d'un client dans l'activit� pr�sente **/
	private Client client = null;

	@Override
	protected void onCreate( Bundle  savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_essai_activity);

		thisActivity = this;
		
		//Permet de r�cup�rer le passage avec lequel on travail
		Bundle bundleReceiver = getIntent().getExtras();
		passage = (Passage) bundleReceiver.getSerializable(Constantes.PASSAGE);
		
		buttonContinuer = (Button)findViewById(R.id.button_continue_essai);
		editTextNomCommerce = (EditText)findViewById(R.id.editText_nom_commerce);
		editTextTypeCommerce = (EditText)findViewById(R.id.editText_type_commerce);
		editTextCivilite = (EditText)findViewById(R.id.editText_title_civilite);
		editTextNomGerant = (EditText)findViewById(R.id.editText_name);
		editTextPrenomGerant = (EditText)findViewById(R.id.editText_firstname);
		editTextAdressCommmerce = (EditText)findViewById(R.id.editText_adresse_essai);
		editTextCodePostal = (EditText)findViewById(R.id.editText_codepostal_essai);
		editTextVille = (EditText)findViewById(R.id.editText_ville_essai);
		editTextNumeroTelephone = (EditText)findViewById(R.id.editText_number_phone);
		editTextObservation = (EditText)findViewById(R.id.editText_observation_essai);
		editTextNbCassette = (EditText)findViewById(R.id.editText_nbCasette);


		buttonContinuer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(v.getId() == R.id.button_continue_essai){
					String nomCommerce = editTextNomCommerce.getText().toString();
					if(!nomCommerce.equals(""))
					{	
						//Initialisation du client qui va �tre compl�ter gr�ce au diff�rent champ texte.
						client = new Client();
						client.setNomCommerce(editTextNomCommerce.getText().toString());
						client.setTypeCommerce(editTextTypeCommerce.getText().toString());
						client.setCivilite(editTextCivilite.getText().toString());
						client.setNom(editTextNomGerant.getText().toString());
						client.setPrenom(editTextPrenomGerant.getText().toString());
						client.setAdresseCommerce(editTextAdressCommmerce.getText().toString());
						client.setCodePostalCommerce(editTextCodePostal.getText().toString());
						client.setVilleCommerce(editTextVille.getText().toString());
						client.setTelephone(editTextNumeroTelephone.getText().toString());
						//Ce client �tant cr�� � l'instant (il n'existe pas en db externe) pour le diff�rencier 
						//de d'autre qui pourrait �tre existant en db externe mais import� en db local, on lui assigne un flag "new" � true
						
						
						//Initialisation de l'essai qui va �tre compl�ter gr�ce au diff�rent champ texte.
						essai = new Essai();
						essai.setDateDebut(new GregorianCalendar());
						essai.setDateDernierPassage(new GregorianCalendar());
						essai.setIdPartenaire(getSharedPreferences(Constantes.PREFS_NAME, 0).getString(Constantes.ID_PARTENAIRE, ""));
						essai.setNbCassette(editTextNbCassette.getText().toString());
						essai.setObservation(editTextObservation.getText().toString());
						//Cet essai �tant cr�� � l'instant (il n'existe pas en db externe) pour le diff�rencier 
						//de d'autre qui pourrait �tre existant en db externe mais import� en db local, on lui assigne un flag "new" � true
						
						
						/* ETAPE 1 */
						//Attribuer un id provisoire au client(Provisoire car valable tant que le client n'a pas �t� enregistr� en externe) 
						//Client enregistrer en db local
						client.setId(String.valueOf(new AdapterClient(getApplicationContext()).insert(client)));
						
						/* ETAPE 2 */
						//Lier l'essai au client (Gr�ce � l'id provisoire du client)
						essai.setIdClient(client.getId());
						
						/* ETAPE 3 */
						//Attribuer un id provisoire � l'essai(Provisoire car valable tant que le client n'a pas �t� enregistr� en externe) 
						//Essai enregistrer en db local
						essai.setId(String.valueOf(new AdapterEssai(getApplicationContext()).insert(essai)));
						
						/* ETAPE 4 */
						//Lier le passage � l'essai (Gr�ce � l'id provisoire de l'essai)
						passage.setIdEssai(essai.getId());
						
						//Permet de lancer l'activite MenuScanActivity
						Intent mIntent = new Intent(thisActivity, MenuScanActivity.class);
						Bundle bundleSend = new Bundle();
						bundleSend.putSerializable(Constantes.PASSAGE, passage);
						mIntent.putExtras(bundleSend);
						startActivity(mIntent);

						//Permet de mettre fin � cette activite et donc la supprimer de la memoire.
						thisActivity.finish();
					}
					else
					{
						Toast toast = Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.message_new_essai_vide), Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
						textView.setTextColor(getResources().getColor(R.color.red));
						toast.show();
					}
				}
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(thisActivity, ChoiceTypePassageActivity.class);
		startActivity(mIntent);
		thisActivity.finish();
		super.onBackPressed();
	}
}
