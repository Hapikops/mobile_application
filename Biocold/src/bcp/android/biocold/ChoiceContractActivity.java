package bcp.android.biocold;

import java.util.Calendar;

import tools.Utility;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import bcp.android.db.adapters.AdapterContrat;
import bcp.android.metier.Client;
import bcp.android.metier.Constantes;
import bcp.android.metier.Contrat;
import bcp.android.metier.Passage;
import bcp.android.metier.enums.TypePassage;
import bcp.android.parsers.ParserXMLClient;
import bcp.android.parsers.ParserXMLContrat;
import bpc.android.biocold.R;

/**
 * <p>
 * Cette classe permet de c�er et de g�rer l'activit� ChoiceContract.
 * </p>
 * <pre>
 * Cette activit� peut �tre lanc� depuis :
 * - ChoiceTypePassage
 * Cette activit� m�ne � : 
 * - MenuScan
 * - EndPassage
 * 
 * Cette activit� doit permettre d'entrer un num�ro de contrat  et de le v�rifier  et 
 * de visualiser si possible certaines informations sur le contrat.
 * Si c'est possible cela signifie que le smartphone a acc�s � internet.
 * </pre>
 * @author Junior & Matthiass
 * 
 */
@SuppressLint("DefaultLocale")
public class ChoiceContractActivity extends Activity
{
	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_ChoiceContract";
	/** Bouton permettant de continuer, il lance �galement la v�rification de num�ro de contrat **/
	private Button buttonContinue;

	/** Edit text dans lequel est �crit le num�ro de contrat **/
	private EditText editTextNumberContract;

	/** Variable repr�sentant l'activit� dans tout la classe **/
	private Activity thisActivity;

	/** Text view permettant d'afficher le nom + pr�nom du client **/
	private TextView textViewName;

	/** Text view permettant d'afficher l'adresse du client **/
	private TextView textViewAddress;

	/** Text view permettant d'afficher le nom + pr�nom du client **/
	private TextView textViewLastChangeCassette;

	/** Text view permettant d'afficher l'initial du partenaire **/
	private EditText editTextIdPartenaire;

	/** Text view permettant d'afficher l'�tat du contrat **/
	private TextView textViewStateContract;
	
	/** Parser permettant d'acc�der au contrat en db extrerne **/
	private ParserXMLContrat parserXMLContrat = new ParserXMLContrat();

	/** Parser permettant d'acc�der au client en db extrerne **/
	private ParserXMLClient parserXMLClient = new ParserXMLClient();

	/** Fichier de pr�f�rences de l'application */
	private SharedPreferences settings;

	/** Juste l� pour pouvoir �tre utilis� globalement dans la classe **/
	private String idPartenaire;

	/** Variable qui contiendra tout les informations d'un client dans l'activit� pr�sente **/
	private Client client =  null;

	/** Variable qui contiendra tout les informations d'un contrat dans l'activit� pr�sente **/
	private Contrat contrat = null;

	/** Progress bar lors du chargement **/
	private ProgressBar progressBar;
	
	/** Variable contenant le passage avec lequel on travail */
	private Passage passage;

	/** Variable bool�enne pour stocker la r�ponse de la m�thode checkNumberContract 
	 * (false par d�faut car le num�ro de contrat n'est pas encore v�rifi�)
	 */
	private boolean verifNumContrat = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choice_contract_activity);

		thisActivity = this;
		settings = getSharedPreferences(Constantes.PREFS_NAME, 0);

		buttonContinue = (Button)findViewById(R.id.button_continue);
		editTextNumberContract =  (EditText)findViewById(R.id.editText_number_contract);

		textViewName = (TextView)findViewById(R.id.textView_name);
		textViewAddress = (TextView)findViewById(R.id.textView_address);
		textViewLastChangeCassette = (TextView)findViewById(R.id.textView_last_change_cassette);
		textViewStateContract = (TextView)findViewById(R.id.textView_state_contract);
		editTextIdPartenaire = (EditText)findViewById(R.id.editText_id_partenaire);
	
		progressBar = (ProgressBar)findViewById(R.id.progressBar);

		progressBar.setVisibility(View.INVISIBLE);

		//Permet de r�cup�rer le passage avec lequel on travail
		Bundle bundleReceiver = getIntent().getExtras();
		passage = (Passage) bundleReceiver.getSerializable(Constantes.PASSAGE);
		
		//Permet de modifier le titre de l'activit� :
		thisActivity.setTitle("Biocold - "+passage.getTypepassage().getLibelle(thisActivity));
		
		//Permet d'afficher les initiale du partenaire n�cessaire � la saisie du num�ro de contrat
		idPartenaire = settings.getString(Constantes.ID_PARTENAIRE, "").toString();
		editTextIdPartenaire.setText(idPartenaire);

		editTextNumberContract.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE)
				{
					//Les deux lignes suivante permette de forcer � cacher le clavier
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(editTextNumberContract.getWindowToken(), 0);
					
					verifNumContrat = checkNumberContract(idPartenaire + editTextNumberContract.getText().toString());
					//V�rifie toujours le num�ro de contrat
					if(verifNumContrat)
					{
						//Affciche un toast comme quoi le nu�mro de contrat est correcte
						Toast toast = Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.message_number_contract_correct), Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
						textView.setTextColor(getResources().getColor(R.color.green));
						toast.show();

						//Va chercher et affiche les informations plus compl�te si connexion au net possible
						if(Utility.isNetworkAvailable(thisActivity))
							new LoadContractAndClient().execute();
					}
					else
					{
						//Si le num�ro de contrat est modifi� et qu'il n'est plus un num�ro correcte, on efface les informations pr�c�dement affich�
						textViewName.setText("");
						textViewAddress.setText("");
						textViewLastChangeCassette.setText("");

						//Affiche un toast comme quoi le nu�mro de contrat est incorrecte
						Toast toast = Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.message_number_contract_not_correct), Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
						textView.setTextColor(getResources().getColor(R.color.red));
						toast.show();
					}
				}
				return true;
			}
		});
		

		/**
		 * Appelle de la v�rification du num�ro de contrat et appelle de l'activit� suivante
		 */
		buttonContinue.setOnClickListener(new OnClickListener() {
			@SuppressLint("DefaultLocale")
			@Override
			public void onClick(View v) {
				if(v.getId()==R.id.button_continue){
					verifNumContrat = checkNumberContract(idPartenaire + editTextNumberContract.getText().toString());
					if(verifNumContrat)
					{				
						//Dans le cas o� l'on doit scanner au moins une cassette
						if(passage.getTypepassage() == TypePassage.REPLACE || passage.getTypepassage() == TypePassage.RECOVRY_END_CONTRACT || passage.getTypepassage() == TypePassage.INSTALL_WITH_REPLACE || passage.getTypepassage() == TypePassage.REFUSAL_WITH_MADE)
						{
							passage.setIdContrat(idPartenaire + editTextNumberContract.getText().toString());
							passage.setIdEssai(new AdapterContrat(thisActivity).find(passage.getIdContrat()).getIdEssai());
							
							//Permet de lancer l'activit� MenuScanActivity
							Intent mIntent = new Intent(thisActivity, MenuScanActivity.class);
							Bundle bundleSend = new Bundle();
							bundleSend.putSerializable(Constantes.PASSAGE, passage);
							mIntent.putExtras(bundleSend);
							startActivity(mIntent);
	
							//Permet de mettre fin � cette activit� et donc la supprimer de la m�moire.
							thisActivity.finish();
						}
						//Dans le cas o� l'on doit ne doit pas scanner de cassette
						else if(passage.getTypepassage() == TypePassage.INSTALL_WITHOUT_REPLACE || passage.getTypepassage() == TypePassage.REFUSAL_WITHOUT_MADE)
						{
							passage.setIdContrat(idPartenaire + editTextNumberContract.getText().toString());
							passage.setIdEssai(new AdapterContrat(thisActivity).find(passage.getIdContrat()).getIdEssai());
							//Permet de lancer l'activit� EndPassageActivity
							Intent mIntent = new Intent(thisActivity, EndPassageActivity.class);
							Bundle bundleSend = new Bundle();
							bundleSend.putSerializable(Constantes.PASSAGE, passage);
							mIntent.putExtras(bundleSend);
							startActivity(mIntent);
	
							//Permet de mettre fin � cette activit� et donc la supprimer de la m�moire.
							thisActivity.finish();
						}
					}	
					else
					{
						//Affiche un toast comme quoi le nu�mro de contrat est incorrecte
						Toast toast = Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.message_number_contract_not_correct), Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
						textView.setTextColor(getResources().getColor(R.color.red));
						toast.show();
					}
				}		
			}
		});
	}

	/**
	 * V�rification du num�ro de contrat en fonction des num�ros de contrat existant en db local
	 * 
	 * La v�rification :
	 * - Contrat existant
	 * - Contrat li� au partenaire que l'agent repr�sente
	 */
	public boolean checkNumberContract(String id)
	{
		Contrat contrat = null;

		//Si Le champ de l'id n'est pas vide.
		if(!id.equals(""))
			contrat = new AdapterContrat(this).find(id);

		//Cette condition permet de v�rifier que l'essai s�lectionner (si il existe) appartient bien au partenaire connect�
		if( contrat == null || !(contrat.getIdPartenaire().equals(settings.getString(Constantes.ID_PARTENAIRE, "").toString())))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Permet de se d�connecter.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		if (item.getItemId() == R.id.logout) {
			SharedPreferences settings = getSharedPreferences(Constantes.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.remove("logged");
			editor.commit();
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(thisActivity, ChoiceTypePassageActivity.class);
		startActivity(mIntent);
		thisActivity.finish();
		super.onBackPressed();
	}

	/** 
	 *  Cette classe permet de charger les information d'un contrat et du client qui lui est li� de fa�on asynchrnone
	 * **/
	private class LoadContractAndClient extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... arg0) {

			//Appelle de la m�thode qui permet de r�cup�rer les informations d'un contrat
			contrat = parserXMLContrat.getContrat(idPartenaire + editTextNumberContract.getText().toString());
			//Appelle de la m�thode qui permet de r�cup�rer les informations d'un client par rapport au contrat
			//Si le contrat a bien �t� r�cup�r�.
			if(contrat != null)
				client = parserXMLClient.getClient(Integer.valueOf(contrat.getIdClient()));

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			progressBar.setVisibility(View.INVISIBLE);

			//Si on a bien re�us les informations sur le client ou le contrat. Cela peut tr�s bien ne pas �tre le cas si le mobile n'est pas connect� au web.
			if(client != null && contrat != null){
				//Compl�te les textView pour affich� les informations
				textViewName.setText(client.getNom().toUpperCase() + " " + client.getPrenom().toUpperCase());
				textViewAddress.setText(client.getNomCommerce().toUpperCase() + " \n" + client.getAdresseCommerce().toUpperCase() + " \n" + client.getCodePostalCommerce().toUpperCase() + " " + client.getVilleCommerce().toUpperCase() );
				textViewLastChangeCassette.setText(thisActivity.getResources().getString(R.string.message_last_change) + " : " + contrat.getDateDernierChangement().get(Calendar.DAY_OF_MONTH) + "/" + (contrat.getDateDernierChangement().get(Calendar.MONTH) + 1 ) + "/" + contrat.getDateDernierChangement().get(Calendar.YEAR));
				//Les diff�rentes valeur de l'�tat sont connue gr�vce � �a javadoc.
				switch (Integer.parseInt(contrat.getEtat())) 
				{
					case 1:
						textViewStateContract.setText(thisActivity.getResources().getString(R.string.state_contract_wait_validation));
						break;
					case 2:
						textViewStateContract.setText(thisActivity.getResources().getString(R.string.state_contract_ok));
						break;
					case 3:
						textViewStateContract.setText(thisActivity.getResources().getString(R.string.state_contract_no_ok));
						break;
					case 4:
						textViewStateContract.setText(thisActivity.getResources().getString(R.string.state_contract_wait_resiliation));
						break;
					case 5:
						textViewStateContract.setText(thisActivity.getResources().getString(R.string.state_contract_resiliation_ok));
						break;
					default:
						textViewStateContract.setText(thisActivity.getResources().getString(R.string.state_contract_unknown));
						break;
				}
				
			}
		}
	}
}
