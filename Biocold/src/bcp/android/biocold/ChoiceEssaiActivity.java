package bcp.android.biocold;

import java.util.Calendar;

import tools.Utility;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import bcp.android.db.adapters.AdapterEssai;
import bcp.android.metier.Client;
import bcp.android.metier.Constantes;
import bcp.android.metier.Essai;
import bcp.android.metier.Passage;
import bcp.android.metier.enums.TypePassage;
import bcp.android.parsers.ParserXMLClient;
import bcp.android.parsers.ParserXMLEssai;
import bpc.android.biocold.R;

/**
 * <p>
 * Cette classe permet de c�er et de g�rer l'activit� ChoiceEssai.
 * </p>
 * <pre>
 * Cette activit� peut �tre lanc� depuis :
 * - ChoiceTypePassage
 * Cette activit� m�ne � : 
 * - MenuScan
 * - EndPassage
 * 
 * Cette activit� doit permettre d'entrer un num�ro d'essai et de le v�rifier et 
 * de visualiser si possible certaines informations sur l'essai.
 * Si c'est possible cela signifie que le smartphone a acc�s � internet.
 * </pre>
 * @author Junior & Matthiass
 * 
 */
@SuppressLint("DefaultLocale")
public class ChoiceEssaiActivity extends Activity 
{
	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_ChoiceEssai";
	/** Repr�sante cette activit� */
	private Activity thisActivity;
	/** Bouton permettant de continuer, il lance �galement la v�rification de num�ro de contrat **/
	private Button buttonContinue;
	/** Edit text dans lequel est �crit le num�ro d'essai **/
	private EditText EditTextNumberEssaiPartenaire;
	/** Fichier de pr�f�rences de l'application */
	private SharedPreferences settings;
	/** Progress bar lors du chargement **/
	private ProgressBar progressBar;
	/** Text view permettant d'afficher le nom + pr�nom du client **/
	private TextView textViewName;
	/** Text view permettant d'afficher l'adresse du client **/
	private TextView textViewAddress;
	/** Text view permettant d'afficher le nom + pr�nom du client **/
	private TextView textViewLastChangeCassette;
	/** Text view permettant d'afficher la date de d�but de l'essai*/
	private TextView textViewDateStartEssai;
	/** Variable contenant le passage avec lequel on travail */
	private Passage passage;
	/** Parser permettant d'acc�der au contrat en db extrerne **/
	private ParserXMLEssai parserXMLEssai = new ParserXMLEssai();
	/** Parser permettant d'acc�der au client en db extrerne **/
	private ParserXMLClient parserXMLClient = new ParserXMLClient();
	/** Variable qui contiendra tout les informations d'un client dans l'activit� pr�sente **/
	private Client client =  null;
	/** Variable qui contiendra tout les informations d'un essai dans l'activit� pr�sente **/
	private Essai essai = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choice_essai_activity);

		thisActivity = this;
		settings = getSharedPreferences(Constantes.PREFS_NAME, 0);

		buttonContinue = (Button) findViewById(R.id.button_continue_essai);
		textViewName = (TextView)findViewById(R.id.textView_name_essai);
		textViewAddress = (TextView)findViewById(R.id.textView_address_essai);
		textViewDateStartEssai = (TextView)findViewById(R.id.textView_date_start_essai);
		textViewLastChangeCassette = (TextView)findViewById(R.id.textView_last_change_cassette_essai);
		EditTextNumberEssaiPartenaire  = (EditText)findViewById(R.id.editText_number_essai);
		progressBar = (ProgressBar)findViewById(R.id.progressBar_essai);

		progressBar.setVisibility(View.INVISIBLE);

		//Permet de r�cup�rer le passage avec lequel on travail
		Bundle bundleReceiver = getIntent().getExtras();
		passage = (Passage) bundleReceiver.getSerializable(Constantes.PASSAGE);

		//Permet de modifier le titre de l'activit� :
		thisActivity.setTitle("Biocold - "+passage.getTypepassage().getLibelle(thisActivity));

		EditTextNumberEssaiPartenaire.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE)
				{
					//Les deux lignes suivante permette de forcer � cacher le clavier
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(EditTextNumberEssaiPartenaire.getWindowToken(), 0);

					if(verificationNumeroEssaiPartenaire(EditTextNumberEssaiPartenaire.getText().toString()))
					{
						Toast toast = Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.message_number_essai_correct), Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
						textView.setTextColor(getResources().getColor(R.color.green));
						toast.show();

						//Va chercher et affiche les informations plus compl�te si connexion au net possible
						if(Utility.isNetworkAvailable(thisActivity))
							new LoadEssaiAndClient().execute();
					}
					else
					{
						Toast toast = Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.message_number_essai_not_correct), Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
						textView.setTextColor(getResources().getColor(R.color.red));
						toast.show();
					}
				}
				return true;
			}
		});


		buttonContinue.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(v.getId()==R.id.button_continue_essai){
					if(verificationNumeroEssaiPartenaire(EditTextNumberEssaiPartenaire.getText().toString()))
					{
						//Lier le passage � l'essai.
						passage.setIdEssai(essai.getId());
						
						//Dans le cas o� l'on doit scanner au moins une cassette
						if(passage.getTypepassage() == TypePassage.REPLACE_DURING_ESSAI || passage.getTypepassage() == TypePassage.RECOVRY_END_ESSAI || passage.getTypepassage() == TypePassage.INSTALL_WITH_REPLACE || passage.getTypepassage() == TypePassage.REFUSAL_WITH_MADE)
						{
							//Permet de lancer l'activit� MenuScanActivity
							Intent mIntent = new Intent(thisActivity, MenuScanActivity.class);
							Bundle bundleSend = new Bundle();
							bundleSend.putSerializable(Constantes.PASSAGE, passage);
							mIntent.putExtras(bundleSend);
							startActivity(mIntent);

							//Permet de mettre fin � cette activit� et donc la supprimer de la m�moire.
							thisActivity.finish();
						}
						//Dans le cas o� l'on doit ne doit pas scanner de cassette
						else if(passage.getTypepassage() == TypePassage.INSTALL_WITHOUT_REPLACE || passage.getTypepassage() == TypePassage.REFUSAL_WITHOUT_MADE)
						{
							//Permet de lancer l'activit� EndPassageActivity
							Intent mIntent = new Intent(thisActivity, EndPassageActivity.class);
							Bundle bundleSend = new Bundle();
							bundleSend.putSerializable(Constantes.PASSAGE, passage);
							mIntent.putExtras(bundleSend);
							startActivity(mIntent);

							//Permet de mettre fin � cette activit� et donc la supprimer de la m�moire.
							thisActivity.finish();
						}
					}	
					else
					{
						Toast toast = Toast.makeText(thisActivity, thisActivity.getResources().getString(R.string.message_number_essai_not_correct), Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
						textView.setTextColor(getResources().getColor(R.color.red));
						toast.show();
					}
				}
			}
		});
	}

	/** 
	 * cette methode de verifier si le numero d'essai existe ou pas en database local
	 * @return false 
	 */
	public boolean verificationNumeroEssaiPartenaire(String idEssaiPartenaire){

		//Si Le champ de l'id n'est pas vide.
		if(!idEssaiPartenaire.equals(""))
			//On regarde si l'essai avec cette id existe
			essai = new AdapterEssai(this).find(idEssaiPartenaire);

		//Cette condition permet de v�rifier que l'essai s�lectionner (si il existe) appartient bien au partenaire connect�
		if(essai == null || !essai.getIdPartenaire().equals(settings.getString(Constantes.ID_PARTENAIRE, "").toString()))
			return false;
		else
			return true;

	}

	/**
	 * Permet de se d�connecter.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		if (item.getItemId() == R.id.logout) {
			SharedPreferences settings = getSharedPreferences(Constantes.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.remove("logged");
			editor.commit();
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(thisActivity, ChoiceTypePassageActivity.class);
		startActivity(mIntent);
		thisActivity.finish();
		super.onBackPressed();
	}
	
	/** 
	 *  Cette classe permet de charger les informations d'un essai et du client qui lui est li� de fa�on asynchrnone
	 * **/
	@SuppressLint("DefaultLocale")
	private class LoadEssaiAndClient extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... arg0) {

			//Appelle de la m�thode qui permet de r�cup�rer les informations d'un essai
			essai = parserXMLEssai.getEssai(essai.getId());
			//Appelle de la m�thode qui permet de r�cup�rer les informations d'un client par rapport � l'essai
			//Si le essai a bien �t� r�cup�r�.
			if(essai != null)
				client = parserXMLClient.getClient(Integer.valueOf(essai.getIdClient()));

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressBar.setVisibility(View.INVISIBLE);

			//Si on a bien re�us les informations sur le client ou le contrat. Cela peut tr�s bien ne pas �tre le cas si le mobile n'est pas connect� au web.
			if(client != null && essai != null){
				//Compl�te les textView pour affich� les informations
				textViewName.setText(client.getNom().toUpperCase() + " " + client.getPrenom().toUpperCase());
				textViewAddress.setText(client.getNomCommerce().toUpperCase() + " \n" + client.getAdresseCommerce().toUpperCase() + " \n" + client.getCodePostalCommerce().toUpperCase() + " " + client.getVilleCommerce().toUpperCase() );
				if(essai.getDateDernierPassage().get(Calendar.YEAR) != 1)
					textViewLastChangeCassette.setText(thisActivity.getResources().getString(R.string.message_last_passage)+ " : " + essai.getDateDernierPassage().get(Calendar.DAY_OF_MONTH) + "/" + (essai.getDateDernierPassage().get(Calendar.MONTH) + 1 ) + "/" + essai.getDateDernierPassage().get(Calendar.YEAR));
				if(essai.getDateDebut().get(Calendar.YEAR) != 1)
					textViewDateStartEssai.setText(thisActivity.getResources().getString(R.string.message_start_essai)+ " : " + essai.getDateDebut().get(Calendar.DAY_OF_MONTH) + "/" + (essai.getDateDebut().get(Calendar.MONTH) + 1 ) + "/" + essai.getDateDebut().get(Calendar.YEAR));
			}
		}
	}
}



