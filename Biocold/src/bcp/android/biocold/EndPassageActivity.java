package bcp.android.biocold;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import bcp.android.metier.Constantes;
import bcp.android.metier.Passage;
import bcp.android.service.SynchroPassageService;
import bpc.android.biocold.R;

/**
 * <p>
 * Cette classe permet de c�er et de g�rer l'activit� EndPassage.
 * </p>
 * <pre>
 * Cette activit� peut �tre lanc� depuis :
 * - RecapScan
 * - ChoiceContract
 * Cette activit� m�ne � : 
 * - MainActivity
 * 
 * Cette activit� doit permettre de visualiser certaine information sur le scan et d'en compl�ter.
 * Cette activit� doit �tre la derni�re concernat la s�quence d'activit� � faire pour enregistrer un passage
 * car c'est bien lorsque l'on fini cette activit� qu'est effectivement enregistr� le passage.
 * </pre>
 * @author Junior
 * 
 */
public class EndPassageActivity extends Activity {

	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_EndPassage";
	/** Bouton permettant de terminer le passage **/
	private Button buttonEnd;
	/** Edit text dans lequel est �crit l'observation **/
	private EditText editTextObservation;
	/** TextView dans lequel est �crit le type de passage **/
	private TextView textViewTypePassage;
	/** Cette activit� */
	private Activity thisActivity;
	/** Passage en cours et qui va �tre terminer gr�ce � cette activit� */
	private Passage passage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.end_passage_activity);
		thisActivity = this;
		
		textViewTypePassage = (TextView)findViewById(R.id.textView_type_passage);
		editTextObservation = (EditText)findViewById(R.id.editText_observation_end_passage);
		buttonEnd = (Button)findViewById(R.id.button_choice_end_passage);
		
		//R�cup�ration du passage pass� � l'activit�
		Bundle bundleReceiver = getIntent().getExtras();
		passage = (Passage) bundleReceiver.getSerializable(Constantes.PASSAGE);
		
		textViewTypePassage.setText(passage.getTypepassage().getLibelle(thisActivity));
		
		buttonEnd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//On ajoute l'observation au passage
				passage.setObservationpassageinterne(editTextObservation.getText().toString());
				// On passe � l'activit� suivante

				Intent intent = new Intent(thisActivity, SynchroPassageService.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable(Constantes.PASSAGE, passage);
				intent.putExtras(bundle);
				startService(intent);

				// On cr�� une boite de dialogue
				AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
				// On lui ajoute un titre
				builder.setTitle(thisActivity.getResources().getString(R.string.info_title_end_passage));
				// On affiche le message
				builder.setMessage(thisActivity.getResources().getString(R.string.message_end_passage));
				//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
				builder.setPositiveButton(thisActivity.getResources().getString(R.string.default_ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent mIntent = new Intent(thisActivity, MainActivity.class);
						startActivity(mIntent);
						thisActivity.finish();
					}
				});
				builder.show();
				

			}
		});
		
	}

	
	@Override
	public void onBackPressed(){
		afficherAlerteRetourMainActivity();
	}
	
	private void afficherAlerteRetourMainActivity(){
		// On cr�� une boite de dialogue
		AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
		// On lui ajoute un titre
		builder.setTitle(thisActivity.getResources().getString(R.string.warning_title));
		// On affiche le message
		builder.setMessage(thisActivity.getResources().getString(R.string.warning_quit));
		//On ajoute un bouton Annuler qui permet simplement de quitter la boite de dialogue
		builder.setPositiveButton(thisActivity.getResources().getString(R.string.default_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On retourne � l'activit� principale
				startActivity(new Intent(thisActivity, MainActivity.class));
				thisActivity.finish();
			}
		}); 
		builder.setNegativeButton(thisActivity.getResources().getString(R.string.default_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// On ferme la bo�te de dialogue
				dialog.dismiss();
			}
		});
		builder.show();
	}
}
