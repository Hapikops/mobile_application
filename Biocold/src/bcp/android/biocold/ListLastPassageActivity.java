package bcp.android.biocold;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import tools.Utility;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import bcp.android.db.adapters.AdapterPassage;
import bcp.android.graphic.adapters.ListLastPassageAdapter;
import bcp.android.metier.Constantes;
import bcp.android.metier.Passage;
import bcp.android.parsers.ParserXMLPassage;
import bpc.android.biocold.R;

/**
 *
 * 
 * Permet d'affich� la liste des dernier passage effectu� et d'avoir ainsi leur identifiant.
 * Cette vue n'est accessible que si l'acc�s au r�seau internet mobile est bien disponible.
 * Sinon une alerte pr�viens l'utilisateur. 
 * 
 * <pre>Cette activit� peut �tre lanc� depuis :
 * - MainActivity
 * 
 * </pre>
 * @author Junior 
 */
public class ListLastPassageActivity extends Activity {

	/** Pour les log */
	@SuppressWarnings("unused")
	private final static String TAG = "BIOCOLD_ListLastPassage";

	/** List des passages � afficher */
	private List<Passage> listPassage;
	/** Parser permettant d'acc�der aux passages en db extrerne **/
	private ParserXMLPassage parserXMLPassageAgent = new ParserXMLPassage();
	/** Adaptater permettant d'interagir avec la db local (Table : PassageAgent) */
	private AdapterPassage adapterPassageAgent;
	/** Fichier de pr�f�rences de l'application */
	private SharedPreferences settings;
	/** Progress bar lors du chargement **/
	private ProgressBar progressBar;
	/** List qui contient les diff�rents groupes de passage (Chaque date de la semaine �coul�e) */
	private List<GregorianCalendar> groupes;
	/** List qui contient des listes de PassageAgent pour chaque groupe */
	private static List<List<Passage>> childItems;
	/** ExpandableList qui permet d'afficher la liste des passage selon leur date de passage */
	private static ExpandableListView expandableList;
	/** Cette activit� */
	private Activity thisActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_last_passage_activity);
		settings = getSharedPreferences(Constantes.PREFS_NAME, 0);
		thisActivity = this;

		expandableList = (ExpandableListView) findViewById(R.id.expandable_list_last_passage);
		
		adapterPassageAgent = new AdapterPassage(getApplicationContext());

		progressBar = (ProgressBar)findViewById(R.id.progressBar_passage_list);


		if(Utility.isNetworkAvailable(this))
		{			
			new ProcessusThread().execute();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_last_passage, menu);
		return true;
	}

	public static void collapse(int groupPosition){
		expandableList.collapseGroup(groupPosition);
	}
	
	/** method to add parent Items */
	public void setGroupParents(){
		groupes = new ArrayList<GregorianCalendar>();
		GregorianCalendar currentDate = listPassage.get(0).getDatePassage();
		groupes.add(listPassage.get(0).getDatePassage());
		for(int i = 0; i < listPassage.size(); i++)
		{
			if(reduceCuracyDate(listPassage.get(i).getDatePassage()).before(reduceCuracyDate(currentDate)))
			{
				currentDate = listPassage.get(i).getDatePassage();
				groupes.add(listPassage.get(i).getDatePassage());
			}
		}
	}

	/** method to set child data of each parent */
	public void setChildData(){
		childItems = new ArrayList<List<Passage>>();
		for(GregorianCalendar date: groupes){
			// Contient les passge correspondant � la date en cours
			List<Passage> listPassage = new ArrayList<Passage>();
			for(Passage passage : this.listPassage)
			{
				if(reduceCuracyDate(passage.getDatePassage()).getTimeInMillis() == reduceCuracyDate(date).getTimeInMillis()){
					listPassage.add(passage);
				}
			}
			childItems.add(listPassage);
		}
	}
	
	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(thisActivity, MainActivity.class);
		startActivity(mIntent);
		thisActivity.finish();
		super.onBackPressed();
	}

	/** R�duit la pr�cision de la date au jour pr�t **/
	private GregorianCalendar reduceCuracyDate(GregorianCalendar calendar)
	{ 
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeInMillis(calendar.getTimeInMillis());
		cal.clear(Calendar.MILLISECOND);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.HOUR_OF_DAY);
		cal.clear(Calendar.HOUR);
		return cal;
	}

	private class ProcessusThread extends AsyncTask<String, Void, String> {
		ListLastPassageAdapter adapter;
		@Override
		protected String doInBackground(String... arg0) {			

			//Faire patienter tant que tout n'as pas �t� synchronis�
			while(adapterPassageAgent.count() != 0);

			//R�cup�rayion de la liste des passages
			listPassage = parserXMLPassageAgent.getListPassageAgent(settings.getString(Constantes.ID_AGENT, "").toString(), thisActivity);

			setGroupParents();
			setChildData();
			
			adapter = new ListLastPassageAdapter(groupes, childItems);


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			//Une fois que �a ne boucle plus, c'est que la synchro est ok et donc on peut cacher la progressBar
			progressBar.setVisibility(View.GONE);
 
			expandableList.setDividerHeight(2);
			expandableList.setGroupIndicator(null);
			expandableList.setClickable(true);
			adapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), thisActivity);
			expandableList.setAdapter(adapter);
		}
	}
}
