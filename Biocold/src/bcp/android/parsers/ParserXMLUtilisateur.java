package bcp.android.parsers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import bcp.android.metier.Utilisateur;

/**
 * 
 * <p>
 * Ce parser XML permet de r�cup�rer un ou plusieurs utilisateur depuis le flux XML d'un service web
 * </p>
 * 
 * @author Junior
 *
 */
public class ParserXMLUtilisateur extends DefaultHandler implements Runnable {
	/** Utilisateur � renvoyer � l'appelant */
	private Utilisateur user;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	
	/** Adresse du web service auquel est li� le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/UserWS.svc/";

	/**
	 * Retourne un utilisateur gr�ce a un web service XML
	 * @param idPartenaire ID du partenaire � retourner.
	 * @param activity Activit� sur laquel sera affich� les messages d'erreur
	 * @return Retourne un client. Ou un objet null si le webservice n'a pas su �tre atteint ou que le client n'existe pas.
	 */
	public Utilisateur getUser(String loginUtilisateur){

		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);
			HttpMethodeGetRunnable httpMethodeGetRunnable = new HttpMethodeGetRunnable();
			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = httpMethodeGetRunnable.getInputStream(adresse + loginUtilisateur);
			if (httpMethodeGetRunnable.getReturnCode() == 404){
				return null;
			}
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {
		}
		return user;
	}

	@Override
	public void startDocument() throws SAXException{
		user = new Utilisateur();
	}
	@Override 
	public void endDocument()throws SAXException{

	}
	@Override
	public void startElement(String namespaceURI, String localName,
			String qName, Attributes atts) throws SAXException {
		if(localName.equalsIgnoreCase("loginResult")){
			user = new Utilisateur();
		}
	}
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("loginResult")){
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("id")){
				user.setId(current);
				current = "";
			}
			else if(localName.equalsIgnoreCase("idpartenaire")){
				user.setIdPartenaire(current);
				current = "";
			}
			else if(localName.equalsIgnoreCase("login")){
				user.setLogin(current);
				current = "";
			}
			else if(localName.equalsIgnoreCase("niveauacces")){
				user.setNiveauAcces(current);
				current = "";
			}
			else if(localName.equalsIgnoreCase("password")){
				user.setPassword(current);
				current = "";
			}
		}
	}
	// M�thode appel�e sur les valeurs se trouvant entre les balises
	@Override
	public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}

	@Override
	public void run() {
		
	}
	
}
