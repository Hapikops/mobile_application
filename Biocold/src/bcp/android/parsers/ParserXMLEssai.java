package bcp.android.parsers;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import android.annotation.SuppressLint;
import android.util.Log;
import bcp.android.metier.Essai;

/**
 *
 *Ce parseur permet de recuperer un ou plusieur essai depuis les flux XML d'un web service
 *
 *@author frederic radigoy
 *
 **/

public class ParserXMLEssai extends DefaultHandler {
	/** Variable Essai **/
	private Essai essai;

	/** liste de essai**/
	private List<Essai> listEssai;

	/** Variable pour stocker la valeur de la balise courante */
	private String current;

	/** Aadresse du WebService auquel le parser est ratach� **/
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/EssaiWS.svc/";

	/**
	 * Retourne une liste d'essai à partir d'un web service XML
	 */

	public List<Essai> getListEssai(String idPartenaire){


		try{
			// On recupere un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux alimente par le SAXparser
			readerxml.setContentHandler(this);

			// On r�cupere l'objet InputStream depuis la methode ci-dessous definie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + "listWithoutContract/" + idPartenaire);
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {}
		if(listEssai.size() != 0)
			return listEssai;
		else
			return null;





	}

	/**
	 * 
	 * Retourner un Essai gr�ce au web service
	 * 
	 */
	public Essai getEssai(String idEssai){
		try
		{
			//on recupere un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			readerxml.setContentHandler(this);

			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + idEssai );
			// Si l'objet n'est pas null

			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {
			//Toast.makeText(context, "Erreur", Toast.LENGTH_LONG).show();
			Log.e("BIO", e.getMessage());
		}
		if(listEssai != null && listEssai.size() != 0)
			return listEssai.get(0);
		else
			return null;
	}

	@Override
	public void startDocument() throws SAXException{
		listEssai = new ArrayList<Essai>();
	}
	@Override
	public void endDocument() throws SAXException{

	}
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException{
		if(localName.equalsIgnoreCase("Essai")){
			essai = new Essai();
		}

	}
	@SuppressLint("SimpleDateFormat")
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException{
		if(localName.equalsIgnoreCase("Essai")){
			listEssai.add(essai);
			essai = null;
			endDocument();
		}
		else 
		{
			if(localName.equalsIgnoreCase("Id")){
				essai.setId(current);
				current ="";
			}
			if(localName.equalsIgnoreCase("IdEssaiPartenaire")){
				essai.setIdEssaiPartenaire(current);
				current="";
			}
			if(localName.equalsIgnoreCase("IdPartenaire")){
				essai.setIdPartenaire(current);
				current="";
			}
			if(localName.equalsIgnoreCase("DateDernierChangement")){  
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					essai.setDateDernierChangement(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("DateDernierPassage")){  
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					essai.setDateDernierPassage(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("DateDebut")){  
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					essai.setDateDebut(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("IdClient")){
				essai.setIdClient(current);
				current = null;
			}
		}

	}
	@Override
	public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}




}
