package bcp.android.parsers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import bcp.android.metier.Client;

/**
 * 
 * <p>
 * Ce parser XML permet de r�cup�rer un ou plusieurs client depuis le flux XML d'un service web
 * </p>
 * 
 * @author Junior
 *
 */
public class ParserXMLClient extends DefaultHandler{
	
	private Client client;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	
	/** Adresse du web service auquel est li� le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/ClientWS.svc/";


	/**
	 * Retourne un client gr�ce a un web service XML
	 * @param idClient ID du client � retourner.
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 * @return Retourne un client. Ou un objet null si le webservice n'a pas su �tre atteint ou que le client n'existe pas.
	 */
	public Client getClient(int idClient){

		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);

			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + idClient);
			// Si l'objet n'est pas null

			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {}
		return client;
	}

	@Override
	public void startDocument() throws SAXException{
		client = new Client();
	}
	@Override 
	public void endDocument()throws SAXException{
		
	}
	
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if(localName.equalsIgnoreCase("Client")){
			client = new Client();
		}
	}
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("Client")){
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("AdresseCommerce")){
					client.setAdresseCommerce(current);
					current = "";
			}
			if(localName.equalsIgnoreCase("CodePostalCommerce")){
					client.setCodePostalCommerce(current);
					current = "";
			}
			if(localName.equalsIgnoreCase("Id")){
					client.setId(current);
					current = "";
			}
			if(localName.equalsIgnoreCase("IdContrat")){
					client.setIdPartenaire(current);
					current = "";
			}
			if(localName.equalsIgnoreCase("IdEssai")){
					client.setIdEssai(current);
					current = "";
			}
			if(localName.equalsIgnoreCase("IdPartenaire")){
				client.setIdPartenaire(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("Nom")){
				client.setNom(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("NomCommerce")){
				client.setNomCommerce(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("Prenom")){
				client.setPrenom(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("VilleCommerce")){
				client.setVilleCommerce(current);
				current = "";
			}
		}
	}
	@Override
    public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}
}
