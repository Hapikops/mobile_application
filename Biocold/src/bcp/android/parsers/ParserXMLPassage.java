package bcp.android.parsers;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;
import bcp.android.metier.Passage;
import bcp.android.metier.enums.TypePassage;

/**
 * 
 * <p>
 * Ce parser XML permet de r�cup�rer un ou plusieurs passage d'agent depuis le flux XML d'un service web
 * </p>
 * 
 * @author Junior
 *
 */
public class ParserXMLPassage extends DefaultHandler implements Runnable{
	/** Liste de passage � renvoyer */
	private List<Passage> listPassage;
	/** Variable pour stocker le passage temporaire que l'on ajoute � la List listPassage pour chaque contrat */
	private Passage passage;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	
	/** Adresse du web service auquel est li� le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/PassageAgentWS.svc/";

	/**
	 * Retourne une liste de passage gr�ce a un web service XML
	 * @param idAgent ID de l'agent � qui appartient les passages � retourner
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 * @return Retourne une liste de passages. Ou un objet null si le webservice n'a pas su �tre atteint ou qu'il n'y a pas de passage pour cette agent.
	 */
	public List<Passage> getListPassageAgent(String idAgent, Context context){

		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);

			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + "list/" + idAgent);
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {
			Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
		}
		if(listPassage.size() != 0)
			return listPassage;
		else
			return null;
	}

	@Override
	public void startDocument() throws SAXException{
		listPassage = new ArrayList<Passage>();
	}
	@Override 
	public void endDocument()throws SAXException{
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if(localName.equalsIgnoreCase("PassageAgent")){
			passage = new Passage();
		}
	}
	
	/**
	 * Se charge de vraiment interpr�ter le XML et de compl�ter un objet PassageAgent qui est ajouter � une liste de PassageAgent
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("PassageAgent")){
			listPassage.add(passage);
			passage = null;
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("DatePassage")){  
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					passage.setDatepassage(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("DureePassage")){
				passage.setDureepassage(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("Id")){
				passage.setId(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("IdAgent ")){
				passage.setIdAgent(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("IdEssai")){
				passage.setIdEssai(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("IdUtilisateur")){
				passage.setIdUtilisateur(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("ObservationPassageClient ")){
				passage.setObservationpassageclient(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("ObservationPassageInterne ")){
				passage.setObservationpassageinterne(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("TypePassage")){
				for (TypePassage enumTypePassage : TypePassage.getList(0)) {
					if(current.equalsIgnoreCase(enumTypePassage.getCode()))
						passage.setTypepassage(enumTypePassage);
				}
				current = null;
			}
			if(localName.equalsIgnoreCase("IdContrat")){
				passage.setIdContrat(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("IdEssaiPartenaire")){
				passage.setIdEssaiPartenaire(current);
				current = null;
			}
		}
	}
	@Override
	public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}


	@Override
	public void run() {
		
	}	
}

