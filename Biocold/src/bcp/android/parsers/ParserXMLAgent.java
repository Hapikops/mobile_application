package bcp.android.parsers;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import android.annotation.SuppressLint;
import bcp.android.metier.Agent;

/**
 * 
 * <p>
 * Ce parser XML permet de r�cup�rer un ou plusieurs agent depuis le flux XML d'un service web
 * </p>
 * 
 * @author Junior
 *
 */
public class ParserXMLAgent extends DefaultHandler{
	
	private Agent agent;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	
	/** Adresse du web service auquel est li� le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/AgentWS.svc/";


	/**
	 * Retourne un client gr�ce a un web service XML
	 * @param idClient ID du client � retourner.
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 * @return Retourne un client. Ou un objet null si le webservice n'a pas su �tre atteint ou que le client n'existe pas.
	 */
	public Agent getAgent(String idUtilisateur){
		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);
			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + idUtilisateur);
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {}
		return agent;
	}

	@Override
	public void startDocument() throws SAXException{
		agent = new Agent();
	}
	
	@SuppressLint("SimpleDateFormat")
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("Agent")){
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("actif")){
					agent.setActif(current);
					current = "";
			}
			if(localName.equalsIgnoreCase("dateentree")){
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					agent.setDateentree(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("datesortie")){
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					agent.setDatesortie(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("id")){
					agent.setIdagent(current);
					current = "";
			}
			if(localName.equalsIgnoreCase("IdPartenaire")){
				agent.setInitialespartenaire(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("idutilisateur")){
				agent.setIdUtilisateur(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("Nom")){
				agent.setNomagent(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("Prenom")){
				agent.setPrenomagent(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("telephone")){
				agent.setTelephone(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("telephoneportable")){
				agent.setTelephoneportable(current);
				current = "";
			}			
		}
	}
	@Override
    public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}
}
