package bcp.android.parsers;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import android.annotation.SuppressLint;
import bcp.android.metier.Partenaire;

/**
 * 
 * <p>
 * Ce parser XML permet de r�cup�rer un ou plusieurs partenaire depuis le flux XML d'un service web
 * </p>
 * 
 * @author Junior
 *
 */
public class ParserXMLPartenaire extends DefaultHandler{
	private Partenaire partenaire;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	
	/** Adresse du web service auquel est li� le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/PartenaireWS.svc/";

	/**
	 * Retourne un partenaire gr�ce a un web service XML
	 * @param idPartenaire ID du partenaire � retourner.
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 * @return Retourne un client. Ou un objet null si le webservice n'a pas su �tre atteint ou que le client n'existe pas.
	 */
	public Partenaire getPartenaire(String idPartenaire){

		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);

			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + idPartenaire);
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {}
		return partenaire;
	}

	@Override
	public void startDocument() throws SAXException{
		partenaire = new Partenaire();
	}
	@Override 
	public void endDocument()throws SAXException{

	}
	@Override
	public void startElement(String namespaceURI, String localName,
			String qName, Attributes atts) throws SAXException {
		if(localName.equalsIgnoreCase("partenaire")){
			partenaire = new Partenaire();
		}
	}
	@SuppressLint("SimpleDateFormat")
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("partenaire")){
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("adresse")){
				partenaire.setAdresse(current);
				// On remet la valeur de current � null pour �viter une erreur : 
				// Si une balise � cette forme <../> current ne change pas de valeur
				current = "";
			}
			if(localName.equalsIgnoreCase("baremecom")){
				partenaire.setBaremCom(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("datenaissance")){
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					partenaire.setDateNaiss(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("email")){
				partenaire.setEmail(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("codepostal")){
				partenaire.setCp(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("id")){
				partenaire.setId(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("nom")){
				partenaire.setNom(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("pays")){
				partenaire.setPays(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("prenom")){
				partenaire.setPrenom(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("telephone1")){
				partenaire.setTel1(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("telephone2")){
				partenaire.setTel2(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("telephonefacture")){
				partenaire.setTelfacture(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("telephoneportable")){
				partenaire.setPortable(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("type")){
				partenaire.setType(current);
				current = "";
			}
			if(localName.equalsIgnoreCase("ville")){
				partenaire.setVille(current);
				current = "";

			}
		}
	}

	@Override
	public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}

}
