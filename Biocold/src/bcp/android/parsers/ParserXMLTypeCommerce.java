package bcp.android.parsers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import bcp.android.metier.TypeCommerce;


/**
*
*Ce parseur permet de recuperer un type de commerce  depuis les flux XML d'un web service
*
*@author frederic radigoy
*
**/
public class ParserXMLTypeCommerce extends DefaultHandler{
	
	/** Variable qui contient la liste des types de commerce */
	private List<TypeCommerce> ListTypeCommerce;
	/** Variable qui contient le commerce stocké temporairement à ajouter dans la liste */
	private TypeCommerce typeCommerce;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	/** Adresse du web service auquel est lié le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/TypeCommerceWS.svc/";
	
	
	/**
	 * Retourne un client grâce a un web service XML
	 * 
	 * @return Retourne un client. Ou un objet null si le webservice n'a pas su être atteint ou que le client n'existe pas.
	 */
	public List<TypeCommerce> getListTypeCommerce()
	{
		try{
			// On récupére un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux alimenté par le SAXparser
			readerxml.setContentHandler(this);
			// On récupére l'objet InputStream depuis la méthode ci-dessous définie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + "ListTypeCommerce");
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuyé par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {}
		if(ListTypeCommerce.size() != 0)
			return ListTypeCommerce;
		else
			return null;
	}
	
	@Override
	public void startDocument() throws SAXException
	{
		ListTypeCommerce = new ArrayList<TypeCommerce>();
		
	}
	
	@Override 
	public void endDocument()throws SAXException{
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if(localName.equalsIgnoreCase("typeCommerce")){
			typeCommerce = new TypeCommerce();
		}
	}
	
	/**
	 * Se charge de vraiment interpréter le XML et de compléter un objet contrat qui est ajouter à une liste de contrat
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("TypeCommerce")){
			ListTypeCommerce.add(typeCommerce);
			typeCommerce = null;
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("Key")){
				typeCommerce.setId(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("Value")){
				typeCommerce.setLibelle(current);
				current = null;
			}
		}
	}
	@Override
	public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}

}
