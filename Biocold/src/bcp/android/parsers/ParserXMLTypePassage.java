package bcp.android.parsers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import bcp.android.metier.TypePassage;

/**
 * 
 * <p>
 * Ce parser XML permet de r�cup�rer un ou plusieurs agent depuis le flux XML d'un service web
 * </p>
 * <p>
 * Cette classe est d�pr�ci� car elle ne sert plus et n'a normalement plus besoin d'�tre utilis�e. 
 * Car la liste des types de passage est devenue une Enum�artion et n'est donc plus charg�e depuis la db externe
 * </p>
 * 
 * @author Junior
 * @author Matthias
 *
 */
@Deprecated
public class ParserXMLTypePassage extends DefaultHandler{
	/** Variable qui contient la liste des types de passages */
	private List<TypePassage> listTypePassage;
	/** Variable qui contient le passage stock� temporairement � ajouter dans la liste */
	private TypePassage typePassage;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	
	/** Adresse du web service auquel est li� le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/PassageAgentWS.svc/";


	/**
	 * Retourne un client gr�ce a un web service XML
	 * 
	 * @return Retourne un client. Ou un objet null si le webservice n'a pas su �tre atteint ou que le client n'existe pas.
	 */
	public List<TypePassage> getTypesPassage(){
		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);
			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + "listTypePassage");
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {}
		if(listTypePassage.size() != 0)
			return listTypePassage;
		else
			return null;
	}

	@Override
	public void startDocument() throws SAXException{
		listTypePassage = new ArrayList<TypePassage>();
	}
	@Override 
	public void endDocument()throws SAXException{
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if(localName.equalsIgnoreCase("TypePassage")){
			typePassage = new TypePassage();
		}
	}
	
	/**
	 * Se charge de vraiment interpr�ter le XML et de compl�ter un objet contrat qui est ajouter � une liste de contrat
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("TypePassage")){
			listTypePassage.add(typePassage);
			typePassage = null;
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("Key")){
				typePassage.setId(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("Value")){
				typePassage.setLibelleType(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("Categorie")){
				typePassage.setCategorie(current);
				current = null;
			}
		}
	}
	@Override
	public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}
}
