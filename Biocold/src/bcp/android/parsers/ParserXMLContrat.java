package bcp.android.parsers;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import tools.HttpMethodeGetRunnable;
import android.annotation.SuppressLint;
import android.util.Log;
import bcp.android.metier.Contrat;

/**
 * 
 * <p>
 * Ce parser XML permet de r�cup�rer un ou plusieurs contrat depuis le flux XML d'un service web
 * </p>
 * 
 * @author Junior
 *
 */
public class ParserXMLContrat extends DefaultHandler {
	/** Liste de contrat � renvoyer */
	private List<Contrat> listContrat;
	/** Variable pour stocker le contrat temporaire que l'on ajoute � la List lesContrats pour chaque contrat */
	private Contrat contrat;
	/** Variable pour stocker la valeur de la balise courante */
	private String current;
	
	/** Adresse du web service auquel est li� le parser */
	private String adresse = "http://extranetbcp.fr/WebService/Implementation/ContratWS.svc/";

	/**
	 * Retourne une liste de contrat gr�ce a un web service XML
	 * @param idPartenaire ID du partenaire � qui appartient les contrats � retourner
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 * @return Retourne une liste de contrat. Ou un objet null si le webservice n'a pas su �tre atteint ou qu'il n'y a pas de contrat pour ce partenaire.
	 */
	public List<Contrat> getListContrat(String idPartenaire){

		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);

			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + "list/" + idPartenaire);
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {}
		if(listContrat.size() != 0)
			return listContrat;
		else
			return null;
	}
	
	
	/**
	 * Retourne un contrat gr�ce a un web service XML.
	 * @param idContrat ID du contrat
	 * @param context Context sur laquel sera affich� les messages d'erreur
	 * @return Retourne un contrat. Ou un objet null si le webservice n'a pas su �tre atteint ou que le contrat n'existe pas.
	 */
	public Contrat getContrat(String idContrat){

		try{
			// On r�cup�re un SaxParser depuis le SaxParserFactory
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxFactory.newSAXParser();
			XMLReader readerxml = saxParser.getXMLReader();
			// creer un lecteur de flux aliment� par le SAXparser
			readerxml.setContentHandler(this);
			// On r�cup�re l'objet InputStream depuis la m�thode ci-dessous d�finie
			InputStream is = new HttpMethodeGetRunnable().getInputStream(adresse + idContrat);
			// Si l'objet n'est pas null
			if(is != null){
				// On le parse avec notre readerxml (appuy� par notre parserxml)
				readerxml.parse(new InputSource(is));
				// On ferme l'objet dont on a plus besoins
				is.close();
			}
		}
		catch(Exception e) {
			//Toast.makeText(context, "Erreur", Toast.LENGTH_LONG).show();
			Log.e("BIO", e.getMessage());
		}
		if(listContrat != null && listContrat.size() != 0)
			return listContrat.get(0);
		else
			return null;
	}

	@Override
	public void startDocument() throws SAXException{
		listContrat = new ArrayList<Contrat>();
	}
	@Override 
	public void endDocument()throws SAXException{
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if(localName.equalsIgnoreCase("Contrat")){
			contrat = new Contrat();
		}
	}
	
	/**
	 * Se charge de vraiment interpr�ter le XML et de compl�ter un objet contrat qui est ajouter � une liste de contrat
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if(localName.equalsIgnoreCase("Contrat")){
			listContrat.add(contrat);
			contrat = null;
			endDocument();
		}
		else{
			if(localName.equalsIgnoreCase("DateDernierChangement")){  
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					contrat.setDateDernierChangement(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("DateDernierPassage")){  
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date;
				try {
					date = simpleDateFormat.parse(current);
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					contrat.setDateDernierPassage(gregorianCalendar);
					current = null;
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(localName.equalsIgnoreCase("Id")){
				contrat.setId(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("IdPartenaire")){
				contrat.setIdPartenaire(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("NumeroContrat")){
				contrat.setNumeroContrat(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("IdClient")){
				contrat.setIdClient(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("idessai")){
				contrat.setIdEssai(current);
				current = null;
			}
			if(localName.equalsIgnoreCase("Etat")){
				contrat.setEtat(current);
				current = null;
			}
		}
	}
	@Override
	public void characters(char ch[], int start, int length) {
		current = new String(ch, start, length);
	}
}

